

$(document).ready(function(){
	// Mascaras
    var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00)00000-0000' : '(00)0000-00009';
      },
      spOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
          }
      };
      
    $('.telefone').mask(SPMaskBehavior, spOptions);
    $('.numero').mask('0000000000');
    $('.date').mask('00/00/0000');
    $('.cep').mask('00000-000');
    $('.cpf').mask('000.000.000-00', {reverse: true});
    $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
    $('.number').mask('00000000000');
    $('.gtin').mask('00000000000000');
    $('.porcentagem').mask('000,00', {reverse: true});
    $('.money').mask("#.##0,00", {reverse: true});



  	$("#input-filtro").on("keyup", function() {
	    var value = $(this).val().toLowerCase();
	    $("#tabela-corpo tr").filter(function() {
	      	$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
	    });
  	});


	  jQuery.validator.addMethod("cnpj", function(cnpj, element) {
		
			var numeros, digitos, soma, resultado, pos, tamanho,
				digitos_iguais = true;
		
			if (cnpj.length < 14 && cnpj.length > 15)
				return false;
		
			for (var i = 0; i < cnpj.length - 1; i++)
				if (cnpj.charAt(i) != cnpj.charAt(i + 1)) {
					digitos_iguais = false;
					break;
				}
		
			if (!digitos_iguais) {
				tamanho = cnpj.length - 2
				numeros = cnpj.substring(0,tamanho);
				digitos = cnpj.substring(tamanho);
				soma = 0;
				pos = tamanho - 7;
		
				for (i = tamanho; i >= 1; i--) {
					soma += numeros.charAt(tamanho - i) * pos--;
					if (pos < 2)
						pos = 9;
				}
		
				resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		
				if (resultado != digitos.charAt(0))
					return false;
		
				tamanho = tamanho + 1;
				numeros = cnpj.substring(0,tamanho);
				soma = 0;
				pos = tamanho - 7;
		
				for (i = tamanho; i >= 1; i--) {
					soma += numeros.charAt(tamanho - i) * pos--;
					if (pos < 2)
						pos = 9;
				}
		
				resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		
				if (resultado != digitos.charAt(1))
					return false;
		
				return true;
			}
		
			return false;
		}, "Informe um CNPJ válido.");
	


$.validator.addMethod("cpf", function(value, element) {
	value = jQuery.trim(value);
	
	value = value.replace('.','');
	value = value.replace('.','');
	cpf = value.replace('-','');
	while(cpf.length < 11) cpf = "0"+ cpf;
	var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
	var a = [];
	var b = new Number;
	var c = 11;
	for (i=0; i<11; i++){
		a[i] = cpf.charAt(i);
		if (i < 9) b += (a[i] * --c);
	}
	if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
	b = 0;
	c = 11;
	for (y=0; y<10; y++) b += (a[y] * c--);
	if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }

	var retorno = true;
	if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) retorno = false;

		return this.optional(element) || retorno;
	}, "Informe um CPF válido");

	$.validator.addMethod(  
		"date",  
		function(value, element) {  
				var check = false;  
				var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;  
				if( re.test(value)){  
					var adata = value.split('/');  
					var gg = parseInt(adata[0],10);  
					var mm = parseInt(adata[1],10);  
					var aaaa = parseInt(adata[2],10);  
					var xdata = new Date(aaaa,mm-1,gg);  
					if ( ( xdata.getFullYear() == aaaa ) && ( xdata.getMonth () == mm - 1 ) && ( xdata.getDate() == gg ) )  
						check = true;  
					else  
						check = false;  
				} else  
					check = false;  
				return this.optional(element) || check;  
		},  
		"Insira uma data válida"  
	);
       
	$.validator.addMethod("select", function(value, element, arg){
	return arg !== value;
	});

	$.validator.addMethod(  
      "date",  
      function(value, element) {  
           var check = false;  
           var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;  
           if( re.test(value)){  
                var adata = value.split('/');  
                var gg = parseInt(adata[0],10);  
                var mm = parseInt(adata[1],10);  
                var aaaa = parseInt(adata[2],10);  
                var xdata = new Date(aaaa,mm-1,gg);  
                if ( ( xdata.getFullYear() == aaaa ) && ( xdata.getMonth () == mm - 1 ) && ( xdata.getDate() == gg ) )  
                     check = true;  
                else  
                     check = false;  
           } else  
                check = false;  
           return this.optional(element) || check;  
      },  
      "Insira uma data válida"  
 );  

});

function setData(data){
    var ano = data.substring(0,4);
    var mes = data.substring(5,7);
    var dia = data.substring(8,10);
    return dia+"/"+mes+"/"+ano;
}

function setTelefone(data){
    var ddd = data.substring(0,2);
    if(data.length === 10){
        var pri = data.substring(2,6);
        var sec = data.substring(6,11);
    }else{
        var pri = data.substring(2,7);
        var sec = data.substring(7,11);
    }
    
    return "("+ddd+")"+pri+"-"+sec;
}
