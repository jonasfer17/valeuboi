$(function(){
    $('#form-login').on('submit', function(e){
        e.preventDefault();
        var txt = $('#form-login').serialize();
        $.ajax({
            url: URL+'login/login/',
            type: 'POST',
            data: txt,
            datatype: 'json',
            success:function(data){
                var json = JSON.parse(data);
                if(json.code =='200'){
                    window.location.href = URL + 'admin';
                }else if(json.code == '300'){
                    $('#aviso').html('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <p>'+json.mensagem+'</p></div>');
                }
                
            },
            error:function(data){
                console.log(data);
            }
        });
    });

    $('#login').on('focus', function(){
        $('#aviso').html('');
    });

    $('#senha').on('focus', function(){
        $('#aviso').html('');
    });
});

