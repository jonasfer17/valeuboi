$(function(){
	
	$("#form-produto").submit(function(e) {
			e.preventDefault();
		}).validate({
			rules: {
				nome : {
					required: true
				},
				unidade_medida: {
					select: '0'
				},
				preco_compra :{
					required: true
				},
				preco_custo :{
					required: true
				},
				margem :{
					required: true
				},
				preco_venda :{
					required: true
				}
			},
			highlight: function (input) {
				$(input).parents('.form-group').addClass('has-error');
			},
			unhighlight: function (input) {
				$(input).parents('.form-group').removeClass('has-error');
			},
			errorPlacement: function (error, element) {
				$(element).parents('.form-group').append(error);
			},
			submitHandler: function(form) { 
				var url = $('#form-produto').data('url');
				var txt = $('#form-produto').serialize();
				$('#btn-salvar').prop('disabled', true);
				$.ajax({
					url: URL+'produto/'+url,
					type: 'POST',
					data: txt,
					datatype: 'json',
					success:function(data){
						var json = JSON.parse(data);
						if(json.code == '200'){
							swal({
								  title: json.mensagem,
								  text:  'Cadastrar mais?',
								  type: 'success',
								  showCancelButton: true,
								  confirmButtonColor: '#3085d6',
								  cancelButtonColor: '#d33',
								  confirmButtonText: 'Sim',
								  cancelButtonText: 'Voltar',
								  allowOutsideClick: false
							}).then((result) => {
								  if (result.value) {
									$('form').trigger('reset');
									$('#btn-salvar').prop('disabled', false);
								  }else{
									  window.location.href = URL+"produto/";
								  }
							})
						}else if(json.code == '202'){
							swal({
								  title: json.mensagem,
								  type: 'success',
								  confirmButtonColor: '#3085d6',
								  confirmButtonText: 'OK'
							}).then((result) => {
								  window.location.href = URL+"produto/";
							})
						}else{
							swal({
								  title: json.mensagem,
								  type: 'error',
								  confirmButtonColor: '#3085d6',
								  confirmButtonText: 'OK'
							}).then((result) => {
								  $('#btn-salvar').prop('disabled', false );
							})
						}
						
					},
					error:function(data){
						alert(data);
					}
				});
				return false;  //This doesn't prevent the form from submitting.
			}
		});

		$("#form-sec").submit(function(e) {
			e.preventDefault();
		}).validate({
			rules: {
				nome : {
					required: true
				},
				fator: {
					required: true
				}
			},
			highlight: function (input) {
				$(input).parents('.form-group').addClass('has-error');
			},
			unhighlight: function (input) {
				$(input).parents('.form-group').removeClass('has-error');
			},
			errorPlacement: function (error, element) {
				$(element).parents('.form-group').append(error);
			},
			submitHandler: function(form) { 
				var url = $('form').data('url');
				var txt = $('form').serialize();
				$('#btn-salvar').prop('disabled', true);
				$.ajax({
					url: URL+'produto/'+url,
					type: 'POST',
					data: txt,
					datatype: 'json',
					success:function(data){
						var json = JSON.parse(data);
						if(json.code == '200'){
							swal({
								  title: json.mensagem,
								  text:  'Cadastrar mais?',
								  type: 'success',
								  showCancelButton: true,
								  confirmButtonColor: '#3085d6',
								  cancelButtonColor: '#d33',
								  confirmButtonText: 'Sim',
								  cancelButtonText: 'Voltar',
								  allowOutsideClick: false
							}).then((result) => {
								  if (result.value) {
									$('form').trigger('reset');
									$('#btn-salvar').prop('disabled', false);
								  }else{
									  window.location.href = URL+"produto/"+json.pagina;
								  }
							})
						}else if(json.code == '202'){
							swal({
								  title: json.mensagem,
								  type: 'success',
								  confirmButtonColor: '#3085d6',
								  confirmButtonText: 'OK'
							}).then((result) => {
								  window.location.href = URL+"produto/"+json.pagina;
							})
						}else{
							swal({
								  title: json.mensagem,
								  type: 'error',
								  confirmButtonColor: '#3085d6',
								  confirmButtonText: 'OK'
							}).then((result) => {
								  $('#btn-salvar').prop('disabled', false );
							})
						}
						
					},
					error:function(data){
						alert(data);
					}
				});
				return false;  //This doesn't prevent the form from submitting.
			}
		});

		
		$('.margem').keyup(function() {
			preco_custo = parseFloat($('#preco_custo').val().replace(',','.'));
			margem_lucro =  parseFloat($('#margem_lucro').val().replace(',','.'));
			preco_venda = parseFloat($('#preco_venda').val().replace(',','.'));
			if( preco_custo > 0 && margem_lucro > 0){
				preco_sugerido = preco_custo + (preco_custo * (margem_lucro /100) );
				$('#preco_sugerido').val(preco_sugerido.toFixed(2).replace('.',','));
				if(preco_venda > 0){
					margem_real = ( ( preco_venda - preco_custo ) / preco_custo) * 100 ;
					$('#margem_real').val((margem_real).toFixed(2).replace('.',','));
				}else{
					$('#margem_real').val();
				}
			}else{
				$('#preco_sugerido').val('');
			}
		});

		$('#preco_venda').keyup(function(){
			preco_custo = parseFloat($('#preco_custo').val().replace(',','.'));
			preco_venda = parseFloat($(this).val().replace(',','.'));
			if(preco_custo > 0 && preco_venda > 0){
				margem_real = ( ( preco_venda - preco_custo ) / preco_custo) * 100 ;
				$('#margem_real').val((margem_real).toFixed(2).replace('.',','));
			}else{
				$('#margem_real').val();
			}
		})


		$('.btn-excluir').on('click', function(){
		var id = $(this).data('id');
		var url = $(this).data('url');
		swal({
			title: 'Deletar',
			text: 'Tem certeza que deseja deletar?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sim',
			cancelButtonText: 'Não',
			showLoaderOnConfirm: true,
			preConfirm: function() {
			  return new Promise(function(resolve) {
			     $.ajax({
			   		url: URL+'produto/'+url+'/'+id,
			    	type: 'GET',
			       	dataType: 'json'
			     })
			     .done(function(response){
			     	swal({
						  title: response.mensagem,
						  type: 'success',
						  confirmButtonColor: '#3085d6',
						  confirmButtonText: 'OK'
					}).then((result) => {
						  window.location.href = URL+"produto/"+response.pagina;
					})
			     })
			     .fail(function(){
			     	swal('Oops...', 'Estamos com alguns problemas... tente novamente', 'error');
			     });
			  });
		    },
			allowOutsideClick: false			  
		});
	});

})


