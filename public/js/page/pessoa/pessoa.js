$(function(){

	$('#btn-add').on('click', function(){
		$('form').trigger('reset');
		$('form').attr('data-url','cadastrar');
		$('#modal-default').modal('show');
	});

	$('.btn-editar').on('click', function(){
		var id = $(this).attr('data-id');
		$('form').trigger('reset');
		$('#nome').val($(this).attr('data-nome'));
		$('form').attr('data-url','editar/'+id);
		$('#modal-default').modal('show');
	});

	$('.btn-deletar').on('click', function(){
		var id = $(this).attr('data-id');
		swal({
			title: 'Deletar pessoa',
			text: 'Tem certeza ?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sim',
			cancelButtonText: 'Não',
			showLoaderOnConfirm: true,
			  
			preConfirm: function() {
			  return new Promise(function(resolve) {
				 $.ajax({
							url: URL+'pessoa/deletar/'+id,
							type: 'GET',
							dataType: 'json'
						})
						.done(function(response){
							console.log(response);

							swal({
									title: response.mensagem,
									type: response.icon,
									confirmButtonColor: '#3085d6',
									confirmButtonText: 'OK'
								}).then((result) => {
									window.location.href = URL+"pessoa";
								})
						})
						.fail(function(){
							swal('Oops...', 'Estamos com alguns problemas... tente novamente', 'error');
						});
				});
			},
			allowOutsideClick: false			  
		});
	});

	$("#form-pessoa").submit(function(e) {
    	e.preventDefault();
	}).validate({
		rules: {
				nome : {
					required: true
				}
			},
			messages: {
				nome: {
					required: 'Campo obrigatório.'
				}
			},
			highlight: function (input) {
				$(input).parents('.form-group').addClass('has-error');
			},
			unhighlight: function (input) {
				$(input).parents('.form-group').removeClass('has-error');
			},
			errorPlacement: function (error, element) {
				$(element).parents('.form-group').append(error);
			},
			submitHandler: function(form) { 
				var url = $('form').data('url');
				var txt = $('form').serialize();
				$.ajax({
					url: URL+'pessoa/'+url,
					type: 'POST',
					data: txt,
					datatype: 'json',
					success:function(data){
						var json = JSON.parse(data);
						if(json.code == '200'){
							swal({
								  title: json.mensagem,
								  text:  'Cadastrar mais?',
								  type: 'success',
								  showCancelButton: true,
								  confirmButtonColor: '#3085d6',
								  cancelButtonColor: '#d33',
								  confirmButtonText: 'Sim',
								  cancelButtonText: 'Voltar',
								  allowOutsideClick: false
							}).then((result) => {
								  if (result.value) {
									$('form').trigger('reset');
								  }else{
									  window.location.href = URL+"pessoa/";
								  }
							})
						}else if(json.code == '202'){
							swal({
								  title: json.mensagem,
								  type: 'success',
								  confirmButtonColor: '#3085d6',
								  confirmButtonText: 'OK'
							}).then((result) => {
								  window.location.href = URL+"pessoa";
							})
						}else if(json.code == '300'){
							swal({
								  title: json.mensagem,
								  text:  'Tentar novamente?',
								  type: 'error',
								  showCancelButton: true,
								  confirmButtonColor: '#3085d6',
								  cancelButtonColor: '#d33',
								  confirmButtonText: 'Sim',
								  cancelButtonText: 'Voltar',
								  allowOutsideClick: false
							}).then((result) => {
								  if (result.value) {
									$('form').trigger('reset');
								  }else{
									  window.location.href = URL+"pessoa";
								  }
							})
						}
						
					},
					error:function(data){
						alert(data);
					}
				});
				return false;  //This doesn't prevent the form from submitting.
			}
	})

})