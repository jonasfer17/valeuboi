function setQuantidade(){
	if($('#quantidade').val() == ''){
			$('#quantidade').val('1');
		}
}

function insereProdutoClick(obj){
	var id = obj.getAttribute('data-id');
	var q = $('#quantidade').val();
	if(q <= 0 ){
		q = 1;
	}
	$.ajax({
		url: URL+'pdv/insereProdutoClick/'+id,
		type: 'GET',
		datatype: 'json',
		success:function(data){
			var json = JSON.parse(data);
			if(json.code == '200'){
				$('#produto').val(json.produto.gtin);
				$('#unitario').val(json.produto.preco_venda);
				$('#total').val((Number(json.produto.preco_venda) * Number(q)).toFixed(2));
				$('.searchresults').hide();
				$('#produto').focus();
			}else{
				swal('Produto não encontrado!');
			}
			
		},
		error:function(data){
			alert(data);	
		}
	});
}

function insereProdutoEnter(){
	var cod = $('#produto').val();
	var q = $('#quantidade').val();
	if(q <= 0 ){
		q = 1;
	}
	if(cod != ''){
		$.ajax({
			url: URL+'pdv/insereProdutoEnter/'+cod,
			type: 'GET',
			data: {q:q},
			datatype: 'json',
			success:function(data){
				var json = JSON.parse(data);
				if(json.code == '200'){
					$('#total-itens').html(json.info.info.total_produtos);
					$('#valor-desconto').html('R$ '+json.info.info.descontos);
					$('#valor-total').html('R$ '+json.info.info.valor_total);
					$('#tabela-produto').append('<tr id="'+json.id+'"><td class="text-center">'+json.produto.nome+'</td><td class="text-center">'+json.quantidade+'</td><td class="text-center">'+json.produto.preco_venda+'</td><td class="text-center">'+(Number(json.quantidade) * Number(json.produto.preco_venda)).toFixed(2)+'</td><td class="text-right"><button type="button" class="btn btn-sm btn-default btn-flat"><i class="fa fa-edit"></i></button><button type="button" class="btn btn-sm btn-default btn-flat"><i class="fa fa-trash" style="color:red;" onclick="excluirProduto(this);" data-id="'+json.id+'"></i></button></td>/tr>');
					$( "#tabela" ).scrollTop(3000);
					$('.searchresults').hide();
					$('#quantidade').val('');
					$('#produto').val('');
					$('#unitario').val('');
					$('#total').val('');
				}else{
					swal('Produto não encontrado!');
				}
			},
			error:function(data){
				swal('Produto não informado!');	
			}
		});
	}else{
		swal('Produto não informado!');
	}
	
}


function excluirProduto(obj){
	var id = obj.getAttribute('data-id');
	swal({
			title: 'Excluir produto da venda',
			text: 'Tem certeza que deseja excluir?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sim',
			cancelButtonText: 'Não',
			showLoaderOnConfirm: true,
			preConfirm: function() {
			  return new Promise(function(resolve) {
			     $.ajax({
			   		url: URL+'pdv/excluirProdutoVenda/'+id,
			    	type: 'GET',
			       	dataType: 'json'
			     })
			     .done(function(response){
                    if(response.code == '200'){
                        var linha = response.id;
                        $('#'+linha).remove();
                        $('#total-itens').html(response.info.info.total_produtos);
						$('#valor-desconto').html('R$ '+response.info.info.descontos);
						$('#valor-total').html('R$ '+response.info.info.valor_total);
						swal.close();
                    }else{
                        swal('Oops...', 'Estamos com alguns problemas... tente novamente', 'error');
                    }
                })
			     .fail(function(){
			     	swal('Oops...', 'Estamos com alguns problemas... tente novamente', 'error');
			     });
			  });
		    },
			allowOutsideClick: false			  
		});
}

function addFormaPagamento(obj){
	var id = obj.getAttribute('data-id');
	$.ajax({
		url: URL+'pdv/insereFormaPagamento/'+id,
		type: 'GET',
		datatype: 'json',
		success:function(data){
			var json = JSON.parse(data);
			var fp = 'fp'+json.forma.id;
			if(json.code == '200'){
					$('#box-pagamento').append('<div class="row pagamento"><div class="col-md-4"><h3>'+json.forma.nome+'</h3></div><div class="col-md-4"><h3><input onblur="atualizarValorPagamento(this);" autofocus="true" type="text" name="valor" class="form-control input-finalizar money" id="fp'+json.id+'" value="'+json.valor+'"></h3></div><div class="col-md-3 text-right" style="margin-top: 5px;"><h3><i class="fa fa-trash"></i></h3></div></div><hr>');
					$('#modal-forma-pagamento').hide();	
				}else{
					swal('Produto não encontrado!');
				}
			$('#modal-forma-pagamento').hide();	
		},
		error:function(data){
		}
	});
}


function atualizarValorPagamento(obj){
	var id = obj.getAttribute('id');
	var valor = $('#'+id+'').val().replace(',','.');
	var id_fp = id.replace('fp','');
	console.log(id_fp);
	if(Number(valor) > 0){
		$.ajax({
			url: URL+'pdv/setValorFormaPagamento/'+id_fp,
			type: 'POST',
			data: {valor,valor},
			datatype: 'json',
			success:function(data){
				var json = JSON.parse(data);
				if(json.code == '200'){
					$('#valor-recebido').html('R$ '+ somarPagamentos());
					$('#valor-desconto').html("R$ "+json.info.descontos);
					if(somarPagamentos().replace(',','.') >= (Number(json.info.valor_total) - Number(json.info.descontos))){
						var troco = Number(somarPagamentos().replace(',','.')) - (Number(json.info.valor_total) - Number(json.info.descontos));
						$('#valor-troco').html(troco.toFixed(2));
						$('#btn-finalizar').prop('disabled', false);
						trava = 1;
					}else{
						$('#valor-troco').html('R$ 0,00');
						$('#btn-finalizar').prop('disabled', true);
					} 
					$('.input-finalizar').blur();
				}else{
				
				}
			},
			error:function(data){
			}
		});
	}else{
		$('#valor-recebido').html('R$ '+ somarPagamentos());
		$('#valor-troco').html("R$ 0,00");
	}
}

function atualizarValorPagamentoId(id){
	var valor = $('#'+id+'').val().replace(',','.');
	var id_fp = id.replace('fp','');
	console.log(id_fp);
	if(Number(valor) > 0){
		$.ajax({
			url: URL+'pdv/setValorFormaPagamento/'+id_fp,
			type: 'POST',
			data: {valor,valor},
			datatype: 'json',
			success:function(data){
				var json = JSON.parse(data);
				if(json.code == '200'){
					$('#valor-recebido').html('R$ '+ somarPagamentos());
					$('#valor-desconto').html("R$ "+json.info.descontos);
					if(somarPagamentos().replace(',','.') >= (Number(json.info.valor_total) - Number(json.info.descontos))){
						var troco = Number(somarPagamentos().replace(',','.')) - (Number(json.info.valor_total) - Number(json.info.descontos));
						$('#valor-troco').html(troco.toFixed(2));
						$('#btn-finalizar').prop('disabled', false);
						trava = 1;
					}else{
						$('#valor-troco').html('R$ 0,00');
						$('#btn-finalizar').prop('disabled', true);
						trava = 0;
					} 
					$('.input-finalizar').blur();
				}else{
				
				}
			},
			error:function(data){
			}
		});
	}else{
		$('#valor-recebido').html('R$ '+ somarPagamentos());
		$('#valor-troco').html("R$ 0,00");
	}
}



function somarPagamentos(){
    var soma = 0;
    $('.box').find(":text:visible.input-finalizar").each(function(i) {
        soma += Number($(this).val().replace(',','.'));
        console.log(soma);
    });
    return soma.toFixed(2).replace('.',',');
}


var pressedCtrl = false; //variável de controle

$(document).keyup(function (e) {  //O evento Kyeup é acionado quando as teclas são soltas
	 	if(e.which == 17) pressedCtrl=false; //Quando qualuer tecla for solta é preciso informar que Crtl não está pressionada
})
$(document).keydown(function (e) { //Quando uma tecla é pressionada
	if(e.which == 17) pressedCtrl = true; //Informando que Crtl está acionado
	if((e.which == 13|| e.keyCode == 13) && pressedCtrl == true) { //Reconhecendo tecla Enter
		if(trava > 0){
			window.location.href = URL+"pdv/"+url_venda;
		}else{
			swal("Verifique os valores");
		}
	}
	if((e.which == 77|| e.keyCode == 77) && pressedCtrl == true) { //Reconhecendo tecla "m"
		window.location.href = URL+"pdv/opcoescaixa";
	}
});

$(function(){

	$( "#tabela" ).scrollTop(3000);
	$('form').on('keypress', function(e) {
  		var keyCode = e.keyCode || e.which;
	  	if (keyCode === 13) {
	  		if($('#produto').focus()){
	  			if($('#produto').val() != ''){
	  				insereProdutoEnter();
	  			}
	  			e.preventDefault();
	  			return false;
			}

	  		if($('#quantidade').focus()){
	  			$('#produto').focus();
	  			e.preventDefault();
	  			return false;
	  		}
	  	}
	  	if(keyCode === 82){
	  		alert('aaa');
	  	}
	});


	$('#quantidade').focusout(function(){
		setQuantidade();
	});

	$('#produto').keyup(function(){
		if($(this).val() == ''){
			$('.searchresults').hide();
		}else{
			var codigo = $(this).val();
			$.ajax({
					url: URL+'produto/pesquisarProduto/'+codigo,
					type: 'GET',
					datatype: 'json',
					success:function(data){
						var json = JSON.parse(data);
						if(json.code == '200'){
							if($('.searchresults').length == 0){
		                        $('#produto').after('<div class="searchresults"></div>');
		                    }
					        var html = '';
							$.each(json.produtos, function( index, value ){
								var form = "'form-pdv-produto'";
								html += '<a href="javascript:;" onclick="insereProdutoClick(this);" data-id="'+value.id+'"><div class="si">'+value.gtin+'<span class="text-center" style="margin-left: 80px;">'+value.nome.substring(0,30)+'</span></div></a>';
							})
							if(Object.keys(json.produtos).length == 1){
								var q = $('#quantidade').val();
								if(q <= 0 ){
									q = 1;
								}
								if(json.produtos[0].gtin == codigo){
									$('#unitario').val(json.produtos[0].preco_venda);
									$('#total').val((Number(json.produtos[0].preco_venda) * Number(q)).toFixed(2));
								}else{
									$('#unitario').val('');
									$('#total').val('');
								}
							}
							$('.searchresults').html(html);
							$('.searchresults').show();
						}else{
							$('.searchresults').remove();
							$('.searchresults').hide();
							$('#unitario').val('');
							$('#total').val('');
						}
					},
					error:function(data){
						//$('#prod option').remove();
						//$("#prod").append($('<option value="Produto não encontrado"></option>'));
					}
				});
		}
		
		
	});

	
	$('.select2').select2()
	

	$("#form-novo-caixa").submit(function(e) {
		e.preventDefault();
	}).validate({
		rules: {
			valor: {
				required: true
			}
		},
		highlight: function (input) {
			$(input).parents('.form-group').addClass('has-error');
		},
		unhighlight: function (input) {
			$(input).parents('.form-group').removeClass('has-error');
		},
		errorPlacement: function (error, element) {
			$(element).parents('.form-group').append(error);
		},
		submitHandler: function(form) { 
			var txt = $("#form-novo-caixa").serialize();
			$('#btn-salvar').prop('disabled', true);
			$.ajax({
				url: URL+'pdv/abrirCaixa',
				type: 'POST',
				data: txt,
				datatype: 'json',
				success:function(data){
					window.location.href = URL+"pdv/";
				},
				error:function(data){
					alert(data);
				}
			});
			return false;  //This doesn't prevent the form from submitting.
		}
	});

	$('.input-finalizar').on('keypress', function(e) {
  		var keyCode = e.keyCode || e.which;
	  	if (keyCode === 13) {
	  		var id = $(this).attr('id');
	  		atualizarValorPagamentoId(id);
	  	}
	  	
	});
	$('.input-finalizar').on('keydown', function(e) {
  		var keyCode = e.keyCode || e.which;
	  	if (keyCode === 9) {
	  		var id = $(this).attr('id');
	  		atualizarValorPagamentoId(id);
	  	}
	});

	$('#btn-fechar-caixa').click(function() {
		Swal({
			title: 'Fechar caixa',
			text: "Deseja fechar o caixa?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sim',
			cancelButtonText: 'Não'
		  }).then((result) => {
			if (result.value) {
				$.ajax({
					url: URL+"caixa/encerrarCaixa",
					type: "GET",
					datatype: 'json',
					success:function(data){
						var json = JSON.parse(data);
						if(json.code == '200'){
							window.location.href = URL+"caixa/infoFechamentoCaixa/"+json.id
						}else{
							alert(data);
						}
					},
					error:function(data){
	
					}
				})
			  }
		  })
	});
	  

})
/*
document.querySelector('body').addEventListener('keydown', function(event) {
 
    console.info( event.keyCode );
 
});
*/

