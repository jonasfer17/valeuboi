$(function(){


	//Initialize Select2 Elements
    $('.select2').select2()

    $('.repete').hide();

    $('.periodo').hide();

    $('.pago_em').hide();

    if($('#status').val() == '1'){
        $('.pago_em').show();
    }else{
        $('.pago_em').hide();
    }

    $('#status').change(function(){
    	if($(this).val() == '1'){
    		$('.pago_em').show();
    	}else{
    		$('.pago_em').hide();
    	}
    })

    $('#repete').change(function(){
    	if($(this).val() != 0){
    		$('.repete').show();
    		if($(this).val() >= '4'){
    			$('.periodo').show();
    		}else{
                $('.periodo').hide();
            }
    	}else{
    		$('.repete').hide();
    		$('.periodo').hide();
    	}
    });



    $('.date').datepicker({
        autoclose: true
      })

    $("#form-filtro-lancamento").submit(function(e) {
            e.preventDefault();
        }).validate({
            submitHandler: function(form) { 
                var txt = $('#form-filtro-lancamento').serialize();
                $.ajax({
                    url: URL+'financeiro/filtrarLancamentos',
                    type: 'POST',
                    data: txt,
                    datatype: 'json',
                    success:function(data){
                        var json = JSON.parse(data);
                        if(json.code == '200'){
                            $('#tabela-lancamento tr').remove();
                            var row = '';
                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth()+1; //January is 0!
                            var yyyy = today.getFullYear();
                            if(dd<10) {
                                dd = '0'+dd
                            } 
                            if(mm<10) {
                                mm = '0'+mm
                            } 
                            today = yyyy + '-' + mm + '-' + dd;
                            
                            $.each(json.lista, function( index, value ) {
                                if(today > value.vencimento && value.status == 0){
                                    status = '<span class="label label-danger">Atrasado</span>';
                                }else if(value.status == '0'){
                                    status = '<span class="label label-warning">Aberto</span>';
                                }else{
                                    status = '<span class="label label-success">Pago</span>';
                                }
                                row  = "<tr>";
                                row += "<td class='text-center'>"+setTipo(value.tipo) +"</td>";
                                row += "<td class='text-center'>"+value.descricao+"</td>";
                                row += "<td class='text-center'>"+value.nome+"</td>";
                                row += "<td class='text-center'>"+value.forma_pagamento+"</td>";
                                row += "<td class='text-center'>"+value.total.replace('.',',')+"</td>";
                                row += "<td class='text-center'>"+status+"</td>";
                                row += "<td class='text-center'>"+setData(value.vencimento)+"</td>";
                                row += '<td class="text-right">';
                                row += '<a href="'+URL+'financeiro/formularioLancamento/'+value.id+'">';
                                row += '<button class="btn btn-flat btn-default btn-editar"><i class="fa fa-cog"></i></button></a>';
                                //row += '<button class="btn btn-flat btn-danger btn-deletar"><i class="fa fa-trash" ></i></button></td>"';
                                row += "</tr>";
                                $("#tabela-lancamento").append(row);
                            });
                            $('#totalDespesa').html('R$ '+json.despesa);
                            $('#totalReceita').html('R$ '+json.receita);
                            
                        }else{
                            $('#tabela-lancamento tr').remove();
                            row  = "<tr>";
                            row += "<td colspan='6' class='text-center'>"+json.mensagem+"</td>";    
                            row += "</tr>";
                                $("#tabela-lancamento").append(row);
                        }
                        
                    },
                    error:function(data){
                    }
                });
                return false;  //This doesn't prevent the form from submitting.
            }
    })

     $("#form-lancamento").submit(function(e) {
            e.preventDefault();
        }).validate({
            rules: {
                descricao : {
                    required: true
                },
                valor : {
                    required: true
                },
                forma_pagamento: {
                    select: '0'
                },
                vencimento: {
                    date: true,
                    required: true
                },
                data_caixa: {
                    date: true,
                    required: true
                }
            },
            messages: {
                categoria: {
                    select: 'Selecione uma categoria'
                },
                forma_pagamento: {
                    select: 'Selecione uma categoria'
                }
            },
            highlight: function (input) {
                $(input).parents('.form-group').addClass('has-error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-group').removeClass('has-error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            },
            submitHandler: function(form) { 
                var url = $('form').data('url');
                var txt = $('form').serialize();
                $.ajax({
                    url: URL+'financeiro/'+url,
                    type: 'POST',
                    data: txt,
                    datatype: 'json',
                    success:function(data){
                        var json = JSON.parse(data);
                        if(json.code == '300'){
                            swal({
                                title: json.mensagem,
                                type: 'warning',
                                focusConfirm: false,
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'OK'
                            });
                        }else{
                            swal({
                                    title: json.mensagem,
                                    type: 'success',
                                    focusConfirm: false,
                                    confirmButtonColor: '#3085d6',
                                    confirmButtonText: 'OK',
                                    closeOnClickOutside: false
                                }).then((result) => {
                                    window.location.href = URL+"financeiro/lancamento/";
                                    });
                        }
                        
                    },
                    error:function(data){
                        alert(data);
                    }
                });
                return false;  //This doesn't prevent the form from submitting.
            }
    })


        $('#tabela').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : false,
            'info'        : false,
            'autoWidth'   : false,
            "language": {
                "paginate": {
                  "previous": "Anterior",
                  "next" : "Próximo"
                }
            }
        });
})

function setTipo(valor){
    if(valor == '0'){
        return "Receita";
    }else{
        return "Despesa";
    }
}