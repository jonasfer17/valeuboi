function btnEdit(obj){
    var id = obj.getAttribute('data-id');
    $('#form-caixa').trigger('reset');
    $('#form-caixa').attr('data-url','editarCaixa/'+id);
        $.ajax({
            url: URL+'financeiro/listarDetalheCaixaPorId/'+id,
            type: 'GET',
            datatype: 'json',
            success:function(data){
                var json = JSON.parse(data);
                if(json.code == '300'){
                    swal({
                        title: json.mensagem,
                        type: 'warning',
                        focusConfirm: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'OK'
                    });
                }else{
                    $.each(json, function( index, value ) {
                        $('#forma'+value.id_forma_pagamento).val(value.valor_total.replace('.',','));
                        $('#valor_total').val(value.total.replace('.',','));
                        $('#valor_caixa').val(value.caixa.replace('.',','));
                        $('#diferenca').val(value.diferenca.replace('.',','));
                        $('#obs').val(value.obs);
                        $('#saida').val(value.valor_saida.replace('.',','));
                        $('#fiado').val(value.valor_fiado.replace('.',','));
                        $('#total_cliente').val(value.total_cliente);
                        $('#ticket').val(value.media_cliente.replace('.',','));
                    });

                    $('#modal-default').modal('show');
                }
                
            },
            error:function(data){
                alert(data);
            }
        });
}

function btnInfo(obj){
    var id = obj.getAttribute('data-id');
        $.ajax({
            url: URL+'financeiro/infoCaixa/'+id,
            type: 'GET',
            datatype: 'json',
            success:function(data){
                var json = JSON.parse(data);
                $('#tabela-entrada tr').remove();
                $('#tabela-fiado tr').remove();
                $('#tabela-saida tr').remove();
                var liq = 0;
                // Tabela entrada
                $.each(json.caixa, function( index, value ) {
                    row = '<tr><td>'+value.forma_pagamento+'</td>';
                    row += '<td class="align-center">'+value.valor_total.replace('.',',')+'</td>';
                    row += '<td class="align-center">'+value.valor_liquido.replace('.',',')+'</td>';
                    row += '</tr>';
                    $("#tabela-entrada").append(row);
                    liq += Number(value.valor_liquido);
                });
                liq += Number(json.caixa[0].fiado);
                row = '<tr><td>Fiado</td>';
                row += '<td class="align-center">'+json.caixa[0].fiado.replace('.',',')+'</td>';
                row += '<td class="align-center">'+json.caixa[0].fiado.replace('.',',')+'</td>';
                row += '</tr>';
                row += '<tr><td><b>Total:</b></td>';
                row += '<td class="align-center">'+json.caixa[0].caixa.replace('.',',')+'</td>';
                row += '<td class="align-center">'+liq.toFixed(2).replace('.',',')+'</td>';
                row += '</tr>';
                $("#tabela-entrada").append(row);

                // Tabela Fiado
                var total_fiado = 0;
                $.each(json.fiado, function( index, value ) {
                    row = '<tr><td class="align-center">'+value.nome+'</td>';
                    row += '<td>'+value.valor.replace('.',',')+'</td>';
                    row += '</tr>';
                    $("#tabela-fiado").append(row);
                    total_fiado += Number(value.valor);
                });
                row = '<tr><td><b>Total:</b></td>';
                row += '<td class="align-center">'+total_fiado.toFixed(2).replace('.',',')+'</td>';
                row += '</tr>';
                $("#tabela-fiado").append(row);

                // Tabela Saida

                // Tabela Fiado
                var total_saida = 0;
                $.each(json.lancamento, function( index, value ) {
                    row = '<tr><td class="align-center">'+value.descricao+'</td>';
                    row += '<td>'+value.valor.replace('.',',')+'</td>';
                    row += '</tr>';
                    $("#tabela-saida").append(row);
                    total_saida += Number(value.valor);
                });
                row = '<tr><td><b>Total:</b></td>';
                row += '<td class="align-center">'+total_saida.toFixed(2).replace('.',',')+'</td>';
                row += '</tr>';
                $("#tabela-saida").append(row);
                

                $('#usuario').html("<b>Usuário: </b>"+json.caixa[0].login);
                $('#data').html();
                $('#modal-title').html('Informações do Caixa');
                    $('#modal-info').modal('show');
                
            },
            error:function(data){
                alert(data);
            }
        });
}


$(function(){
        $('#tabela').DataTable({
            'paging'      : false,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : false,
            'info'        : false,
            'autoWidth'   : true,
            "language": {
                "paginate": {
                    "previous": "Anterior",
                    "next" : "Próximo"
                }
            }
        });

        $('#btn-add').on('click', function(){
            $('#form-caixa').trigger('reset');
            $('#form-caixa').attr('data-url','novoCaixa');
            $('#modal-default').modal('show');
        });

        $('.date').datepicker({
            autoclose: true
        })

        

        function somarEntradas(){
            var soma = 0;
            $('#form-caixa').find(":text:visible.forma").each(function(i) {
                soma += Number($(this).val().replace(',','.'));
            });
            return soma.toFixed(2).replace('.',',');
        }

        function getDiferenca(){
            var diferenca = $('#valor_caixa').val().replace(',','.') -  $('#valor_total').val().replace(',','.');
            return diferenca.toFixed(2).replace(',','.');
        }

        function calculaDiferenca(){
            var valor;
            valor = $('#saida').val().replace(',','.');
            if(valor > 0){
                var saida = parseFloat(getDiferenca()) + parseFloat(valor)
            }else{
                var saida = parseFloat(getDiferenca());
            }  
            return saida.toFixed(2).replace('.',',');
        }



        $(".forma").keyup(function() {
            $('#valor_caixa').val(somarEntradas());
            $('#diferenca').val(calculaDiferenca());
        });

        $(".saida").keyup(function(){
            $('#diferenca').val(calculaDiferenca());
        })

        $('#valor_total').keyup(function(){
            if($('#valor_caixa').val().replace(',','.') >  0 ) {
                $('#diferenca').val(getDiferenca());
            }
            if($('#total_cliente').val() != ''){
                var media = ($('#valor_total').val().replace(',','.') / $('#total_cliente').val());
                $('#ticket').val(media.toFixed(2));
            }
        })


        $('#total_cliente').keyup(function(){
            if($('#valor_total').val().replace(',','.') > 0){
                if($(this).val() != ''){
                    var media = ($('#valor_total').val().replace(',','.') / $(this).val());
                    $('#ticket').val(media.toFixed(2));
                }else{
                    $('#ticket').val('');
                }
            }else{
                $('#ticket').val('0');
            }
        });

        $("#form-filtro-caixa").submit(function(e) {
            e.preventDefault();
        }).validate({
            submitHandler: function(form) { 
                var txt = $('#form-filtro-caixa').serialize();
                $.ajax({
                    url: URL+'financeiro/filtrarCaixa',
                    type: 'POST',
                    data: txt,
                    datatype: 'json',
                    success:function(data){
                        var json = JSON.parse(data);
                        if(json.code == '200'){
                            $('#tabela-caixa tr').remove();                            
                                $.each(json.caixa, function( index, value ) {
                                    row  = "<tr>";
                                    row += "<td>"+setData(value.data)+"</td>";
                                    row += "<td>"+value.valor_total.replace('.',',')+"</td>";
                                    row += "<td>"+value.valor_caixa.replace('.',',')+"</td>";
                                    row += "<td>"+value.valor_fiado.replace('.',',')+"</td>";
                                    row += "<td>"+value.valor_saida.replace('.',',')+"</td>";
                                    row += "<td>"+value.media_cliente.replace('.',',')+"</td>";
                                    row += '<td class="text-right"><button class="btn btn-flat btn-primary" onClick="btnInfo(this);"  data-id="'+value.id+'"><i class="fa fa-info"></i></button>';
                                    //row += ' <button class="btn btn-flat btn-default btn-editar" onClick="btnEdit(this);" data-id="'+value.id+'"><i class="fa fa-cog"></i></button>';
                                    //row += ' <button class="btn btn-flat btn-danger btn-deletar" onCLick="btnDelete(this);" data-id="'+value.id+'"><i class="fa fa-trash" ></i></button>';
                                    row += "</tr>";
                                    $("#tabela-caixa").append(row);
                                    $('#totalVenda').html('R$ '+json.detalhes.venda.replace('.',','));
                                    $('#totalCaixa').html('R$ '+json.detalhes.caixa.replace('.',','));
                                    $('#totalFiado').html('R$ '+json.detalhes.fiado.replace('.',','));
                                    $('#totalSaida').html('R$ '+json.detalhes.saida.replace('.',','));
                                    $('#mediaCliente').html('R$ '+Number(json.detalhes.media).toFixed(2).replace('.',','));
                                });
                            
                        }else{
                            $('#tabela-caixa tr').remove();
                            row  = "<tr>";
                            row += "<td colspan='6' class='text-center'>"+json.mensagem+"</td>";    
                            row += "</tr>";
                                $("#tabela-caixa").append(row);
                                $('#totalVenda').html('R$ 0,00');
                                $('#totalCaixa').html('R$ 0,00');
                                $('#totalFiado').html('R$ 0,00');
                                $('#totalSaida').html('R$ 0,00');
                                $('#mediaCliente').html('R$ 0,00');
                        }
                        
                    },
                    error:function(data){
                    }
                });
                return false;  //This doesn't prevent the form from submitting.
            }
    })
    
    
        $("#form-caixa").submit(function(e) {
            e.preventDefault();
        }).validate({
            rules: {
                    total_venda : {
                        required: true
                    },
                    data : {
                        required: true,
                        date: true
                    }
                },
                highlight: function (input) {
                    $(input).parents('.form-group').addClass('has-error');
                },
                unhighlight: function (input) {
                    $(input).parents('.form-group').removeClass('has-error');
                },
                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error);
                },
                submitHandler: function(form) { 
                    var url = $('#form-caixa').data('url');
                    var txt = $('#form-caixa').serialize();
                    $.ajax({
                        url: URL+'financeiro/'+url,
                        type: 'POST',
                        data: txt,
                        datatype: 'json',
                        success:function(data){
                            var json = JSON.parse(data);
                            if(json.code == '300'){
                                swal({
                                    title: json.mensagem,
                                    type: 'warning',
                                    focusConfirm: false,
                                    confirmButtonColor: '#3085d6',
                                    confirmButtonText: 'OK'
                                });
                            }else{
                                swal({
                                        title: json.mensagem,
                                        type: 'success',
                                        focusConfirm: false,
                                        confirmButtonColor: '#3085d6',
                                        confirmButtonText: 'OK',
                                        closeOnClickOutside: false
                                    }).then((result) => {
                                        window.location.href = URL+"financeiro/caixa/";
                                        });
                            }
                            
                        },
                        error:function(data){
                            alert(data);
                        }
                    });
                    return false;  //This doesn't prevent the form from submitting.
                }
        })
    


        
    
    })

