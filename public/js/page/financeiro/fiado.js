function btnEdit(obj){
    var id = obj.getAttribute('data-id');
    $('.form-modal').trigger('reset');
    $('.form-modal').attr('data-url','editarFiado/'+id);
        $.ajax({
            url: URL+'financeiro/listarDetalheFiadoPorId/'+id,
            type: 'GET',
            datatype: 'json',
            success:function(data){
                var json = JSON.parse(data);
                if(json.code == '300'){
                    swal({
                        title: json.mensagem,
                        type: 'warning',
                        focusConfirm: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'OK'
                    });
                }else{
                    $('#data').val(setData(json.data_caixa));
                    $('#vencimento').val(setData(json.vencimento));
                    $('#obs').val(json.obs);
                    $('#status').val(json.status).change();
                    $('#cliente').val(json.id_ator).change();
                    $('#valor').val(json.valor.replace('.',','));
                    if(json.data_pagamento != null){
                        $('#pagamento').val(setData(json.data_pagamento));
                    }
                    $('#modal-default').modal('show');
                }
                
            },
            error:function(data){
                alert(data);
            }
        });
}

function btnDelete(obj){
    var id = obj.getAttribute('data-id');
    swal({
        title: 'Deletar fiado',
        text: 'Tem certeza que deseja deletar?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sim',
        cancelButtonText: 'Não',
        showLoaderOnConfirm: true,
        preConfirm: function() {
            return new Promise(function(resolve) {
                $.ajax({
                url: URL+'financeiro/deletarfiado/'+id,
                type: 'GET',
                dataType: 'json'
                })
                .done(function(response){
                swal({
                    title: 'Deletado',
                    type: 'success',
                    focusConfirm: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK',
                    closeOnClickOutside: false
                }).then((result) => {
                    window.location.href = URL+"financeiro/fiado/";
                    });
                
                })
                .fail(function(){
                swal('Oops...', 'Estamos com alguns problemas... tente novamente', 'error');
                });
            });
        },
        allowOutsideClick: false              
    });
}



$(function(){

    $('#tabela').DataTable({
        'paging'      : false,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : false,
        'info'        : false,
        'autoWidth'   : true,
        "language": {
            "paginate": {
                "previous": "Anterior",
                "next" : "Próximo"
            }
        }
    });

    $('#pag').hide();

    $('#status').change(function(){
        if($(this).val() == '0'){
            $('#pag').hide();
        }else{
            $('#pag').show();
        }
    });

    $('#btn-add').on('click', function(){
        $('.form-modal').trigger('reset');
        $('.form-modal').attr('data-url','novoFiado');
        $('#modal-default').modal('show');
    });

     $("#form-filtro-fiado").submit(function(e) {
            e.preventDefault();
        }).validate({
            submitHandler: function(form) { 
                var txt = $('#form-filtro-fiado').serialize();
                $.ajax({
                    url: URL+'financeiro/filtrarFiado',
                    type: 'POST',
                    data: txt,
                    datatype: 'json',
                    success:function(data){
                        var json = JSON.parse(data);
                        if(json.code == '200'){
                            $('#tabela-fiado tr').remove();
                            var row = '';
                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth()+1; //January is 0!
                            var yyyy = today.getFullYear();
                            if(dd<10) {
                                dd = '0'+dd
                            } 
                            if(mm<10) {
                                mm = '0'+mm
                            } 
                            today = yyyy + '-' + mm + '-' + dd;
                            
                            $.each(json.fiado, function( index, value ) {
                                if(today > value.vencimento && value.status == 0){
                                    status = '<span class="label label-danger">Atrasado</span>';
                                }else if(value.status == '0'){
                                    status = '<span class="label label-warning">Aberto</span>';
                                }else{
                                    status = '<span class="label label-success">Pago</span>';
                                }
                                row  = "<tr>";
                                row += "<td>"+value.nome+"</td>";
                                row += "<td>"+value.valor.replace('.',',')+"</td>";
                                row += "<td>"+setData(value.data_caixa)+"</td>";
                                row += "<td>"+setData(value.vencimento)+"</td>";
                                row += '<td>'+status+'</td>';
                                row += '<td class="text-right"><button class="btn btn-flat btn-default btn-editar" onClick="btnEdit(this);" data-id="'+value.id+'"><i class="fa fa-cog"></i></button>';
                                row += ' <button class="btn btn-flat btn-danger btn-deletar" onCLick="btnDelete(this);" data-id="'+value.id+'"><i class="fa fa-trash" ></i></button></td>"';
                                row += "</tr>";
                                $("#tabela-fiado").append(row);
                                $('#total_aberto').html('R$ '+json.total_aberto.replace('.',','));
                                $('#total_vencido').html('R$ '+json.total_vencido.replace('.',','));
                                $('#total_pago').html('R$ '+json.total_pago.replace('.',','));
                                $('#total_fiado').html('R$ '+json.total_fiado.replace('.',','));
                            });
                            
                        }else{
                            $('#tabela-fiado tr').remove();
                            row  = "<tr>";
                            row += "<td colspan='6' class='text-center'>"+json.mensagem+"</td>";    
                            row += "</tr>";
                                $("#tabela-fiado").append(row);
                            $('#total_aberto').html('R$ 0,00');
                            $('#total_vencido').html('R$ 0,00');
                            $('#total_pago').html('R$ 0,00');
                            $('#total_fiado').html('R$ 0,00');
                        }
                        
                    },
                    error:function(data){
                    }
                });
                return false;  //This doesn't prevent the form from submitting.
            }
    })

    $('.date').datepicker({
        autoclose: true
      })

     $("#form-fiado").submit(function(e) {
            e.preventDefault();
        }).validate({
        rules: {
                valor : {
                    required: true
                },
                vencimento: {
                    required: true
                },
                cliente: {
                    select: '0'
                },
                pagamento: {
                    date: true,
                    required: true
                }
            },
            highlight: function (input) {
                $(input).parents('.form-group').addClass('has-error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-group').removeClass('has-error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            },
            submitHandler: function(form) { 
                var url = $('#form-fiado').data('url');
                var txt = $('#form-fiado').serialize();
                $.ajax({
                    url: URL+'financeiro/'+url,
                    type: 'POST',
                    data: txt,
                    datatype: 'json',
                    success:function(data){
                        var json = JSON.parse(data);
                        if(json.code == '300'){
                            swal({
                                title: json.mensagem,
                                type: 'warning',
                                focusConfirm: false,
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'OK'
                            });
                        }else if(json.code == '200'){
                            swal({
                                    title: json.mensagem,
                                    type: 'success',
                                    focusConfirm: false,
                                    confirmButtonColor: '#3085d6',
                                    confirmButtonText: 'OK',
                                    closeOnClickOutside: false
                                }).then((result) => {
                                    window.location.href = URL+"financeiro/fiado/";
                                    });
                        }else if(json.code == '202'){
                            swal({
                                title: json.mensagem,
                                type: 'success',
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'OK'
                            }).then((result) => {
                                window.location.href = URL+"financeiro/fiado";
                            })
                        }
                        
                    },
                    error:function(data){
                        alert(data);
                    }
                });
                return false;  //This doesn't prevent the form from submitting.
            }
    })


});