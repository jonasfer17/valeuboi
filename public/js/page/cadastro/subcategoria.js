$(function(){

	$("#form-subcategoria").submit(function(e) {
    	e.preventDefault();
	}).validate({
	    rules: {
            nome : {
                required: true
			},
			categoria: {
				select: '0'
			}
        },
        messages: {
            nome: {
                required: 'Campo obrigatório.'
            },
			categoria: {
				select: 'Selecione uma categoria'
			}
        },
        highlight: function (input) {
            $(input).parents('.form-group').addClass('has-error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        },
	    submitHandler: function(form) { 
	    	var url = $('#form-subcategoria').data('url');
	    	var txt = $('#form-subcategoria').serialize();
	        $.ajax({
	        	url: URL+'cadastro/'+url,
	        	type: 'POST',
	        	data: txt,
	        	datatype: 'json',
	        	success:function(data){
	        		var json = JSON.parse(data);
	        		if(json.codigo == '200'){
	        			swal({
						  	title: json.mensagem,
						  	text:  'Cadastrar mais?',
						  	type: 'success',
						  	showCancelButton: true,
						  	confirmButtonColor: '#3085d6',
						  	cancelButtonColor: '#d33',
						  	confirmButtonText: 'Sim',
						  	cancelButtonText: 'Voltar',
						  	allowOutsideClick: false
						}).then((result) => {
						  	if (result.value) {
						    	$('form').trigger('reset');
						  	}else{
						  		window.location.href = URL+"cadastro/subcategoria";
						  	}
						})
	        		}else if(json.codigo == '202'){
	        			swal({
						  	title: json.mensagem,
						  	type: 'success',
						  	confirmButtonColor: '#3085d6',
						  	confirmButtonText: 'OK'
						}).then((result) => {
						  	window.location.href = URL+"cadastro/subcategoria";
						})
	        		}else{
	        			swal({
						  	title: json.mensagem,
						  	type: 'error',
						  	confirmButtonColor: '#3085d6',
						  	confirmButtonText: 'OK'
						}).then((result) => {
						  	window.location.href = URL+"cadastro/subcategoria";
						})
	        		}
	        		
	        	},
	        	error:function(data){
	        		alert(data);
	        	}
	        });
	        return false;  //This doesn't prevent the form from submitting.
	    }
	});

	$('.btn-excluir-subcategoria').on('click', function(){
		var id = $(this).data('id');
		swal({
			title: 'Deletar permissão',
			text: 'Tem certeza que deseja deletar?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sim',
			cancelButtonText: 'Não',
			showLoaderOnConfirm: true,
			preConfirm: function() {
			  return new Promise(function(resolve) {
			     $.ajax({
			   		url: URL+'cadastro/deletarSubCategoria/'+id,
			    	type: 'GET',
			       	dataType: 'json'
			     })
			     .done(function(response){
			     	swal(response.mensagem, '' ,'success');
			     	var linha = '#'+id;
                    $(linha).remove();
			     })
			     .fail(function(){
			     	swal('Oops...', 'Estamos com alguns problemas... tente novamente', 'error');
			     });
			  });
		    },
			allowOutsideClick: false			  
		});
	});

})