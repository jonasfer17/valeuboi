$(function(){
    
        $('#btn-add').on('click', function(){
            $('form').trigger('reset');
            $('form').attr('data-url','cadastrarFormaPagamento');
            $('#modal-default').modal('show');
        });
    
        $('.btn-editar').on('click', function(){
            var id = $(this).attr('data-id');
            $('form').trigger('reset');
            $('form').attr('data-url','editarFormaPagamento/'+id);
            $('#modal-default').modal('show');
        });
    
        $('.btn-deletar').on('click', function(){
            var id = $(this).attr('data-id');
            swal({
                title: 'Deletar forma de pagamento',
                text: 'Tem certeza ?',
                type: 'warning',
                showCancelButton: true,
                focusConfirm: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sim',
                cancelButtonText: 'Não',
                showLoaderOnConfirm: true,
                  
                preConfirm: function() {
                  return new Promise(function(resolve) {
                     $.ajax({
                                url: URL+'cadastro/deletarFormaPagamento/'+id,
                                type: 'GET',
                                dataType: 'json'
                            })
                            .done(function(response){
                                console.log(response);
    
                                swal({
                                        title: response.mensagem,
                                        type: response.icon,
                                        focusConfirm: false,
                                        confirmButtonColor: '#3085d6',
                                        confirmButtonText: 'OK'
                                    }).then((result) => {
                                        window.location.href = URL+"cadastro/formapagamento";
                                    })
                            })
                            .fail(function(){
                                swal('Oops...', 'Estamos com alguns problemas... tente novamente', 'error');
                            });
                    });
                },
                allowOutsideClick: false        
            });
        });
    
        $("#form-f-pagamento").submit(function(e) {
            e.preventDefault();
        }).validate({
            rules: {
                    nome : {
                        required: true
                    },
                    taxa : { 
                      required: true
                  },
                },
                messages: {
                    nome: {
                        required: 'Campo obrigatório.'
                    },
                    taxa: {
                      required: 'Taxa obrigatória.'
                    },
                },
                highlight: function (input) {
                    $(input).parents('.form-group').addClass('has-error');
                },
                unhighlight: function (input) {
                    $(input).parents('.form-group').removeClass('has-error');
                },
                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error);
                },
                submitHandler: function(form) { 
                    var url = $('form').data('url');
                    var txt = $('form').serialize();
                    $.ajax({
                        url: URL+'cadastro/'+url,
                        type: 'POST',
                        data: txt,
                        datatype: 'json',
                        success:function(data){
                            var json = JSON.parse(data);
                            if(json.codigo == '200'){
                                swal({
                                      title: json.mensagem,
                                      text:  'Cadastrar mais?',
                                      type: 'success',
                                      showCancelButton: true,
                                      confirmButtonColor: '#3085d6',
                                      cancelButtonColor: '#d33',
                                      focusConfirm: false,
                                      confirmButtonText: 'Sim',
                                      cancelButtonText: 'Voltar',
                                      allowOutsideClick: false
                                }).then((result) => {
                                      if (result.value) {
                                        $('form').trigger('reset');
                                      }else{
                                          window.location.href = URL+"cadastro/formapagamento";
                                      }
                                })
                            }else if(json.codigo == '202'){
                                swal({
                                      title: json.mensagem,
                                      type: 'success',
                                      confirmButtonColor: '#3085d6',
                                      focusConfirm: false,
                                      confirmButtonText: 'OK'
                                }).then((result) => {
                                      window.location.href = URL+"cadastro/formapagamento";
                                })
                            }else if(json.codigo == '300'){
                                swal({
                                      title: json.mensagem,
                                      text:  'Tentar novamente?',
                                      type: 'error',
                                      showCancelButton: true,
                                      confirmButtonColor: '#3085d6',
                                      cancelButtonColor: '#d33',
                                      focusConfirm: false,
                                      confirmButtonText: 'Sim',
                                      cancelButtonText: 'Voltar',
                                      allowOutsideClick: false
                                }).then((result) => {
                                      if (result.value) {
                                        $('form').trigger('reset');
                                      }else{
                                          window.location.href = URL+"cadastro/formapagamento";
                                      }
                                })
                            }
                            
                        },
                        error:function(data){
                            alert(data);
                        }
                    });
                    return false;  //This doesn't prevent the form from submitting.
                }
        })
    
    })