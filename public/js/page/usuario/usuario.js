$(function(){

    $('#select-empresa').on('change', function(){
        $("#select-funcionario option").remove();
        var id = $(this).val();
        $.ajax({
            url: URL+'funcionario/listarFuncionariosPorEmpresa/'+id,
            type: 'GET',
            datatype: 'json',
            success:function(data){
                var json = JSON.parse(data);
                alert(id);
                $("#select-funcionario").append('<option value="0">Selecione</option>');
                $.each(json, function (i, x) {
                   var linha = '<option value="'+x.id+'">'+x.nome+'</option>';
                   $("#select-funcionario").append(linha);
                });
            },
            error:function(data){
                console.log(data);
            }
        });
    });

})