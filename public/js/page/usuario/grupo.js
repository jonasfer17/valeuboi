$(function(){
	
		$("#form-grupo").submit(function(e) {
			e.preventDefault();
		}).validate({
			rules: {
				nome : {
					required: true
				},
				'permissao[]' :{
					required: true
				}
			},
			messages: {
				nome: {
					required: 'Campo obrigatório.'
				}
			},
			highlight: function (input) {
				$(input).parents('.form-group').addClass('has-error');
			},
			unhighlight: function (input) {
				$(input).parents('.form-group').removeClass('has-error');
			},
			errorPlacement: function (error, element) {
				$(element).parents('.form-group').append(error);
			},
			submitHandler: function(form) { 
				var url = $('#form-grupo').data('url');
				var txt = $('#form-grupo').serialize();
				$.ajax({
					url: URL+'usuario/'+url,
					type: 'POST',
					data: txt,
					datatype: 'json',
					success:function(data){
						var json = JSON.parse(data);
						if(json.code == '200'){
							swal({
								  title: json.mensagem,
								  text:  'Cadastrar mais?',
								  type: 'success',
								  showCancelButton: true,
								  confirmButtonColor: '#3085d6',
								  cancelButtonColor: '#d33',
								  confirmButtonText: 'Sim',
								  cancelButtonText: 'Voltar',
								  allowOutsideClick: false
							}).then((result) => {
								  if (result.value) {
									$('input').iCheck('uncheck');
									$('form').trigger('reset');
								  }else{
									  window.location.href = URL+"usuario/grupo";
								  }
							})
						}else if(json.code == '202'){
							swal({
								  title: json.mensagem,
								  type: 'success',
								  confirmButtonColor: '#3085d6',
								  confirmButtonText: 'OK'
							}).then((result) => {
								  window.location.href = URL+"usuario/grupo";
							})
						}
						
					},
					error:function(data){
						alert(data);
					}
				});
				return false;  //This doesn't prevent the form from submitting.
			}
		});
	
		$('.btn-excluir-grupo').on('click', function(){
			var id = $(this).data('id');
			swal({
				title: 'Deletar permissão',
				text: 'Tem certeza que deseja deletar?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sim',
				cancelButtonText: 'Sim',
				showLoaderOnConfirm: true,
				  
				preConfirm: function() {
				  return new Promise(function(resolve) {
					 $.ajax({
						   url: URL+'usuario/deletarGrupo/'+id,
						type: 'GET',
						   dataType: 'json'
					 })
					 .done(function(response){
						 swal(response.mensagem, '' ,'success');
						 var linha = '#'+id;
						$(linha).remove();
					 })
					 .fail(function(){
						 swal('Oops...', 'Estamos com alguns problemas... tente novamente', 'error');
					 });
				  });
				},
				allowOutsideClick: false			  
			});
		});
	
	})