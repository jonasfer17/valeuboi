$(function(){
  
    /*
     * DONUT CHART
     * -----------
     

    var recebimentoData = [
      { label: '20%', data: 20, color: '#3c8dbc' },
      { label: '', data: 80, color: '#d2d6de' }
    ]
    $.plot('#recebimento-chart', recebimentoData, {
      series: {
        pie: {
          show       : true,
          radius     : 1,
          innerRadius: 0.7,
          label      : {
            show     : true,
            radius   : 0 / 3,
            formatter: labelFormatter,
            threshold: 0.1
          }

        }
      },
      legend: {
        show: false
      }
    })

    var despesaData = [
      { label: '100%', data: 100, color: '#dd4b39' },
      { label: '', data: 0, color: '#d2d6de' }
    ]
    $.plot('#despesa-chart', despesaData, {
      series: {
        pie: {
          show       : true,
          radius     : 1,
          innerRadius: 0.7,
          label      : {
            show     : true,
            radius   : 0 / 3,
            formatter: labelFormatter,
            threshold: 0.1
          }

        }
      },
      legend: {
        show: false
      }
    })
    /*
     * END DONUT CHART
     
    function labelFormatter(label, series) {
      return '<div style="font-size:24px; text-align:center; padding:12px; color: #000; font-weight: 600;">'
        + label
        + '<br>'
    }
    
    // The Calender
    $('#calendar').datepicker('setDate', 'today');
    
    */
})


function getInfoCaixa(obj){
    var id = obj.getAttribute('data-id');
    $.ajax({
        url: URL+'caixa/getInfoCaixa/'+id,
        type: 'GET',
        datatype: 'json',
        success:function(data){
            var json = JSON.parse(data);
            var caixa = json.caixa;
            if(json.code == '200'){
                $('#modal-title').html('Caixa do dia '+ caixa.data_abertura);
                $('#sp_usuario').html(caixa.login);
                $('#sp_dt_abertura').html(caixa.data_abertura);
                $('#sp_hr_abertura').html(caixa.hora_abertura);
                $('#sp_dt_fechamento').html(caixa.data_fechamento);
                $('#sp_hr_fechamento').html(caixa.hora_fechamento);
                $('#sp_fundo_caixa').html("R$ "+ caixa.fundo_caixa.replace('.',','));
                $('#sp_desconto').html('R$ '+ caixa.total_desconto.replace('.',','));
                $('#sp_venda').html('R$ ' + caixa.total_vendas.replace('.',','));
                $('#sp_total').html('R$ '+ caixa.total_geral.replace('.',','));
                $('#modal-info-caixa').modal('show');
                
			}else{
				swal('Produto não encontrado!');
			}
        }
    })
}
