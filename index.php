<?php

define('BASE', dirname(__FILE__));

require 'vendor/autoload.php';

// Carrega os dados iniciais da aplicação
require 'app/config/config.php';


// Carrega as classes da aplicação
require 'app/libs/application.php';
require 'app/libs/controller.php';

// inicia uma sessão
session_start();
header ('Content-type: text/html; charset=utf-8');
// Inicia a aplicação
$app = new Application();


// Ao mudar os diretórios do sistema
// Verificar no .htacess da raiz do projeto 