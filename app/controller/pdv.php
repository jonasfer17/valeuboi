<?php

class PDV extends Controller{

	private $venda = null;
	
	function __construct(){
		parent::__construct();
		if(!$this->estaLogado()){
			header("Location: ".URL."login");
		}
		$this->venda = $this->loadModel('VendaModel');
	}

	public function index(){
		$dados = array();
		$dados = $this->loadDados('','pdv');
		$dados['permissao'] = $this->permissoes();
		$dados['script'] = 'page/pdv.js';
		$caixaAberto = json_decode($this->venda->getCaixaAberto(), true);
		if($caixaAberto['code'] == '200'){
			$dados['venda'] = json_decode($this->venda->getVendaAberta(), true);
			// Se venda não estiver vazia
			if($dados['venda']['code'] == '200'){
				$dados['detalhe'] = $dados['venda']['venda'];
				$dados['cabecalho'] = $this->venda->getVendaCabecalho();
			}
			$this->loadTemplatePDV('pdv/home', $dados);
		}else{
			$this->loadTemplatePDV('pdv/novoCaixa', $dados);
		}
	}

	public function finalizarVenda(){
		$dados = array();
		$forma_pagamento = $this->loadModel("FormaPagamentoModel");
		$dados = $this->loadDados('','pdv');
		$dados['permissao'] = $this->permissoes();
		$dados['script'] = 'page/pdv.js';
		$dados['venda'] = json_decode($this->venda->getFormaPagamento(), true);
		$dados['forma_pagamento'] = $forma_pagamento->listar();
		$dados['info'] = json_decode($this->venda->getInfoVenda($_SESSION['idVenda']), true);
		$dados['valor_recebido'] = $this->venda->getValorRecebidoVenda();
		$this->loadTemplatePDV('pdv/finalizarVenda', $dados);
	}

	public function setFormaPagamento($id = ''){
		if(isset($_SESSION['idVenda']) && !empty($_SESSION['idVenda'])){
			header("Location: ".URL."pdv/finalizarVenda");
		}else{
			header("Location: ".URL."pdv");
		}
	}

	public function abrirCaixa(){
		$caixa = $this->loadModel("CaixaModel");
		echo $caixa->abrirCaixa();
	}

	public function insereProdutoClick($id){
		$produto = $this->loadModel('ProdutoModel');
		echo $produto->listarPorId($id);
	}

	public function insereProdutoEnter($cod){
		$produto = $this->loadModel('ProdutoModel');
		$prod = json_decode($produto->pesquisarProdutoPorCodigo($cod), true);
		if($prod['code'] == '200'){
		echo $this->venda->insereProduto($prod['produto']['id'], $prod['produto'], str_replace(',', '.', $_GET['q']));
		}else{
			echo json_encode(
                array(
                    "code" => '204',
                    "mensagem" => 'Produto não encontrado'
                )
            );
		}
	}

	public function novaVenda(){
		return json_decode($this->venda->novaVenda(), true);
	}

	public function excluirProdutoVenda($id_detalhe){
		echo $this->venda->excluirProdutoVenda($id_detalhe);
	}

	public function insereFormaPagamento($id = ''){
		if($id > 0){
			echo $this->venda->insereFormaPagamento($id);
		}else{
			echo json_encode( array('code' => '300', 'mensagem' => 'Erro' ));
		}	
	}

	public function setValorFormaPagamento($id = ''){
		if($id > 0){
			echo $this->venda->setValorFormaPagamento($id);
		}else{
			echo json_encode( array('code' => '300', 'mensagem' => 'Erro' ));
		}	
	}

	public function encerrarVenda(){
		if(isset($_SESSION['idVenda'])){
			$dados = array();
			$dados = $this->loadDados('','pdv');
			$dados['permissao'] = $this->permissoes();
			$dados['script'] = 'page/pdv.js';
			$dados['info'] = json_decode($this->venda->getInfoVenda($_SESSION['idVenda']), true);
			$dados['valor_recebido'] = $this->venda->getValorRecebidoVenda();
			$valor_venda = $dados['info']['info']['valor_total'] - $dados['info']['info']['descontos'];
			$valor_recebido = $dados['valor_recebido']['valor'];
			if(  $valor_venda > $valor_recebido ){
				header("Location: ".URL."pdv/finalizarVenda/");
			}else{
				$venda = json_decode($this->venda->encerrarVenda(), true);
				if($venda['code'] == '200'){
					unset($_SESSION['idVenda']);
					$dados['id'] = $venda['id'];
				}else{
					header("Location: ".URL."pdv/finalizarVenda/");
				}
				$this->loadTemplatePDV('pdv/encerrarVenda', $dados);	
			}
		}else{
			header("Location: ".URL."pdv/");
		}
		
	}

	public function imprimirCupom($id){
		try{
    		$dados = array();
    		$empresa = $this->loadModel('atorModel');
    		$forma_pagamento = $this->loadModel("FormaPagamentoModel");
    		$dados['venda'] = json_decode($this->venda->dadosVenda($id), true);
    		if($dados['venda']['code'] == '200'){
    			$dados['empresa'] = json_decode($empresa->getDadosEmpresa(), true);
    			$dados['usuario'] = json_decode($empresa->getDadosUsuario($dados['venda']['venda']['id_usuario']), true);
    			$dados['forma_pagamento'] = $forma_pagamento->listarPorId($dados['venda']['venda']['id_forma_pagamento']);
    			$dados['produtos'] = json_decode($this->venda->getProdutosVenda($dados['venda']['venda']['id']), true);
    			$this->loadPDF('pdv/pdfCupom', $dados);
    		}
		}catch(Exception $e){
			print_r($e);
		}
	}

	public function opcoesCaixa(){
		$dados = array();

		$this->loadTemplatePDV('pdv/opcoesCaixa');
	}

}