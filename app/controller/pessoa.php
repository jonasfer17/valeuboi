<?php

class Pessoa extends Controller{

	private $pessoa = null;

    function __construct(){
		parent::__construct();
		if(!$this->estaLogado()){
			header("Location: ".URL."login");
		}
		$this->pessoa = $this->loadModel("AtorModel");
	}

	public function index(){
		$dados = array();
		$dados = $this->loadDados('', 'pessoa');
		$dados['permissao'] = $_SESSION['permissoes'];
		$dados['script'] = 'page/pessoa/pessoa.js';
		$dados['pessoa'] = $this->pessoa->listar();
		$this->loadTemplate('pessoas/home', $dados);
	}

	public function info($id){
		$dados = array();
		$dados['permissao'] = $this->permissoes();
		$this->loadTemplate('pessoas/cliente/info', $dados);
	}

	public function cadastrar(){
		echo $this->pessoa->cadastrar();
	}

	public function editar($id){
		echo $this->pessoa->editar($id);
	}

	public function deletar($id){
		echo $this->pessoa->deletar($id);
	}

}