<?php

class Cadastro extends Controller{

	private $forma_pagamento = null;
	private $categoria = null;
	private $subcategoria = null;
	private $cartao = null;
	
	function __construct(){
		parent::__construct();
		if(!$this->estaLogado()){
			header("Location: ".URL."login");
		}
		$this->categoria = $this->loadModel('CategoriaModel');
		$this->subcategoria = $this->loadModel('SubCategoriaModel');
		$this->forma_pagamento = $this->loadModel('FormaPagamentoModel');
		$this->cartao = $this->loadModel('CartaoModel');
	}

	public function index(){
		$dados = array();
		$dados['permissao'] = $this->permissoes();
		$this->loadTemplate('admin/home', $dados);
	}

	/* ==== Categoria === */

	public function categoria(){
		$dados = array();
		$dados = $this->loadDados('','cadastros', 'categoria');
		$dados['permissao'] = $this->permissoes();
		$dados['script'] = 'page/cadastro/categoria.js';
		$dados['categoria'] = $this->categoria->listar();
		$this->loadTemplate('cadastro/categoria', $dados);
	}

	public function formularioCategoria($id = ''){
		$dados = array();
		$dados = $this->loadDados('Categoria','cadastros','categoria');
		$dados['permissao'] = $this->permissoes();
		$dados['script'] = 'page/cadastro/categoria.js';
		if(isset($id) && !empty($id) ){
			$dados['categoria'] = $this->categoria->listarPorId($id);
		}
		$this->loadTemplate('cadastro/formCategoria', $dados);
	}

	public function cadastrarCategoria(){
		echo $this->categoria->cadastrar();
	}

	public function editarCategoria($id){
		echo $this->categoria->editar($id);
	}

	public function deletarCategoria($id){
		echo $this->categoria->deletar($id);
	}

	/* ==== Fim Categoria === */


	/* ==== SubCategoria === */

	public function subCategoria(){
		$dados = array();
		$dados = $this->loadDados('','cadastros', 'subcategoria');
		$dados['permissao'] = $this->permissoes();
		$dados['script'] = 'page/cadastro/subcategoria.js';
		$dados['subcategoria'] = $this->subcategoria->listar();
		$this->loadTemplate('cadastro/subcategoria', $dados);
	}

	public function formularioSubCategoria($id = ''){
		$dados = array();
		$dados = $this->loadDados('Categoria','cadastros','categoria');
		$dados['categoria'] = $this->categoria->listar();
		$dados['permissao'] = $this->permissoes();
		$dados['script'] = 'page/cadastro/subcategoria.js';
		if(isset($id) && !empty($id) ){
			$dados['subcategoria'] = $this->subcategoria->listarPorId($id);
		}
		$this->loadTemplate('cadastro/formSubCategoria', $dados);
	}

	public function cadastrarSubCategoria(){
		echo $this->subcategoria->cadastrar();
	}

	public function editarSubCategoria($id){
		echo $this->subcategoria->editar($id);
	}

	public function deletarSubCategoria($id){
		echo $this->subcategoria->deletar($id);
	}

	/* ==== Fim SubCategoria === */


	/* ==== Forma de pagamento === */

	public function formaPagamento(){
		$dados = array();
		$dados = $this->loadDados('','cadastros', 'forma_pagamento');
		$dados['permissao'] = $this->permissoes();
		$dados['formas'] = $this->forma_pagamento->listar();
		$dados['script'] = 'page/cadastro/forma_pagamento.js';
		$this->loadTemplate('cadastro/formaPagamento', $dados);
	}

	public function cadastrarFormaPagamento(){
		echo $this->forma_pagamento->cadastrar();
	}

	public function editarFormaPagamento($id){
		echo $this->forma_pagamento->editar($id);
	}

	public function deletarFormaPagamento($id){
		echo $this->forma_pagamento->deletar($id);
	}

	/* ==== Fim Forma de pagamento === */

}