<?php

class Configuracao extends Controller{
	
	function __construct(){
		parent::__construct();
		if(!$this->estaLogado()){
			header("Location: ".URL."login");
		}
	}

	public function index(){
		$dados = array();
		$dados = $this->loadDados('','configuracao', 'empresa');
		//$dados['permissao'] = $this->permissoes();
		$dados['script'] = 'page/empresa.js';
		$this->loadTemplate('configuracao/empresa', $dados);
	}

	public function uploadLogo(){
		print_r($_FILES);
		$valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp'); // valid extensions
		$path = IMG.'logo/'; // upload directory

		if(isset($_FILES['image'])){
 			$img = $_FILES['image']['name'];
 			$tmp = $_FILES['image']['tmp_name'];
  
 			// get uploaded file's extension
 			$ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
 
 			// can upload same image using rand function
			$final_image = rand(1000,1000000).$img;

			// check's valid format
			if(in_array($ext, $valid_extensions)) {     
				$path = $path.strtolower($final_image); 

				if(move_uploaded_file($tmp,$path)) {
					echo "<img src='$path' />";
				}
			}else{
					echo 'invalid file';
			}
		}
	}
}