<?php

class Admin extends Controller{

	private $financeiro = null;
	private $caixa = null;
	function __construct(){
		parent::__construct();
		if(!$this->estaLogado()){
			header("Location: ".URL."login");
		}
		$this->financeiro = $this->loadModel("FinanceiroModel");
		$this->caixa = $this->loadModel('CaixaModel');
	}

	public function index(){
		$dados = array();
		$dados = $this->loadDados('','admin');
		$dados['permissao'] = $this->permissoes();
		$dados['script'] = 'page/home.js';
		$dados['recebimentos_hoje'] = json_decode($this->financeiro->getContasReceberHoje(), true);
		$dados['recebimentos_atrasados'] = json_decode($this->financeiro->getContasReceberAtrasadas(), true);
		$dados['contas_pagar'] = json_decode($this->financeiro->getContasPagarHoje(), true);
		$dados['contas_atrasadas'] = json_decode($this->financeiro->getContasPagarAtrasadas(), true);
		$dados['caixas'] = $this->caixa->getCaixas();
		$this->loadTemplate('admin/home', $dados);
	}
}