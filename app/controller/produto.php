<?php

class Produto extends Controller{

	private $produto = null;
	private $grupo = null;
	private $unidade_medida = null;
	
	function __construct(){
		parent::__construct();
		if(!$this->estaLogado()){
			header("Location: ".URL."login");
		}
		$this->produto = $this->loadModel("ProdutoModel");
		$this->grupo = $this->loadModel("ProdutoGrupoModel");
		$this->unidade_medida = $this->loadModel("ProdutoUnidadeMedidaModel");
	}

	public function index(){
		$dados = array();
		$dados = $this->loadDados('','produto', 'produto');
		$dados['permissao'] = $this->permissoes();
		$dados['script'] = 'page/produto.js';
		$dados['produto'] = json_decode($this->produto->listar(), true);
		$this->loadTemplate('produto/produto', $dados);
	}

	public function formulario($id = ''){
		$dados = array();
		$dados = $this->loadDados('','produto');
		$dados['permissao'] = $this->permissoes();
		$dados['script'] = 'page/produto.js';
		$dados['grupo'] = json_decode($this->grupo->listar(), true);
		$dados['unidade_medida'] = json_decode($this->unidade_medida->listar(), true);
		if(isset($id) && $id != ''){
  			$dados['produto'] = json_decode($this->produto->listarPorId($id), true);
 			if($dados['produto']['code'] != '200'){
 				header("Location: ".URL."produto/");
 			}
 		}
		$this->loadTemplate('produto/produtoForm', $dados);
	}

	public function verificaGtin($gtin){
		
	}

	public function cadastrarProduto(){
		echo $this->produto->cadastrar();
	}

	public function editarProduto($id){
		if(isset($id) && $id != ''){
 			echo $this->produto->editar($id);
 		}else{
 			echo json_encode(
                array(
                    "code" => '300',
                    "mensagem" => 'Erro'
                )
            );
 		}
	}

	public function formularioGrupo($id = ''){
		$dados = array();
		$dados = $this->loadDados('','produto', 'grupo');
		$dados['permissao'] = $this->permissoes();
		$dados['script'] = 'page/produto.js';
		if(isset($id) && $id != ''){
 			$dados['grupo'] = json_decode($this->grupo->listarPorId($id), true);
 			if($dados['grupo']['code'] != '200'){
 				header("Location: ".URL."produto/grupo");
 			}
 		}
		$this->loadTemplate('produto/grupoForm', $dados);
	}

	public function grupo(){
		$dados = array();
		$dados = $this->loadDados('','produto', 'grupo');
		$dados['permissao'] = $this->permissoes();
		$dados['script'] = 'page/produto.js';
		$dados['grupo'] = json_decode($this->grupo->listar(), true);
		$this->loadTemplate('produto/grupo', $dados);
	}

	public function cadastrarGrupo(){
		echo $this->grupo->cadastrar();
	}

	public function editarGrupo($id){
		echo $this->grupo->editar($id);
	}

	public function excluirGrupo($id){
		echo $this->grupo->excluir($id);
	}

	public function unidadeMedida(){
		$dados = array();
		$dados = $this->loadDados('','produto', 'unidade_medida');
		$dados['permissao'] = $this->permissoes();
		$dados['script'] = 'page/produto.js';
		$dados['unidade_medida'] = json_decode($this->unidade_medida->listar(), true);
		$this->loadTemplate('produto/unidadeMedida', $dados);
	}

	public function formularioUnidadeMedida($id = ''){
		$dados = array();
		$dados = $this->loadDados('','produto', 'unidade_medida');
		$dados['permissao'] = $this->permissoes();
		$dados['script'] = 'page/produto.js';
		if(isset($id) && $id != ''){
 			$dados['unidade_medida'] = json_decode($this->unidade_medida->listarPorId($id), true);
 			if($dados['unidade_medida']['code'] != '200'){
 				header("Location: ".URL."produto/unidadeMedida");
 			}
 		}
		$this->loadTemplate('produto/unidadeMedidaForm', $dados);
	}

	public function cadastrarUnidadeMedida(){
		echo $this->unidade_medida->cadastrar();
	}

	public function editarUnidadeMedida($id){
		echo $this->unidade_medida->editar($id);
	}

	public function excluirUnidadeMedida($id){
		echo $this->unidade_medida->excluir($id);
	}

	public function pesquisarProduto($pesquisa){
		echo $this->produto->pesquisarProduto($pesquisa);
	}

}