<?php

class Empresa extends Controller{
	
	protected $empresa = null;

	function __construct(){
		parent::__construct();
		if(!$this->estaLogado()){
			header("Location: ".URL."login");
		}
		$this->empresa = $this->loadModel('AtorModel');
	}

	public function index(){
		$dados = array();
		$dados = $this->loadDados('Empresa','pessoa', 'empresa');
		//$dados['permissao'] = $this->permissoes();
		$this->loadTemplate('pessoas/empresa/home', $dados);
	}

	public function formularioEmpresa($id = ''){
		$dados = array();
		$dados = $this->loadDados('Empresa','pessoa', 'empresa');
		//$dados['permissao'] = $this->permissoes();
		$dados['script'] = 'page/empresa.js';
		if(isset($id) && !empty($id) ){
			$dados['empresa'] = null;
		}
		$this->loadTemplate('_utils/forms/empresa/form-empresa', $dados);
	}

	public function cadastrarEmpresa(){
		echo $this->empresa->cadastrarEmpresa();
	}
}