<?php

class Cliente extends Controller{

    function __construct(){
		parent::__construct();
		if(!$this->estaLogado()){
			header("Location: ".URL."login");
		}
	}

	public function index(){
		$dados = array();
		$dados['permissao'] = $_SESSION['permissoes'];
		$dados['script'] = 'page/pessoa/pessoa.js';
		$this->loadTemplate('pessoas/cliente/home', $dados);
	}

	public function info($id){
		$dados = array();
		$dados['permissao'] = $this->permissoes();
		$this->loadTemplate('pessoas/cliente/info', $dados);
	}

}