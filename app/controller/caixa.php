<?php

class Caixa extends Controller{

	private $caixa = null;
	function __construct(){
		parent::__construct();
		if(!$this->estaLogado()){
			header("Location: ".URL."login");
        }
        
		$this->caixa = $this->loadModel('CaixaModel');
	}

	public function index(){
		$dados = array();
    }
    
    public function getInfoCaixa($id){
        echo $this->caixa->getInfoCaixa($id);
	}
	
	public function encerrarCaixa(){
		echo $this->caixa->encerrarCaixa();
	}

	public function infoFechamentoCaixa($id){
		$dados = array();
		$dados['permissao'] = $this->permissoes();
		$dados['script'] = 'page/pdv.js';
		$dados['caixa'] = json_decode($this->caixa->getInfoCaixa($id), true);
		$this->loadTemplatePDV('caixa/info', $dados);
	}
}