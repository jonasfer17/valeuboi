<?php

class Financeiro extends Controller{
	
	private $forma_pagamento = null;
	private $caixa = null;
	private $cliente = null;
	private $fiado = null;
	private $categoria = null;
	private $lancamento = null;

	function __construct(){
		parent::__construct();
		if(!$this->estaLogado()){
			header("Location: ".URL."login");
		}
		$this->forma_pagamento = $this->loadModel("FormaPagamentoModel");
		$this->caixa = $this->loadModel('CaixaModel');
		$this->cliente = $this->loadModel('AtorModel');
		$this->fiado = $this->loadModel('FiadoModel');
		$this->categoria = $this->loadModel('CategoriaModel');
		$this->lancamento = $this->loadModel('LancamentoModel');
	}

	public function index(){
		$dados = array();
		$dados = $this->loadDados('Financeiro','financeiro', 'financeiro');
		$dados['permissao'] = $this->permissoes();
		$this->loadTemplate('financeiro/home', $dados);
	}

	public function listarCaixa(){
		echo $this->caixa->listarCaixaPorData();
	}

	public function InfoCaixa($id){
		$cx = $this->caixa->infoCaixa($id);
		$data = $this->caixa->getDataCaixa($id);
		$fd = $this->fiado->listarPorDataCaixa($data['data']);
		$lc = $this->lancamento->listarPorDataCaixa($data['data']);
		echo json_encode(
			array(
				'caixa' => $cx,
				'fiado' => $fd,
				'lancamento' => $lc
			)
		);
	}

	public function filtrarCaixa(){
		echo $this->caixa->listar();
	}

	public function lancamento(){
		$dados = array();
		$dados = $this->loadDados('','financeiro', 'lancamento');
		$dados['permissao'] = $this->permissoes();
		$dados['script'] = 'page/financeiro/lancamento.js';
		$dados['categoria'] = $this->categoria->listar();
		$dados['lancamento'] = json_decode($this->lancamento->listar(), true);
		$this->loadTemplate('financeiro/lancamento', $dados);
	}

	public function formularioLancamento($id = ''){
		$dados = array();
		$dados = $this->loadDados('','financeiro', 'lancamento');
		$dados['permissao'] = $this->permissoes();
		$dados['categoria'] = $this->categoria->listar();
		$dados['pessoa'] = $this->cliente->listar();
		$dados['forma_pagamento'] = $this->forma_pagamento->listar();
 		$dados['script'] = 'page/financeiro/lancamento.js';
 		if(isset($id) && $id != ''){
 			$dados['lancamento'] = json_decode($this->lancamento->listarPorId($id), true);
 			if($dados['lancamento']['code'] != '200'){
 				header("Location: ".URL."financeiro/lancamento");
 			}
 		}
		$this->loadTemplate('financeiro/formLancamento', $dados);
	}

	public function editarLancamento($id){
		echo $this->lancamento->editar($id);
		
	}

	public function filtrarLancamentos(){
		echo $this->lancamento->listar();
	}


	public function caixa(){
		$dados = array();
		$dados = $this->loadDados('','financeiro', 'caixa');
		$dados['permissao'] = $this->permissoes();
		$dados['script'] = 'page/financeiro/caixa.js';
		$dados['caixa'] = json_decode($this->caixa->listar(), true);
		$dados['forma_pagamento'] = $this->forma_pagamento->listar();
		$dados['total'] = $this->caixa->detalhesFiltroCaixa();
		$this->loadTemplate('financeiro/caixa', $dados);
	}

	public function novoCaixa(){
		if($_POST['total_caixa'] > 0){
			echo $this->caixa->novoCaixa();
		}else{
			echo json_encode( 
				array(
					'code' => '300',
					'mensagem' => 'Valor do caixa vazio'
				) 
			);
		}
	}

	public function editarCaixa($id){
		if($_POST['total_caixa'] > 0){
			echo $this->caixa->editarCaixa($id);
		}else{
			echo json_encode( 
				array(
					'code' => '300',
					'mensagem' => 'Valor do caixa vazio'
				) 
			);
		}
	}

	public function listarDetalheCaixaPorId($id){
		echo json_encode($this->caixa->listarDetalheCaixaPorId($id));
	}

	public function listarDetalheFiadoPorId($id){
		echo json_encode($this->fiado->listarDetalheFiadoPorId($id));
	}

	public function fiado(){
		$dados = array();
		$dados = $this->loadDados('','financeiro', 'fiado');
		$dados['permissao'] = $this->permissoes();
		$dados['script'] = 'page/financeiro/fiado.js';
		$dados['clientes'] = $this->cliente->listar();
		$dados['fiado'] = $this->fiado->listar();
		$dados['forma_pagamento'] = $this->forma_pagamento->listar();
		$dados['total_aberto'] = $this->fiado->totalAberto();
		$dados['total_vencido'] = $this->fiado->totalVencido();
		$dados['total_pago'] = $this->fiado->totalPago();
		$dados['total_fiado'] = $this->fiado->totalFiado();
		$this->loadTemplate('financeiro/fiado', $dados);
	}

	public function filtrarFiado(){
		echo $this->fiado->filtrarFiado();
	}

	public function novoFiado(){
		if(!isset($_POST['cliente'])){
			echo json_encode(
				array(
					'code' => '300',
					'mensagem' => 'Informe o cliente'
				)
			);
		}else{
			echo $this->fiado->novo();
		}
	}

	public function editarFiado($id){
		if(!isset($_POST['cliente'])){
			echo json_encode(
				array(
					'code' => '300',
					'mensagem' => 'Informe o cliente'
				)
			);
		}else{
			echo $this->fiado->editar($id);
		}
	}

	public function deletarfiado($id){
		echo $this->fiado->excluir($id);
	}

	public function novoLancamento(){
		echo $this->lancamento->novo();
	}

}