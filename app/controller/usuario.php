<?php

class Usuario extends Controller{
	
	protected $usuario = null;
	protected $ator = null;

	function __construct(){
		parent::__construct();
		if(!$this->estaLogado()){
			header("Location: ".URL."login");
		}
		$this->usuario = $this->loadModel('UsuarioModel');
		$this->ator = $this->loadModel('AtorModel');
	}

	public function index(){
		$dados = array();
		$dados = $this->loadDados('Usuários','pessoa','usuario', 'gerenciar');
		$dados['permissao'] = $this->permissoes();
		$this->loadTemplate('pessoas/usuario/home', $dados);
	}

	public function formularioUsuario($id = ''){
		$dados = array();
		$dados = $this->loadDados('Usuários','pessoa','usuario', 'gerenciar');
		$dados['permissao'] = $this->permissoes();
		$dados['script'] = 'page/usuario/usuario.js';
		$dados['empresa'] = $this->ator->listarEmpresas();
		$dados['grupo'] = $this->usuario->listarGrupos();
		if(isset($id) && !empty($id) ){
			$dados['usuario'] = $this->usuario->listarPorId($id);
		}
		$this->loadTemplate('_utils/forms/usuario/form-usuario', $dados);
	}

	public function grupo(){
		$dados = array();
		$dados = $this->loadDados('Grupos de Permissões','pessoa','usuario', 'grupo-permissao');
		$dados['permissao'] = $this->permissoes();
		$dados['grupo'] = $this->usuario->listarGrupos();
		$dados['script'] = 'page/usuario/grupo.js';
		$this->loadTemplate('pessoas/usuario/grupo', $dados);
	}

	public function formularioGrupo($id = ''){
		$dados = array();
		$dados = $this->loadDados('Grupos de Permissões','pessoa','usuario', 'grupo-permissao');
		$dados['permissao'] = $this->permissoes();
		$dados['script'] = 'page/usuario/grupo.js';
		$dados['permissoes'] = $this->usuario->listarPermissoes();
		if(isset($id) && !empty($id) ){
			$dados['grupo'] = $this->usuario->listarGrupoPorId($id);
			$dados['parametros'] = explode(',', $dados['grupo']['parametros']);
		}
		$this->loadTemplate('_utils/forms/usuario/form-grupo', $dados);
	}

	public function permissao(){
		$dados = array();
		$dados = $this->loadDados('Permissões','pessoa','usuario', 'parametros');
		$dados['permissao'] = $this->permissoes();
		$dados['script'] = 'page/usuario/permissao.js';
		$dados['permissoes'] = $this->usuario->listarPermissoes();
		$this->loadTemplate('pessoas/usuario/permissao', $dados);
	}

	public function formularioPermissao($id = ''){
		$dados = array();
		$dados = $this->loadDados('Permissões','pessoa','usuario', 'parametros');
		$dados['permissao'] = $this->permissoes();
		$dados['script'] = 'page/usuario/permissao.js';
		$dados['categoria'] = $this->usuario->listarCategorias();
		if(isset($id) && !empty($id) ){
			$dados['permissoes'] = $this->usuario->listarPermissaoPorId($id);
		}
		$this->loadTemplate('_utils/forms/usuario/form-permissao', $dados);
	}

	public function cadastrarGrupo(){
		echo $this->usuario->cadastrarGrupo();
	}

	public function editarGrupo($id){
		echo $this->usuario->editarGrupo($id);
	}

	public function deletarGrupo($id){
		echo $this->usuario->deletarGrupo($id);
	}

	public function cadastrarPermissao(){
		echo $this->usuario->cadastrarPermissao();
	}

	public function editarPermissao($id){
		echo $this->usuario->editarPermissao($id);
	}

	public function deletarPermissao($id){
		echo $this->usuario->deletarPermissao($id);
	}
}