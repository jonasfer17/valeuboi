<?php

class Funcionario extends Controller{

    function __construct(){
		parent::__construct();
		if(!$this->estaLogado()){
			header("Location: ".URL."login");
        }
        $this->ator = $this->loadModel('AtorModel');
	}

	public function index(){
		$dados = array();
		$dados['permissao'] = $_SESSION['permissoes'];
		$this->loadTemplate('pessoas/cliente/home', $dados);
	}

	public function listarFuncionariosPorEmpresa($id){
        echo json_encode($this->ator->listarFuncionariosPorEmpresa($id));
    }

}