<?php

class Home extends Controller{

    public function __construct(){
        parent::__construct();
        if(!$this->estaLogado()){
			header("Location: ".URL."admin");
		}
    }
    
    public function index(){
		$dados = array();
		header("Location: ".URL."admin/");
	}
}