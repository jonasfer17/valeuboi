<?php

class Login extends Controller{

	protected $usuario = null;

	public function __construct(){
		parent::__construct();
		if($this->estaLogado()){
			header("Location: ".URL."admin");
		}
		$this->usuario = $this->loadModel("UsuarioModel");
	}

	public function index(){
		$dados = array();
		$this->loadView('home/login', $dados);
	}

	public function login(){
		echo $this->usuario->login();
	}

	public function logout(){
		echo $this->usuario->logout();
	}

}