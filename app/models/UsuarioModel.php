<?php 

class UsuarioModel{
    
	function __construct($db){
		try{
			$this->db = $db;
		}catch(PDOException $e){
			exit("Não foi possivel conectar ao Banco de Dados");
		}
	}

	 // Método do login
	 public function login(){
        $sql = $this->db->prepare("SELECT 
                                   usuario.id as id_usuario,
                                   usuario.login as login,
                                   funcionario.nome as nome,
                                   funcionario.id_empresa as id_empresa,
                                   usuario.ator_id_grupo as id_grupo
                                   FROM 
                                   ator_login usuario,
                                   ator funcionario
                                   WHERE 
                                   usuario.login = :login 
                                   AND 
                                   usuario.senha = :senha
                                   AND
                                   usuario.id_ator = funcionario.id");

        $sql->bindValue(':login', addslashes($_POST['login']));
        $sql->bindValue(':senha', md5(addslashes($_POST['senha'])));
        

        try{
            $sql->execute();
            
            if($sql->rowCount() > 0){
                    $row = $sql->fetch();
                    $_SESSION['idUsuario'] = $row['id_usuario'];
                    $_SESSION['idEmpresa'] = $row['id_empresa'];
                    $_SESSION['nome'] = $row['nome'];
                    $_SESSION['login'] = $row['login'];
                    $_SESSION['idGrupo'] = $row['id_grupo'];
                    $this->getGrupoPermissao($row['id_grupo']); 
                return json_encode(
                    array(
                        "code" => '200'
                    )
                );

            }else{
                return json_encode(
                    array(
                        "code" => "300",
                        "mensagem" => "Login e/ou senha incorretos!"
                    )
                );
            }
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => "300",
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    public function getGrupoPermissao($id){
        $sql = $this->db->prepare("SELECT
                                   parametros
                                   FROM
                                   grupo_permissao
                                   WHERE
                                   id = :id");
        $sql->bindValue(':id', $id);
        try {
            $sql->execute();
            $row = $sql->fetch();
            $_SESSION['permissoes'] =  $row['parametros'];
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function logout(){
        unset($_SESSION['idUsuario']);
        unset($_SESSION['idEmpresa']);
        unset($_SESSION['nome']);
        unset($_SESSION['login']); 
    }

    public function cadastrarGrupo(){
        $sql = $this->db->prepare("INSERT INTO
                                   grupo_permissao
                                   SET
                                   id_empresa = :id_empresa,
                                   nome = :nome,
                                   parametros = :parametros");
        $parametros = implode(',', $_POST['permissao']);
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        $sql->bindValue(':nome', trim(addslashes(ucfirst($_POST['nome']))));
        $sql->bindValue(':parametros', $parametros);
        try {
            $sql->execute();
            if($this->db->lastInsertId() > 0){
                return json_encode(
                    array(
                            "code" => '200',
                            "mensagem" => 'Cadastrado com sucesso!'
                        )
                );
            }
        } catch (PDOException $e) {
            return json_encode(
                array(
                    "code" => "500",
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    public function listarGrupos(){
        $sql = $this->db->prepare("SELECT
                                   id,
                                   nome
                                   FROM
                                   grupo_permissao
                                   WHERE
                                   id_empresa = :id_empresa");
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        try {
            $sql->execute();
            return $sql->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function listarGrupoPorId($id){
        $sql = $this->db->prepare("SELECT
                                   *
                                   FROM
                                   grupo_permissao
                                   WHERE
                                   id_empresa = :id_empresa
                                   AND
                                   id = :id");
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        $sql->bindValue(':id', $id);
        try {
            $sql->execute();
            return $sql->fetch();
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function editarGrupo($id){
        $sql = $this->db->prepare("UPDATE
                                   grupo_permissao
                                   SET
                                   nome = :nome,
                                   parametros = :parametros
                                   WHERE
                                   id_empresa = :id_empresa
                                   AND
                                   id = :id");
        $parametros = implode(',', $_POST['permissao']);
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        $sql->bindValue(':nome', trim(addslashes(ucfirst($_POST['nome']))));
        $sql->bindValue(':parametros', $parametros);
        $sql->bindValue(':id', $id);
        try {
            $sql->execute();
            if($sql->rowCount() > 0){
                $this->getGrupoPermissao($_SESSION['idGrupo']);
                return json_encode(
                    array(
                            "code" => '202',
                            "mensagem" => 'Editado com sucesso!'
                        )
                );
            }else{
                return json_encode(
                    array(
                            "code" => '300',
                            "mensagem" => 'Erro!'
                        )
                );
            }
        } catch (PDOException $e) {
            return json_encode(
                array(
                    "code" => "500",
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    public function deletarGrupo($id){
        $sql = $this->db->prepare("DELETE FROM
                                   grupo_permissao
                                   WHERE
                                   id_empresa = :id_empresa
                                   AND
                                   id = :id");
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        $sql->bindValue(':id', $id);
        try {
            $sql->execute();
            if($sql->rowCount() > 0){
                return json_encode(
                    array(
                            "code" => '204',
                            "mensagem" => 'Excluido com sucesso!'
                        )
                );
            }else{
                return json_encode(
                    array(
                            "code" => '300',
                            "mensagem" => 'Erro!'
                        )
                );
            }
        } catch (PDOException $e) {
            return json_encode(
                array(
                    "code" => "500",
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    
    public function cadastrarPermissao(){
        $sql = $this->db->prepare("INSERT INTO
                                   grupo_permissao_parametro
                                   SET
                                   nome = :nome,
                                   id_categoria = :categoria");
        $sql->bindValue(':nome', trim(addslashes(ucfirst($_POST['nome']))));
        $sql->bindValue(':categoria', trim($_POST['categoria']));
        try {
            $sql->execute();
            if($this->db->lastInsertId() > 0){
                return json_encode(
                    array(
                            "code" => '200',
                            "mensagem" => 'Cadastrado com sucesso!'
                        )
                );
            }
        } catch (PDOException $e) {
            return json_encode(
                array(
                    "code" => "500",
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    
    public function listarCategorias(){
        $sql = $this->db->prepare("SELECT
                                   *
                                   FROM
                                   grupo_permissao_categoria
                                   ORDER BY
                                   nome
                                   ASC");
        try {
            $sql->execute();
            return $sql->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function listarPermissoes(){
        $sql = $this->db->prepare("SELECT
                                   gp.*,
                                   gc.nome as grupo
                                   FROM
                                   grupo_permissao_parametro gp,
                                   grupo_permissao_categoria gc
                                   WHERE
                                   gp.id_categoria = gc.id");
        try {
            $sql->execute();
            return $sql->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function listarPermissaoPorId($id){
        $sql = $this->db->prepare("SELECT
                                   *
                                   FROM
                                   grupo_permissao_parametro
                                   WHERE
                                   id = :id");
        $sql->bindValue(':id', $id);
        try {
            $sql->execute();
            return $sql->fetch();
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function editarPermissao($id){
        $sql = $this->db->prepare("UPDATE
                                   grupo_permissao_parametro
                                   SET
                                   nome = :nome,
                                   categoria = :categoria
                                   WHERE
                                   id = :id");
        $sql->bindValue(':nome', trim(addslashes(ucfirst($_POST['nome']))));
        $sql->bindValue(':categoria', trim($_POST['categoria']));
        $sql->bindValue(':id', $id);
        try {
            $sql->execute();
            if($sql->rowCount() > 0){
                return json_encode(
                    array(
                            "code" => '202',
                            "mensagem" => 'Editado com sucesso!'
                        )
                );
            }else{
                return json_encode(
                    array(
                            "code" => '300',
                            "mensagem" => 'Erro!'
                        )
                );
            }
        } catch (PDOException $e) {
            return json_encode(
                array(
                    "code" => "500",
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    public function deletarPermissao($id){
        $sql = $this->db->prepare("DELETE FROM
                                   grupo_permissao_parametro
                                   WHERE
                                   id = :id");
        $sql->bindValue(':id', $id);
        try {
            $sql->execute();
            if($sql->rowCount() > 0){
                return json_encode(
                    array(
                            "code" => '204',
                            "mensagem" => 'Excluido com sucesso!'
                        )
                );
            }else{
                return json_encode(
                    array(
                            "code" => '300',
                            "mensagem" => 'Erro!'
                        )
                );
            }
        } catch (PDOException $e) {
            return json_encode(
                array(
                    "code" => "500",
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }


}