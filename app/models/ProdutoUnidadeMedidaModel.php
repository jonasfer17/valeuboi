<?php 

class ProdutoUnidadeMedidaModel{

	function __construct($db){
		try{
			$this->db = $db;
		}catch(PDOException $e){
			exit("Não foi possivel conectar ao Banco de Dados");
		}
    }

    public function listar(){
    	$sql = $this->db->prepare("SELECT
    							   *
    							   FROM
    							   produto_unidade_medida
    							   WHERE
    							   id_empresa = :id_empresa");

    	$sql->bindValue(":id_empresa", $_SESSION['idEmpresa']);
    	try{
            $sql->execute();
                return json_encode(
                    array(
                        "code" => '200',
                        "lista" => $sql->fetchAll()
                    )
                );
            
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    public function listarPorId($id){
    	$sql = $this->db->prepare("SELECT
    							   *
    							   FROM
    							   produto_unidade_medida
    							   WHERE
    							   id_empresa = :id_empresa
    							   AND
    							   id = :id");

    	$sql->bindValue(":id_empresa", $_SESSION['idEmpresa']);
    	$sql->bindValue(':id', addslashes(intval($id)));
    	try{
            $sql->execute();
                return json_encode(
                    array(
                        "code" => '200',
                        "unidade_medida" => $sql->fetch()
                    )
                );
            
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    public function cadastrar(){
    	$sql = $this->db->prepare("INSERT INTO
    							   produto_unidade_medida
    							   SET
    							   id_empresa = :id_empresa,
    							   nome = :nome,
                                   fator = :fator");

    	$sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
    	$sql->bindValue(':nome', addslashes($_POST['nome']));
        $sql->bindValue(':fator', addslashes($_POST['fator']));
    	try{
    		$sql->execute();
    		if($this->db->lastInsertId() > 0){
    			return json_encode(
    				array(
    					'code' => '200',
    					'mensagem' => 'Cadastrado com sucesso',
    					'pagina' => 'unidademedida'
    				)
    			);
    		}else{
    			return json_encode(
    				array(
    					'code' => '300',
    					'mensagem' => 'Erro ao cadastrar',
    					'pagina' => 'unidademedida'
    				)
    			);
    		}
    	}catch(PDOException $e){
    		return json_encode(
				array(
					'code' => '300',
					'mensagem' => $e->getMessage(),
					'pagina' => 'unidademedida'
				)
			);
    	}
    }

    public function editar($id){
    	$sql = $this->db->prepare("UPDATE
    							   produto_unidade_medida
    							   SET
    							   nome = :nome,
                                   fator = :fator
    							   WHERE
    							   id_empresa = :id_empresa
    							   AND
    							   id = :id");

    	$sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
    	$sql->bindValue(':nome', addslashes($_POST['nome']));
        $sql->bindValue(':fator', addslashes($_POST['fator']));
    	$sql->bindValue(':id', addslashes(intval($id)));
    	try{
    		$sql->execute();
    		if($sql->rowCount() > 0){
    			return json_encode(
    				array(
    					'code' => '202',
    					'mensagem' => 'Editado com sucesso',
    					'pagina' => 'unidademedida'
    				)
    			);
    		}else{
    			return json_encode(
    				array(
    					'code' => '300',
    					'mensagem' => 'Nada foi alterado',
    					'pagina' => 'unidademedida'
    				)
    			);
    		}
    	}catch(PDOException $e){
    		return json_encode(
				array(
					'code' => '300',
					'mensagem' => $e->getMessage(),
					'pagina' => 'grupo'
				)
			);
    	}
    }

    public function excluir($id){
    	$sql = $this->db->prepare("DELETE FROM
    							   produto_unidade_medida
    							   WHERE
    							   id_empresa = :id_empresa
    							   AND
    							   id = :id");

    	$sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
    	$sql->bindValue(':id', addslashes(intval($id)));
    	try{
    		$sql->execute();
    		if($sql->rowCount() > 0){
    			return json_encode(
    				array(
    					'code' => '200',
    					'mensagem' => 'Excluido com sucesso',
    					'pagina' => 'unidademedida'
    				)
    			);
    		}else{
    			return json_encode(
    				array(
    					'code' => '300',
    					'mensagem' => 'Nada foi alterado',
    					'pagina' => 'unidademedida'
    				)
    			);
    		}
    	}catch(PDOException $e){
    		return json_encode(
				array(
					'code' => '300',
					'mensagem' => $e->getMessage(),
					'pagina' => 'unidademedida'
				)
			);
    	}
    }

}