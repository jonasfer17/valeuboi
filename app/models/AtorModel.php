<?php 

class AtorModel{

	function __construct($db){
		try{
			$this->db = $db;
		}catch(PDOException $e){
			exit("Não foi possivel conectar ao Banco de Dados");
		}
    }


    public function listar(){
        $sql = $this->db->prepare("SELECT
                                   *
                                   FROM
                                   ator
                                   WHERE
                                   id_empresa = :id_empresa
                                   AND
                                   id_tipo = '6'");
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        try {
            $sql->execute();
            return $sql->fetchAll();
        } catch (PDOException $e) {
            return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => $e->getMessage()
                    )
                );
        }
    }
    
    public function cadastrar(){
        $sql = $this->db->prepare("INSERT INTO
                                   ator
                                   SET
                                   id_empresa = :id_empresa,
                                   id_tipo = :id_tipo,
                                   nome = :nome");
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        $sql->bindValue(':id_tipo', '6');
        $sql->bindValue(':nome', addslashes(ucfirst($_POST['nome'])));
        try {
            $sql->execute();
            if($this->db->lastInsertId() > 0){
                return json_encode(
                    array(
                        "code" => '200'
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Problema ao gravar'
                    )
                );
            }   
        } catch (PDOException $e) {
            return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => $e->getMessage()
                    )
                );
        }
    }

    public function editar($id){
        $sql = $this->db->prepare("UPDATE
                                   ator
                                   SET
                                   nome = :nome
                                   WHERE
                                   id = :id");
        $sql->bindValue(':id', addslashes($id));
        $sql->bindValue(':nome', addslashes(ucfirst($_POST['nome'])));
        try {
            $sql->execute();
            if($sql->rowCount() > 0){
                return json_encode(
                    array(
                        "code" => '202',
                        "mensagem" => 'Editado com sucesso'
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Problema ao gravar'
                    )
                );
            }   
        } catch (PDOException $e) {
            return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => $e->getMessage()
                    )
                );
        }
    }

    public function deletar($id){
        $sql = $this->db->prepare("DELETE FROM
                                   ator
                                   WHERE
                                   id = :id");
        $sql->bindValue(':id', addslashes($id));
        try {
            $sql->execute();
            if($sql->rowCount() > 0){
                return json_encode(
                    array(
                        "code" => '200',
                        "mensagem" => 'Deletado com sucesso',
                        "icon" => 'success'
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Problema ao excluir',
                        "icon" => 'error'
                    )
                );
            }   
        } catch (PDOException $e) {
            return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => $e->getMessage(),
                        "icon" => 'error'
                    )
                );
        }
    }


    public function getDadosEmpresa(){
        $sql = $this->db->prepare("SELECT 
                                   * 
                                   FROM 
                                   ator 
                                   WHERE id = :id");

        $sql->bindValue(":id", $_SESSION['idEmpresa']);
         try {
            $sql->execute();
            if($sql->rowCount() > 0){
                return json_encode(
                    array(
                        "code" => '200',
                        "empresa" => $sql->fetch()
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Problema'
                    )
                );
            }   
        } catch (PDOException $e) {
            return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => $e->getMessage()
                    )
                );
        }
    }

    public function getDadosUsuario($id_usuario){
        $sql = $this->db->prepare("SELECT 
                                   * 
                                   FROM 
                                   ator 
                                   WHERE id = :id");

        $sql->bindValue(":id", $id_usuario);
         try {
            $sql->execute();
            if($sql->rowCount() > 0){
                return json_encode(
                    array(
                        "code" => '200',
                        "empresa" => $sql->fetch()
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Problema'
                    )
                );
            }   
        } catch (PDOException $e) {
            return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => $e->getMessage()
                    )
                );
        }
    }
}