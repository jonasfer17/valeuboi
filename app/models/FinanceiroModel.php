<?php 

class FinanceiroModel{

	function __construct($db){
  		try{
  			$this->db = $db;
  		}catch(PDOException $e){
  			exit("Não foi possivel conectar ao Banco de Dados");
  		}
    }

    public function getContasPagarHoje(){
    	$sql = $this->db->prepare("SELECT
    							   SUM(valor+multa) as total
    							   FROM
    							   financeiro_lancamento
    							   WHERE
    							   vencimento = CURDATE()
    							   AND
    							   tipo = 1
                                   AND
                                   id_empresa = :id_empresa");
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
    	try{
            $sql->execute();
            $valor = $sql->fetch();
            return json_encode(
                array(
                    "code" => '200',
                    "valor" => $valor['total']
                )
            );
            
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    public function getContasPagarAtrasadas(){
    	$sql = $this->db->prepare("SELECT
    							   SUM(valor+multa) as total
    							   FROM
    							   financeiro_lancamento
    							   WHERE
    							   vencimento < CURDATE()
    							   AND
    							   tipo = 1
                                   AND
                                   status = 0
                                   AND
                                   id_empresa = :id_empresa");
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
    	try{
            $sql->execute();
            $valor = $sql->fetch();
            return json_encode(
                array(
                    "code" => '200',
                    "valor" => $valor['total']
                )
            );
            
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    public function getContasReceberHoje(){
    	$sql = $this->db->prepare("SELECT
    							   SUM(valor+multa) as total
    							   FROM
    							   financeiro_lancamento
    							   WHERE
    							   vencimento = CURDATE()
    							   AND
    							   tipo = 0
                                   AND
                                   status = 0
                                   AND
                                   id_empresa = :id_empresa");
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
    	try{
            $sql->execute();
            $valor = $sql->fetch();
            return json_encode(
                array(
                    "code" => '200',
                    "valor" => $valor['total']
                )
            );
            
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    public function getContasReceberAtrasadas(){
    	$sql = $this->db->prepare("SELECT
    							   SUM(valor+multa) as total
    							   FROM
    							   financeiro_lancamento
    							   WHERE
    							   vencimento < CURDATE()
    							   AND
    							   tipo = 0
                                   AND
                                   status = 0
                                   AND
                                   id_empresa = :id_empresa");
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
    	try{
            $sql->execute();
            $valor = $sql->fetch();
            return json_encode(
                array(
                    "code" => '200',
                    "valor" => $valor['total']
                )
            );
            
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }




}