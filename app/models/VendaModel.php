<?php 

require_once "ProdutoModel.php";
require_once "FormaPagamentoModel.php";
require_once "MovimentacaoModel.php";

class VendaModel{

	private $produto = null;
    private $formaPagamento = null;
    private $movimentacao = null;
    
	function __construct($db){
		try{
			$this->db = $db;
			$this->produto = new ProdutoModel($db);
            $this->formaPagamento = new FormaPagamentoModel($db);
            $this->movimentacao = new MovimentacaoModel($db);
		}catch(PDOException $e){
			exit("Não foi possivel conectar ao Banco de Dados");
		}
	}

	public function getCaixaAberto(){
		$sql = $this->db->prepare("SELECT 
								   id
								   FROM
								   venda_caixa
								   WHERE
								   id_empresa = :id_empresa
								   AND
								   id_usuario = :id_usuario
								   AND
								   status = '0'
								   LIMIT 1");

		$sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
		$sql->bindValue(':id_usuario', $_SESSION['idUsuario']);
		try{
            $sql->execute();
            if($sql->rowCount() > 0){
                $venda = $sql->fetch();
                $_SESSION['idCaixa'] = $venda['id'];
                return json_encode(
                    array(
                        "code" => '200',
                        "caixa" => $venda['id']
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Nenhum registro'
                    )
                );
            }
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
	}

	public function novaVenda(){
		$sql = $this->db->prepare("INSERT INTO 
								   venda_cabecalho
								   SET
								   id_empresa = :id_empresa,
								   id_usuario = :id_usuario,
                                   id_caixa = :id_caixa");

		$sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
		$sql->bindValue(':id_usuario', $_SESSION['idUsuario']);
        $sql->bindValue(':id_caixa', $_SESSION['idCaixa']);
		try{
			$sql->execute();
			$id = $this->db->lastInsertId();
            if($id > 0){
            	$_SESSION['idVenda'] = $id;
                return json_encode(
                    array(
                        "code" => '200',
                        "id" => $id
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Problema ao criar venda'
                    )
                );
            }
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
	}

	public function insereProduto($id_item, $produto, $qtd){
		if(!isset($_SESSION['idVenda'])){
			$this->novaVenda();
		}
		$sql = $this->db->prepare("INSERT INTO
								   venda_detalhe
								   SET
								   id_venda = :id_venda,
								   id_item = :id_item,
								   quantidade = :quantidade,
								   valor_unitario = :valor_unitario,
								   valor_total = :valor_total");

		$sql->bindValue(':id_venda', $_SESSION['idVenda']);
		$sql->bindValue(':id_item', addslashes($id_item));
		$sql->bindValue(':quantidade', addslashes($qtd));
		$sql->bindValue(':valor_unitario', addslashes($produto['preco_venda']));
		$sql->bindValue(':valor_total', addslashes($produto['preco_venda'] * $qtd));
		try{
			$sql->execute();
			$id = $this->db->lastInsertId();
            if($id > 0){
            	$this->setTotalProdutoVenda($_SESSION['idVenda']);
            	$this->setValorTotalVenda($_SESSION['idVenda']);
            	$produto = json_decode($this->produto->listarPorId($id_item), true);
                return json_encode(
                    array(
                        "code" => '200',
                        "id" => $id,
                        "produto" => $produto['produto'],
                        "quantidade" => $qtd,
                        "info" => json_decode($this->getInfoVenda($_SESSION['idVenda']), true)
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Problema ao adicionar produto'
                    )
                );
            }
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
	}

	public function getInfoVenda($id_venda){
		$sql = $this->db->prepare("SELECT
								   total_produtos,
								   valor_total,
								   descontos
								   FROM
								   venda_cabecalho
								   WHERE
								   id = :id_venda");

		$sql->bindValue(':id_venda', $id_venda);
		try{
            $sql->execute();
            if($sql->rowCount() > 0){ 
            	return json_encode(
                    array(
                        'code' => '200', 
                        'info' => $sql->fetch()
                    )
                );
            }
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
	}

	public function getVendaCabecalho(){
		$sql = $this->db->prepare("SELECT 
                                   id,
								   total_produtos,
								   valor_total,
								   descontos
								   FROM
								   venda_cabecalho
								   WHERE
								   id_empresa = :id_empresa
								   AND
								   id_usuario = :id_usuario
								   AND
								   status = '1'");
		$sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
		$sql->bindValue(':id_usuario', $_SESSION['idUsuario']);
		try{
            $sql->execute();
            if($sql->rowCount() > 0){
                return $sql->fetch();
            }
        }catch(PDOException $e){
            
        }
	}


	public function getVendaAberta(){
		$sql = $this->db->prepare("SELECT 
								   vd.*,
								   p.nome
								   FROM
								   venda_detalhe vd,
								   venda_cabecalho vc,
								   produto p
								   WHERE
								   vc.id_empresa = :id_empresa
								   AND
								   vc.id_usuario = :id_usuario
								   AND
								   vc.status = '1'
								   AND
								   vd.id_venda = vc.id
								   AND
								   p.id = vd.id_item
                                   AND
                                   vd.status = '0'
                                   ORDER BY
                                   id");

		$sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
		$sql->bindValue(':id_usuario', $_SESSION['idUsuario']);
		try{
            $sql->execute();
            if($sql->rowCount() > 0){
                $venda = $sql->fetchAll();
                $_SESSION['idVenda'] = $venda[0]['id_venda'];
                return json_encode(
                    array(
                        "code" => '200',
                        "venda" => $venda
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Nenhum registro'
                    )
                );
            }
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
	}

	public function setTotalProdutoVenda($id_venda){
		$sql = $this->db->prepare("UPDATE
								   venda_cabecalho
								   SET
								   total_produtos = :total
								   WHERE
								   id = :id
								   AND
								   id_empresa = :id_empresa");

		$sql->bindValue(':id', $id_venda);
        $sql->bindValue(':total', $this->getProdutoTotalVenda($_SESSION['idVenda']));
		$sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
		try{
            $sql->execute();
            if($sql->rowCount() > 0){
                return json_encode(
                    array(
                        "code" => '200'
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Nenhum registro'
                    )
                );
            }
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
	}

    

	public function setValorTotalVenda($id_venda){
		$sql = $this->db->prepare("UPDATE
								   venda_cabecalho
								   SET
								   valor_total = :valor_total
								   WHERE
								   id = :id
								   AND
								   id_empresa = :id_empresa");

		$sql->bindValue(':id', $id_venda);
		$sql->bindValue(':valor_total', $this->getValorTotalVenda($id_venda));
		$sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
		try{
            $sql->execute();
            if($sql->rowCount() > 0){
                return json_encode(
                    array(
                        "code" => '200',
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Nenhum registro'
                    )
                );
            }
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
	}

	public function getValorTotalVenda($id_venda){
		$sql = $this->db->prepare("SELECT
								   SUM(valor_total) as valor_total
								   FROM
								   venda_detalhe
								   WHERE
								   id_venda = :id_venda
                                   AND
                                   status = '0'");

		$sql->bindValue(':id_venda', $id_venda);
		try{
            $sql->execute();
            if($sql->rowCount() > 0){
            	$total = $sql->fetch(); 
            	return $total['valor_total'];  
            }
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
	}

    public function getProdutoTotalVenda($id_venda){
        $sql = $this->db->prepare("SELECT
                                   COUNT(id) as total
                                   FROM
                                   venda_detalhe
                                   WHERE
                                   id_venda = :id_venda
                                   AND
                                   status = '0'");

        $sql->bindValue(':id_venda', $id_venda);
        try{
            $sql->execute();
            if($sql->rowCount() > 0){
                $total = $sql->fetch(); 
                return $total['total'];  
            }
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

	public function excluirProdutoVenda($id_detalhe){
        $sql = $this->db->prepare("UPDATE
                                   venda_detalhe
                                   SET
                                   status = '10'
                                   WHERE
                                   id = :id
                                   AND
                                   id_venda = :id_venda");

        $sql->bindValue(':id', $id_detalhe);
        $sql->bindValue(':id_venda', $_SESSION['idVenda']);
        try{
            $sql->execute();
            if($sql->rowCount() > 0){
                $this->setTotalProdutoVenda($_SESSION['idVenda']);
                $this->setValorTotalVenda($_SESSION['idVenda']);
                return json_encode(
                    array(
                        "code" => '200',
                        "info" => json_decode($this->getInfoVenda($_SESSION['idVenda']), true),
                        "id" => $id_detalhe
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Nenhum registro'
                    )
                );
            }
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    public function getFormaPagamento(){
        $sql = $this->db->prepare("SELECT
                                   v.id,
                                   v.valor,
                                   f.nome
                                   FROM
                                   venda_pagamento v,
                                   financeiro_forma_pagamento f
                                   WHERE
                                   v.id_venda = :id_venda
                                   AND
                                   f.id = v.id_forma_pagamento");

        $sql->bindValue(':id_venda', $_SESSION['idVenda']);
        try{
            $sql->execute();
            if($sql->rowCount() > 0){
                return json_encode(
                    array(
                        "code" => '200',
                        "venda" => $sql->fetchAll()
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Nenhum registro'
                    )
                );
            }
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    public function insereFormaPagamento($id_forma_pagamento){
        $sql = $this->db->prepare("INSERT INTO
                                   venda_pagamento
                                   SET
                                   id_venda = :id_venda,
                                   id_forma_pagamento = :id_forma_pagamento");
        $sql->bindValue(':id_venda', $_SESSION['idVenda']);
        $sql->bindValue(':id_forma_pagamento', addslashes($id_forma_pagamento));
        try{
            $sql->execute();
            $id = $this->db->lastInsertId();
            if($id > 0){
                $valor_total = json_decode($this->getInfoVenda($_SESSION['idVenda']), true);
                $valor_recebido = $this->getValorRecebidoVenda();
                $valor = (($valor_total['info']['valor_total'] - $valor_total['info']['descontos']) - $valor_recebido['valor']);
                if($valor < 0){
                    $valor = 0;
                }
                return json_encode(
                    array(
                        "code" => '200',
                        "id" => $id,
                        "forma" => $this->formaPagamento->listarPorId($id_forma_pagamento),
                        "valor" => $valor
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Erro!'
                    )
                );
            }
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    public function setValorFormaPagamento($id){
        $sql = $this->db->prepare("UPDATE
                                   venda_pagamento
                                   SET
                                   valor = :valor
                                   WHERE
                                   id = :id
                                   AND
                                   id_venda = :id_venda");

        $sql->bindValue(':id', $id);
        $sql->bindValue(':id_venda', $_SESSION['idVenda']);
        if($_POST['valor'] > 0){
             $sql->bindValue(':valor', $_POST['valor']);
        }else{
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => 'Valor incorreto'
                )
            );
        }
        try{
            $sql->execute();
            if($sql->rowCount() > 0){
                $info = json_decode($this->getInfoVenda($_SESSION['idVenda']), true);
                return json_encode(
                    array(
                        "code" => '200',
                        "info" => $info['info']
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Nenhum registro'
                    )
                );
            }
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    public function getValorRecebidoVenda(){
         $sql = $this->db->prepare("SELECT
                                   COALESCE(SUM(valor), 0) as valor
                                   FROM
                                   venda_pagamento
                                   WHERE
                                   id_venda = :id_venda");

        $sql->bindValue(':id_venda', $_SESSION['idVenda']);
        try{
            $sql->execute();
            return $sql->fetch();
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }


    public function encerrarVenda(){
        $sql = $this->db->prepare("UPDATE
                                   venda_cabecalho
                                   SET
                                   status = '2',
                                   data_faturamento = :data,
                                   hora_faturamento = :hora
                                   WHERE
                                   id = :id");

        $sql->bindValue(':data', date('Y-m-d'));
        $sql->bindValue(':hora', date('H:i:s'));
        $sql->bindValue(':id', $_SESSION['idVenda']);

        try{
            $sql->execute();
            if($sql->rowCount() > 0){
                $produtos = json_decode($this->getProdutosVenda($_SESSION['idVenda']), true);
                foreach($produtos['venda'] as $p){
                    $this->movimentacao->inserir($p['id'], $_SESSION['idVenda'], 'Venda de produto', $p['quantidade'], '0', $p['valor_unitario'], $p['preco_custo']);
                }
                $venda = json_decode($this->dadosVenda($_SESSION['idVenda']), true);
                $venda = $venda['venda'];
                $this->atualizarCaixa($venda['valor_total'], $venda['descontos'], '0');
                $this->atualizarCaixaTotalGeral();
                return json_encode(
                    array(
                        "code" => '200',
                        "id" => $_SESSION['idVenda']
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Erro!'
                    )
                );
            }
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }


    public function dadosVenda($id_venda){
        $sql = $this->db->prepare("SELECT
                                   vc.id,
                                   vc.id_usuario,
                                   vc.id_vendedor,
                                   vc.numero,
                                   vc.total_produtos,
                                   vc.valor_total,
                                   vc.descontos,
                                   vc.data_faturamento,
                                   vc.hora_faturamento,
                                   vp.id_forma_pagamento,
                                   vp.valor as valor_pago
                                   FROM
                                   venda_cabecalho vc,
                                   venda_pagamento vp
                                   WHERE
                                   vc.id = vp.id_venda
                                   AND
                                   vc.id = :id
                                   AND
                                   vc.id_empresa = :id_empresa");

        $sql->bindValue(':id', $id_venda);
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
         try{
            $sql->execute();
            if($sql->rowCount() > 0){
                return json_encode(
                    array(
                        "code" => '200',
                        "venda" => $sql->fetch()
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Erro!'
                    )
                );
            }
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    public function getProdutosVenda($id){
        $sql = $this->db->prepare("SELECT 
                                   vd.*,
                                   p.nome,
                                   p.preco_custo
                                   FROM
                                   venda_detalhe vd,
                                   venda_cabecalho vc,
                                   produto p
                                   WHERE
                                   vc.id_empresa = :id_empresa
                                   AND
                                   vc.status = '2'
                                   AND
                                   vd.id_venda = vc.id
                                   AND
                                   p.id = vd.id_item
                                   AND
                                   vd.status = '0'
                                   AND
                                   vc.id = :id
                                   ORDER BY
                                   id");

        $sql->bindValue(':id', addslashes($id));
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        try{
            $sql->execute();
            if($sql->rowCount() > 0){
                $venda = $sql->fetchAll();
                return json_encode(
                    array(
                        "code" => '200',
                        "venda" => $venda
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Nenhum registro'
                    )
                );
            }
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    public function atualizarCaixa($venda, $desconto, $acrescimo){
        $sql = $this->db->prepare("UPDATE
                                   venda_caixa
                                   SET
                                   total_vendas = total_vendas + :venda,
                                   total_desconto = total_desconto + :desconto,
                                   total_acrescimo = total_acrescimo + :acrescimo
                                   WHERE
                                   id = :id");
        $sql->bindValue(':venda', str_replace(',','.', $venda));
        $sql->bindValue(':desconto', str_replace(',', '.', $desconto));
        $sql->bindValue(':acrescimo', str_replace(',', '.', $desconto));
        $sql->bindValue(':id', $_SESSION['idCaixa']);
        try{
            $sql->execute();
            if($sql->rowCount() > 0){
                $venda = $sql->fetchAll();
                return json_encode(
                    array(
                        "code" => '200'
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Nenhum registro'
                    )
                );
            }
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    public function atualizarCaixaTotalGeral(){
        $sql = $this->db->prepare("UPDATE
                                   venda_caixa
                                   SET
                                   total_geral = ((total_vendas - total_desconto)  + fundo_caixa)
                                   WHERE
                                   id = :id");
        $sql->bindValue(':id', $_SESSION['idCaixa']);
        try{
            $sql->execute();
            if($sql->rowCount() > 0){
                return json_encode(
                    array(
                        "code" => '200'
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Nenhum registro'
                    )
                );
            }
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    

}