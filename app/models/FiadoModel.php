<?php 

class FiadoModel{

	function __construct($db){
		try{
			$this->db = $db;
		}catch(PDOException $e){
			exit("Não foi possivel conectar ao Banco de Dados");
		}
    }

    public function novo(){
        $query = "INSERT INTO
                  financeiro_fiado
                  SET
                  id_empresa = :id_empresa,
                  id_usuario = :id_usuario,
                  id_ator = :id_ator,
                  valor = :valor,
                  data_caixa = :caixa,
                  vencimento = :vencimento,
                  obs = :obs,
                  status = :status";
        if($_POST['status'] == '1'){
            $query .= ', data_pagamento = :pagamento';
        }
    	$sql = $this->db->prepare($query);
    	$sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
    	$sql->bindValue(':id_usuario', $_SESSION['idUsuario']);
    	$sql->bindValue(':id_ator', $_POST['cliente']);
    	$sql->bindValue(':valor', str_replace(',', '.', str_replace('.', '', $_POST['valor'] )));
    	$sql->bindValue(':caixa', implode('-', array_reverse(explode('/', addslashes($_POST['data'])))));
        $sql->bindValue(':vencimento', implode('-', array_reverse(explode('/', addslashes($_POST['vencimento'])))));
        $sql->bindValue(':obs', $_POST['obs']);
        $sql->bindValue(':status', $_POST['status']);
        if($_POST['status'] == '1'){
          $sql->bindValue(':pagamento', implode('-', array_reverse(explode('/', addslashes($_POST['pagamento'])))));
        }
        try {
            $sql->execute();
            if($this->db->lastInsertId() > 0){
                return json_encode(
                    array(
                        "code" => '200',
                        "mensagem" => 'Cadastrado com sucesso'
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Problema ao gravar'
                    )
                );
            }   
        } catch (PDOException $e) {
            return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => $e->getMessage()
                    )
                );
        }

    }

    public function editar($id){
        $sql = $this->db->prepare("UPDATE
                                   financeiro_fiado
                                   SET
                                   id_ator = :id_ator,
                                   valor = :valor,
                                   data_caixa = :caixa,
                                   vencimento = :vencimento,
                                   obs = :obs,
                                   status = :status,
                                   data_pagamento = :pagamento
                                   WHERE
                                   id = :id");
        $sql->bindValue(':id_ator', $_POST['cliente']);
        $sql->bindValue(':valor', str_replace(',', '.', str_replace('.', '', $_POST['valor'] )));
        $sql->bindValue(':caixa', implode('-', array_reverse(explode('/', addslashes($_POST['data'])))));
        $sql->bindValue(':vencimento', implode('-', array_reverse(explode('/', addslashes($_POST['vencimento'])))));
        $sql->bindValue(':obs', $_POST['obs']);
        $sql->bindValue(':id', $id);
        $sql->bindValue(':status', $_POST['status']);
        if($_POST['status'] == '1'){
            $sql->bindValue(':pagamento', implode('-', array_reverse(explode('/', addslashes($_POST['pagamento'])))));
        }else{
            $sql->bindValue(':pagamento', NULL);
        }
        try {
            $sql->execute();
            if($sql->rowCount() > 0){
                return json_encode(
                    array(
                        "code" => '202',
                        "mensagem" => 'Editado com sucesso'
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Dados não alterados'
                    )
                );
            }   
        } catch (PDOException $e) {
            return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => $e->getMessage()
                    )
                );
        }
    }

    public function excluir($id){
        $sql = $this->db->prepare("DELETE FROM
                                   financeiro_fiado
                                   WHERE
                                   id = :id
                                   AND
                                   id_empresa = :id_empresa");
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        $sql->bindValue(':id', $id);
        try {
            $sql->execute();
            if($sql->rowCount() > 0){
                return json_encode(
                    array(
                        "code" => '200',
                        "mensagem" => 'Excluido com sucesso',
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Erro ao excluir'
                    )
                );
            }   
        } catch (PDOException $e) {
            return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => $e->getMessage()
                    )
                );
        }
    }

    public function listar(){
    	$sql = $this->db->prepare("SELECT
                							   f.*,
                							   a.nome
                							   FROM
                							   financeiro_fiado f,
                							   ator a
                							   WHERE
                							   f.id_empresa = :id_empresa
                							   AND
                							   a.id = f.id_ator
                                 AND
                                 f.vencimento >= :data_inicio
                                 AND
                                 f.vencimento <= :data_final ");
    	$sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        $sql->bindValue(':data_final', date('Y-m-t'));
        $sql->bindValue(':data_inicio', date('Y-m-01')); 
    	try{
    		$sql->execute();
    		return $sql->fetchAll();
    	}catch(PDOException $e){
    		return array();
    	}
    }

    public function filtrarFiado(){
        $intervalo = explode(' - ', $_POST['intervalo']);
        $query = "SELECT
                  f.*,
                  a.nome
                  FROM
                  financeiro_fiado f,
                  ator a
                  WHERE
                  f.id_empresa = :id_empresa
                  AND
                  a.id = f.id_ator
                  AND
                  f.vencimento >= :data_inicio
                  AND
                  f.vencimento <= :data_final ";
        if($_POST['status'] != '99'){
            if($_POST['status'] == '2'){
                $query .= ' AND f.vencimento < :hoje
                            AND f.status = 0';
            }else{
                $query .= ' AND f.status = :status';
            }
        }
        $sql = $this->db->prepare($query);
        
        $sql->bindValue(':data_inicio', implode('-', array_reverse(explode('/', addslashes($intervalo[0])))));
        $sql->bindValue(':data_final', implode('-', array_reverse(explode('/', addslashes($intervalo[1])))));
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        if($_POST['status'] != '99'){
            if($_POST['status'] == '2'){
                $sql->bindValue(':hoje', date('Y-m-d'));
            }else{
                $sql->bindValue(':status', $_POST['status']);
            }
        }
        try{
            $sql->execute();
            if($sql->rowCount() > 0){
                return json_encode(
                    array(
                        "code" => '200',
                        "fiado" => $sql->fetchAll(),
                        "total_aberto" => $this->totalAberto($intervalo[0], $intervalo[1]),
                        "total_vencido" => $this->totalVencido($intervalo[0], $intervalo[1]),
                        "total_pago" => $this->totalPago($intervalo[0], $intervalo[1]),
                        "total_fiado" => $this->totalFiado($intervalo[0], $intervalo[1])
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => "Nenhum registro"
                    )
                );
            }
        }catch(PDOException $e){
            return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => $e->getMessage()
                    )
                );
        }
    }

    public function listarDetalheFiadoPorId($id){
        $sql = $this->db->prepare('SELECT 
                                   f.*,
                                   a.nome
                                   FROM
                                   financeiro_fiado f,
                                   ator a
                                   WHERE
                                   f.id_empresa = :id_empresa
                                   AND
                                   f.id = :id
                                   AND
                                   a.id = f.id_ator');
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        $sql->bindValue(':id', addslashes($id));
        
        try{
            $sql->execute();
            return $sql->fetch();
        }catch(PDOException $e){
            return $e->getMessage();
        }
    }


    public function totalAberto($data_inicio = '', $data_final = ''){
        $sql = $this->db->prepare("SELECT
                                   SUM(valor) as total
                                   FROM
                                   financeiro_fiado
                                   WHERE
                                   vencimento >= :dt_inicio
                                   AND
                                   vencimento <= :dt_limite
                                   AND
                                   id_empresa = :id_empresa
                                   AND
                                   status = '0'");
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        if($data_inicio == ''){
            $sql->bindValue(':dt_inicio', date('Y-m-01'));    
        }else{
            $sql->bindValue(':dt_inicio', implode('-', array_reverse(explode('/', addslashes($data_inicio)))));
        }
        if($data_final == ''){
            $sql->bindValue(':dt_limite', date('Y-m-t'));    
        }else{
            $sql->bindValue(':dt_limite', implode('-', array_reverse(explode('/', addslashes($data_final)))));
        }
        try{
            $sql->execute();
            $total = $sql->fetch();
            return $total['total'];
        }catch(PDOException $e){
            return array();
        }
    }

    public function totalVencido($data_inicio = '', $data_final = ''){
        $sql = $this->db->prepare("SELECT
                                   SUM(valor) as total
                                   FROM
                                   financeiro_fiado
                                   WHERE
                                   vencimento >= :dt_inicio
                                   AND
                                   vencimento <= :dt_limite
                                   AND
                                   id_empresa = :id_empresa
                                   AND
                                   status = '0'
                                   AND
                                   vencimento < :data");
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        $sql->bindValue(':data', date('Y-m-d'));
        if($data_inicio == ''){
            $sql->bindValue(':dt_inicio', date('Y-m-01'));    
        }else{
            $sql->bindValue(':dt_inicio', implode('-', array_reverse(explode('/', addslashes($data_inicio)))));
        }
        if($data_final == ''){
            $sql->bindValue(':dt_limite', date('Y-m-t'));    
        }else{
            $sql->bindValue(':dt_limite', implode('-', array_reverse(explode('/', addslashes($data_final)))));
        }
        try{
            $sql->execute();
            $total = $sql->fetch();
            return $total['total'];
        }catch(PDOException $e){
            return array();
        }
    }

    public function totalPago($data_inicio = '', $data_final = ''){
        $sql = $this->db->prepare("SELECT
                                   SUM(valor) as total
                                   FROM
                                   financeiro_fiado
                                   WHERE
                                   vencimento >= :dt_inicio
                                   AND
                                   vencimento <= :dt_limite
                                   AND
                                   id_empresa = :id_empresa
                                   AND
                                   status = '1'");
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        if($data_inicio == ''){
            $sql->bindValue(':dt_inicio', date('Y-m-01'));    
        }else{
            $sql->bindValue(':dt_inicio', implode('-', array_reverse(explode('/', addslashes($data_inicio)))));
        }
        if($data_final == ''){
            $sql->bindValue(':dt_limite', date('Y-m-t'));    
        }else{
            $sql->bindValue(':dt_limite', implode('-', array_reverse(explode('/', addslashes($data_final)))));
        }
        try{
            $sql->execute();
            $total = $sql->fetch();
            return $total['total'];
        }catch(PDOException $e){
            return array();
        }
    }

    public function totalFiado($data_inicio = '', $data_final = ''){
        $sql = $this->db->prepare("SELECT
                                   SUM(valor) as total
                                   FROM
                                   financeiro_fiado
                                   WHERE
                                   vencimento >= :dt_inicio
                                   AND
                                   vencimento <= :dt_limite
                                   AND
                                   id_empresa = :id_empresa");
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        if($data_inicio == ''){
            $sql->bindValue(':dt_inicio', date('Y-m-01'));    
        }else{
            $sql->bindValue(':dt_inicio', implode('-', array_reverse(explode('/', addslashes($data_inicio)))));
        }
        if($data_final == ''){
            $sql->bindValue(':dt_limite', date('Y-m-t'));    
        }else{
            $sql->bindValue(':dt_limite', implode('-', array_reverse(explode('/', addslashes($data_final)))));
        }
        try{
            $sql->execute();
            $total = $sql->fetch();
            return $total['total'];
        }catch(PDOException $e){
            return array();
        }
    }


    public function listarPorDataCaixa($data){
      $sql = $this->db->prepare("SELECT
                                 f.*,
                                 a.nome
                                 FROM
                                 financeiro_fiado f,
                                 ator a
                                 WHERE
                                 f.id_empresa = :id_empresa
                                 AND
                                 a.id = f.id_ator
                                 AND
                                 f.data_caixa = :data");
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        $sql->bindValue(':data', $data); 
        try{
            $sql->execute();
            return $sql->fetchAll();
        }catch(PDOException $e){
            return $e->getMessage();
        }
    }

}