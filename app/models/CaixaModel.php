<?php 

class CaixaModel{

	function __construct($db){
		try{
			$this->db = $db;
		}catch(PDOException $e){
			exit("Não foi possivel conectar ao Banco de Dados");
		}
    }

    public function abrirCaixa(){
        $sql = $this->db->prepare("INSERT INTO
                                   venda_caixa
                                   SET
                                   id_empresa = :id_empresa,
                                   id_usuario = :id_usuario,
                                   data_abertura = :data_abertura,
                                   hora_abertura = :hora_abertura,
                                   fundo_caixa = :fundo_caixa");

        $sql->bindValue(":id_empresa", $_SESSION['idEmpresa']);
        $sql->bindValue(":id_usuario", $_SESSION['idUsuario']);
        $sql->bindValue(":data_abertura", date('Y-m-d'));
        $sql->bindValue(':hora_abertura', date('H:i:s'));
        $sql->bindValue(':fundo_caixa', addslashes(str_replace(',', '.', $_POST['valor'])));
        try{
            $sql->execute();
            $id = $this->db->lastInsertId();
            if($id > 0){
                $_SESSION['idCaixa'] = $id;
                return json_encode(
                    array(
                        "code" => '200',
                        "mensagem" => 'Caixa aberto'
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Erro'
                    )
                );
            }
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    public function listar(){
        if(isset($_POST['intervalo'])){
            $intervalo = explode(' - ', $_POST['intervalo']);
        }
        $sql = $this->db->prepare("SELECT
                                   *
                                   FROM
                                   financeiro_caixa_cabecalho
                                   WHERE
                                   id_empresa = :id
                                   AND
                                   (data >= :data_i AND data <= :data_l)                                   
                                   ORDER BY
                                   data DESC");
        if(isset($_POST['intervalo'])){
            $sql->bindValue(':data_i', implode('-', array_reverse(explode('/', addslashes($intervalo[0])))));
            $sql->bindValue(':data_l', implode('-', array_reverse(explode('/', addslashes($intervalo[1])))));
        }else{
            $sql->bindValue(':data_i', date('Y-m-01'));
            $sql->bindValue(':data_l', date('Y-m-t'));
        }
        $sql->bindValue(':id', $_SESSION['idEmpresa']);
        try{
            $sql->execute();
            if($sql->rowCount() > 0){
                return json_encode(
                    array(
                        "code" => '200',
                        "caixa" => $sql->fetchAll(),
                        'detalhes' => $this->detalhesFiltroCaixa()
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Nenhum registro'
                    )
                );
            }
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    public function novoCaixa(){
    	$sql = $this->db->prepare("INSERT INTO
    							   financeiro_caixa_cabecalho
    							   SET
    							   id_empresa = :id_empresa,
    							   id_usuario = :id_usuario,
    							   data = :data,
    							   valor_total = :total,
    							   valor_caixa = :caixa,
                                   diferenca = :diferenca,
                                   valor_fiado = :fiado,
                                   valor_saida = :saida,
                                   total_cliente = :total_cliente,
                                   media_cliente = :ticket,
                                   obs = :obs");
    	$sql->bindValue(':id_empresa',  $_SESSION['idEmpresa']);
    	$sql->bindValue(':id_usuario', $_SESSION['idUsuario']);
    	$sql->bindValue(':data', implode('-', array_reverse(explode('/', addslashes($_POST['data'])))));
      $sql->bindValue(':total', str_replace(',', '.', str_replace('.', '', $_POST['total_venda'] )));
    	$sql->bindValue(':caixa', str_replace(',', '.', str_replace('.', '', $_POST['total_caixa'] )));
      $sql->bindValue(':diferenca', str_replace(',', '.', str_replace('.', '', $_POST['diferenca'] )));
      if($_POST['fiado'] > 0){
        $sql->bindValue(':fiado', str_replace(',', '.', str_replace('.', '', $_POST['fiado'] )));
      }else{
        $sql->bindValue(':fiado', 0);
      }
      if($_POST['saida'] > 0){
        $sql->bindValue(':saida', str_replace(',', '.', str_replace('.', '', $_POST['saida'] )));
      }else{
        $sql->bindValue(':saida', 0);
      }
      if($_POST['total_cliente'] > 0){
          $sql->bindValue(':total_cliente', $_POST['total_cliente']);
          $sql->bindValue(':ticket', $_POST['ticket']);
       }else{
          $sql->bindValue(':total_cliente', 0);
          $sql->bindValue(':ticket', 0);
       }
     
      $sql->bindValue(':obs', addslashes($_POST['obs']));
    	try{
    		$sql->execute();
    		$id = $this->db->lastInsertId();
    		if($id > 0){
                $ok = 0;
    			foreach ($_POST['forma_pagamento'] as $i => $fp) {
    				$ok += $this->insereDetalheCaixa($id, $i, str_replace(',', '.', str_replace('.', '', $fp)));
    			}
                if($ok == count($_POST['forma_pagamento'])){
                    return json_encode(
                        array('code' => '200', 'mensagem' => 'Caixa adicionado com sucesso')
                    );
                }
    		}
    	}catch(PDOException $e){
    		return json_encode(
                array('code' => '300', 'mensagem' => $e->getMessage())
            );
    	}
    }

    public function insereDetalheCaixa($id, $id_forma_pagamento, $valor){
    	$sql = $this->db->prepare("INSERT INTO
    							   financeiro_caixa_detalhe
    							   SET
    							   id_caixa_cabecalho = :id,
    							   id_forma_pagamento = :id_forma_pagamento,
    							   taxa_atual = :taxa_atual,
    							   dias_recebimento_atual = :dias_recebimento,
    							   valor_total = :valor_total,
    							   valor_liquido = :valor_liquido");
    	$sql->bindValue(':id', $id);
        $sql->bindValue(':id_forma_pagamento', $id_forma_pagamento);
        $forma_pagamento = $this->listarFormaPagamentoPorId($id_forma_pagamento);
    	$sql->bindValue(':taxa_atual', $forma_pagamento['taxa']);
    	$sql->bindValue(':dias_recebimento', $forma_pagamento['dias_recebimento']);
        if($valor > 0){
            $sql->bindValue(':valor_total',$valor);
            if($forma_pagamento['taxa'] > 0){
            $valor_liquido = $valor - ( $valor * ($forma_pagamento['taxa'] / 100) );
            }else{
                $valor_liquido = $valor;
            }
            $sql->bindValue(':valor_liquido', $valor_liquido);
        }else{
            $sql->bindValue(':valor_total', 0);
            $sql->bindValue(':valor_liquido', 0);
        }

    	
    	

    	try{
            $sql->execute();
            return 1;
        }catch(PDOException $e){
            return 0;
        }
    }

    public function listarDetalheCaixaPorId($id){
        $sql = $this->db->prepare('SELECT 
                                   d.* ,
                                   c.valor_total as total,
                                   c.valor_caixa as caixa,
                                   c.diferenca,
                                   c.id_usuario,
                                   c.obs,
                                   c.media_cliente,
                                   c.total_cliente,
                                   c.valor_fiado,
                                   c.valor_saida
                                   FROM
                                   financeiro_caixa_detalhe d
                                   INNER JOIN
                                   financeiro_caixa_cabecalho c
                                   ON 
                                   d.id_caixa_cabecalho = c.id
                                   AND
                                   c.id_empresa = :id_empresa
                                   AND
                                   d.id_caixa_cabecalho = :id');
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        $sql->bindValue(':id', addslashes($id));
        
        try{
            $sql->execute();
            return $sql->fetchAll();
        }catch(PDOException $e){
            return array();
        }
    }

    public function infoCaixa($id){
        $sql = $this->db->prepare('SELECT 
                                   d.* ,
                                   c.valor_total as total,
                                   c.valor_caixa as caixa,
                                   c.diferenca,
                                   c.id_usuario,
                                   c.obs,
                                   a.login,
                                   f.nome as forma_pagamento,
                                   c.valor_fiado as fiado,
                                   c.valor_saida as saida,
                                   c.diferenca,
                                   c.data
                                   FROM
                                   financeiro_caixa_detalhe d,
                                   ator_login a,
                                   financeiro_caixa_cabecalho c,
                                   financeiro_forma_pagamento f
                                   WHERE 
                                   d.id_caixa_cabecalho = c.id
                                   AND
                                   c.id_empresa = :id_empresa
                                   AND
                                   d.id_caixa_cabecalho = :id
                                   AND
                                   a.id = c.id_usuario
                                   AND
                                   f.id = d.id_forma_pagamento');
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        $sql->bindValue(':id', addslashes($id));
        
        try{
            $sql->execute();
            return $sql->fetchAll();
        }catch(PDOException $e){
            return array();
        }
    }

    public function getDataCaixa($id){
        $sql = $this->db->prepare('SELECT 
                                   c.data
                                   FROM
                                   financeiro_caixa_cabecalho c
                                   WHERE 
                                   c.id_empresa = :id_empresa
                                   AND
                                   c.id = :id');
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        $sql->bindValue(':id', addslashes($id));
        
        try{
            $sql->execute();
            return $sql->fetch();
        }catch(PDOException $e){
            return array();
        }
    }

    public function listarFormaPagamentoPorId($id){
        $sql = $this->db->prepare('SELECT 
                                   * 
                                   FROM
                                   financeiro_forma_pagamento
                                   WHERE
                                   id_empresa = :id_empresa
                                   AND
                                   id = :id');
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        $sql->bindValue(':id', addslashes($id));
        
        try{
            $sql->execute();
            return $sql->fetch();
        }catch(PDOException $e){
            return array();
        }   
    }

    public function detalhesFiltroCaixa(){
        if(isset($_POST['intervalo'])){
            $intervalo = explode(' - ', $_POST['intervalo']);
        }
        $sql = $this->db->prepare("SELECT
                                   SUM(valor_fiado) as fiado,
                                   SUM(valor_saida) as saida,
                                   SUM(valor_caixa) as caixa,
                                   SUM(valor_total) as venda,
                                   SUM(valor_total)/SUM(total_cliente) as media
                                   FROM
                                   financeiro_caixa_cabecalho
                                   WHERE
                                   (data >= :data_i AND data <= :data_l) 
                                   AND
                                   id_empresa = :id_empresa");
        if(isset($_POST['intervalo'])){
            $sql->bindValue(':data_i', implode('-', array_reverse(explode('/', addslashes($intervalo[0])))));
            $sql->bindValue(':data_l', implode('-', array_reverse(explode('/', addslashes($intervalo[1])))));
        }else{
            $sql->bindValue(':data_i', date('Y-m-01'));
            $sql->bindValue(':data_l', date('Y-m-t'));
        }
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        
        try{
            $sql->execute();
            $total = $sql->fetch();
            return $total;
        }catch(PDOException $e){
            return $e->getMessage();
        }
    }

    public function getCaixas(){
        $sql = $this->db->prepare("SELECT 
                                   c.id,
                                   c.data_abertura,
                                   c.status,
                                   c.total_geral,
                                   u.login
                                   FROM
                                   venda_caixa c,
                                   ator_login u
                                   WHERE
                                   c.id_empresa = :id_empresa
                                   AND
                                   c.id_usuario = u.id
                                   ORDER BY
                                   c.id DESC
                                   LIMIT 5");
            
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        try{
            $sql->execute();
            return $sql->fetchAll();
        }catch(PDOException $e){
            return $e;
        }
    }

    public function getInfoCaixa($id){
        $sql = $this->db->prepare("SELECT 
                                   c.*,
                                   u.login
                                   FROM
                                   venda_caixa c,
                                   ator_login u
                                   WHERE
                                   c.id = :id");

        $sql->bindValue(':id', $id);
        try{
            $sql->execute();
            if($sql->rowCount() > 0){
                return json_encode(
                    array(
                        'code' => '200',
                        'caixa' => $sql->fetch()
                    )
                );
            }else{
                return json_encode(
                    array(
                        'code' => '300',
                        'mensagem' => 'Nenhum caixa encontrado'
                    )
                );
            }   
        }catch(PDOException $e){
            return json_encode(
                array(
                    'code' => '300',
                    'mensagem' => $e->getMessage()
                )
            );
        }
    }

    public function encerrarCaixa(){
        $sql = $this->db->prepare("UPDATE
                                   venda_caixa
                                   SET
                                   status = '1',
                                   data_fechamento = :data,
                                   hora_fechamento = :hora
                                   WHERE
                                   id = :id");
        
        $sql->bindValue(':id', $_SESSION['idCaixa']);
        $sql->bindValue(':data', date('Y-m-d'));
        $sql->bindValue(':hora', date('H:i:s'));
        try{
            $sql->execute();
            if($sql->rowCount() > 0){
                $id = $_SESSION['idCaixa'];
                unset($_SESSION['idVenda']);
                unset($_SESSION['idCaixa']);
                return json_encode(
                    array(
                        'code' => '200',
                        'mensagem' => 'Caixa encerrado',
                        "id" => $id
                    )
                );
            }else{
                return json_encode(
                    array(
                        'code' => '300',
                        'mensagem' => 'Problema ao encerrar caixa'
                    )
                );
            }   
        }catch(PDOException $e){
            return json_encode(
                array(
                    'code' => '300',
                    'mensagem' => $e->getMessage()
                )
            );
        }
    }

}

?>