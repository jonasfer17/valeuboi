<?php 

class LancamentoModel{

	function __construct($db){
		try{
			$this->db = $db;
		}catch(PDOException $e){
			exit("Não foi possivel conectar ao Banco de Dados");
		}
    }

    public function listar(){
		if(isset($_POST['intervalo'])){
            $intervalo = explode(' - ', $_POST['intervalo']);
        }

        $query = " SELECT
                   f.*,
                   f.valor as total,
                   f.multa,
                   a.nome,
                   p.nome as forma_pagamento
                   FROM
                   financeiro_lancamento f,
                   ator a,
                   financeiro_forma_pagamento p
                   WHERE
                   f.id_empresa = :id_empresa
                   AND
                   a.id = f.id_ator
                   AND
                   p.id = f.id_forma_pagamento
                   AND
                   f.vencimento >= :data_i 
                   AND 
                   f.vencimento <= :data_l ";
        if(isset($_POST['tipo']) && $_POST['tipo'] != '99'){
            $query .= "AND tipo = :tipo ";
        }
        if(isset($_POST['categoria']) && $_POST['categoria'] != '99'){
            $query .= "AND id_categoria = :categoria";
        }

    	$sql = $this->db->prepare($query);
		$sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);

        if(isset($_POST['tipo']) && $_POST['tipo'] != '99'){
            if($_POST['tipo'] == 1){
                $sql->bindValue(':tipo', 1);
            }elseif ($_POST['tipo'] == 2) {
                 $sql->bindValue(':tipo', 0);
            }
        }

        if(isset($_POST['categoria']) && $_POST['categoria'] != '99'){
            $sql->bindValue(':categoria', $_POST['categoria']);
        }
        
		
    	if(isset($_POST['intervalo'])){
            $sql->bindValue(':data_i', implode('-', array_reverse(explode('/', addslashes($intervalo[0])))));
            $sql->bindValue(':data_l', implode('-', array_reverse(explode('/', addslashes($intervalo[1])))));
        }else{
            $sql->bindValue(':data_i', date('Y-m-01'));
            $sql->bindValue(':data_l', date('Y-m-t'));
        }

    	try{
            $sql->execute();
                return json_encode(
                    array(
                        "code" => '200',
                        "lista" => $sql->fetchAll(),
						"receita" => $this->totalReceitas(),
						"despesa" => $this->totalDespesas()
                    )
                );
            
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage(),
                    "q" => var_dump($sql)
                )
            );
        }
	}



    public function listarPorId($id){
        $sql = $this->db->prepare("SELECT
                                   f.*
                                   FROM
                                   financeiro_lancamento f
                                   WHERE
                                   f.id_empresa = :id_empresa
                                   AND
                                   f.id = :id");
        $sql->bindValue(':id', addslashes($id));
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);

        try{
            $sql->execute();
            if($sql->rowCount() > 0){
                return json_encode(
                    array(
                        "code" => '200',
                        "lancamento" => $sql->fetch(),
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => 'Nenhum registro'
                    )
                );
            }
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }
	
	public function totalReceitas(){
        if(isset($_POST['intervalo'])){
            $intervalo = explode(' - ', $_POST['intervalo']);
        }

        $query = " SELECT
                   COALESCE(SUM(valor+multa),0) as total
                   FROM
                   financeiro_lancamento
                   WHERE
                   (vencimento >= :data_i AND vencimento <= :data_l) 
                   AND
                   id_empresa = :id_empresa
                   AND
                   tipo = '0' ";

        if(isset($_POST['tipo']) && $_POST['tipo'] != '99'){
            $query .= "AND tipo = :tipo ";
        }
        if(isset($_POST['categoria']) && $_POST['categoria'] != '99'){
            $query .= "AND id_categoria = :categoria";
        }

        $sql = $this->db->prepare($query);

        if(isset($_POST['tipo']) && $_POST['tipo'] != '99'){
            if($_POST['tipo'] == 1){
            $sql->bindValue(':tipo', 1);
            }elseif ($_POST['tipo'] == 2) {
                 $sql->bindValue(':tipo', 0);
            }
        }
        


        if(isset($_POST['categoria']) && $_POST['categoria'] != '99'){
            $sql->bindValue(':categoria', $_POST['categoria']);
        }

        if(isset($_POST['intervalo'])){
            $sql->bindValue(':data_i', implode('-', array_reverse(explode('/', addslashes($intervalo[0])))));
            $sql->bindValue(':data_l', implode('-', array_reverse(explode('/', addslashes($intervalo[1])))));
        }else{
            $sql->bindValue(':data_i', date('Y-m-01'));
            $sql->bindValue(':data_l', date('Y-m-t'));
        }
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        
        try{
            $sql->execute();
            $total = $sql->fetch();
            return $total['total'];
        }catch(PDOException $e){
            return $e->getMessage();
        }
	}
	
	public function totalDespesas(){
        if(isset($_POST['intervalo'])){
            $intervalo = explode(' - ', $_POST['intervalo']);
        }

        $query = " SELECT
                   COALESCE(SUM(valor+multa),0) as total
                   FROM
                   financeiro_lancamento
                   WHERE
                   (vencimento >= :data_i AND vencimento <= :data_l) 
                   AND
                   id_empresa = :id_empresa
                   AND
                   tipo = '1'";

        if(isset($_POST['tipo']) && $_POST['tipo'] != '99'){
            $query .= "AND tipo = :tipo ";
        }
        if(isset($_POST['categoria']) && $_POST['categoria'] != '99'){
            $query .= "AND id_categoria = :categoria";
        }

        $sql = $this->db->prepare($query);

        if(isset($_POST['tipo']) && $_POST['tipo'] != '99'){
            if($_POST['tipo'] == 1){
            $sql->bindValue(':tipo', 1);
            }elseif ($_POST['tipo'] == 2) {
                 $sql->bindValue(':tipo', 0);
            }
        }
        
        if(isset($_POST['categoria']) && $_POST['categoria'] != '99'){
            $sql->bindValue(':categoria', $_POST['categoria']);
        }


        if(isset($_POST['intervalo'])){
            $sql->bindValue(':data_i', implode('-', array_reverse(explode('/', addslashes($intervalo[0])))));
            $sql->bindValue(':data_l', implode('-', array_reverse(explode('/', addslashes($intervalo[1])))));
        }else{
            $sql->bindValue(':data_i', date('Y-m-01'));
            $sql->bindValue(':data_l', date('Y-m-t'));
        }
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        
        try{
            $sql->execute();
            $total = $sql->fetch();
            return $total['total'];
        }catch(PDOException $e){
            return $e->getMessage();
        }
	}
	
	

    public function novo(){

    	$query = "INSERT INTO
				  financeiro_lancamento
				  SET
				  id_empresa = :id_empresa,
				  id_usuario = :id_usuario,
				  id_ator = :id_ator,
				  id_categoria = :id_categoria,
				  descricao = :descricao,
				  id_forma_pagamento = :id_forma_pagamento,
				  tipo = :tipo,
				  valor = :valor, 
				  data_caixa = :data_caixa,
                  numero_doc = :num_doc ";
		if(isset($_POST['vencimento']) && !empty($_POST['vencimento'])){
			$query .= ', vencimento = :vencimento ';
		}

		if($_POST['status'] == '1'){
			$query .= ', status = 1, 
					   pago_em = :pago_em';
            if($_POST['multa'] != ''){
                $query .= ', multa = :multa';
            }
		}else{
			$query .= ', status = 0';
		}

		if($_POST['repete'] == '0'){
			$query .= ", repete = 0, 
					   vezes = :vezes";
		}else{
			$query .= ", repete = 1, 
					   vezes = :vezes ";
		}




    	$sql = $this->db->prepare($query);
    	$sql->bindValue(':id_empresa', addslashes($_SESSION['idEmpresa']));
    	$sql->bindValue(':id_usuario', addslashes($_SESSION['idUsuario']));
    	$sql->bindValue(':id_ator', addslashes($_POST['ator']));
    	$sql->bindValue(':id_categoria', addslashes($_POST['categoria']));
    	$sql->bindValue(':descricao', addslashes($_POST['descricao']));
    	$sql->bindValue(':id_forma_pagamento', addslashes($_POST['forma_pagamento']));
        if($_POST['tipo'] == '2'){
            $sql->bindValue(':tipo', '0');
        }else{
            $sql->bindValue(':tipo', addslashes($_POST['tipo']));
        }
		
		$sql->bindValue(':data_caixa', implode('-', array_reverse(explode('/', $_POST['data_caixa']))));
    	$sql->bindValue(':vencimento', implode('-', array_reverse(explode('/', addslashes($_POST['vencimento'])))));
    	$sql->bindValue(':num_doc', addslashes($_POST['num_doc']));
    	$sql->bindValue(':valor', str_replace(',', '.', str_replace('.', '', addslashes($_POST['valor']))));
		if($_POST['status'] == '1'){
			$sql->bindValue(':pago_em', implode('-', array_reverse(explode('/', addslashes($_POST['pago_em'])))));
            if($_POST['multa'] != ''){
                $sql->bindValue(':multa', implode('-', array_reverse(explode('/', addslashes($_POST['multa'])))));
            }
		} 
		
    	if($_POST['repete'] == '0'){
    		$sql->bindValue(':vezes', '0');
    	}else{
    		$sql->bindValue(':vezes', addslashes($_POST['intervalo']));
    	}
    	
    	try{
    		$sql->execute();
    		$id = $this->db->lastInsertId();
    		if($id > 0){
    			if($_POST['repete'] == '1'){
    				$vezes = $_POST['intervalo'];
    				$vencimento = explode('/', $_POST['vencimento']);
    				for($i = 0; $i < $vezes-1; $i++) {
    					if($vencimento[1] >= 12){
    						$this->repetirLancamento($id, '01', $vencimento[2]);
    						$vencimento[1] = '01';
    						$vencimento[2] += 1;
    					}else{
    						$this->repetirLancamento($id, $vencimento[1]+1, $vencimento[2]);
    						$vencimento[1] += 1;
    					}
    				}
    			}
    			return json_encode(
	                array(
	                    'code' => '200',
	                    'mensagem' => 'Lançamento salvo'
	                )
	            );
    		}else{
    			return json_encode(
	                array(
	                    'code' => '200',
	                    'mensagem' => 'Lançamento salvo'
	                )
	            );
    		}
    		
    	}catch(PDOException $e){
    		return json_encode(
				array(
					'code' => '300',
					'mensagem' => $e->getMessage()
				)
			);
    	}
    }

    public function editar($id){

        $query = "UPDATE
                  financeiro_lancamento
                  SET
                  id_usuario = :id_usuario,
                  id_ator = :id_ator,
                  id_categoria = :id_categoria,
                  descricao = :descricao,
                  id_forma_pagamento = :id_forma_pagamento,
                  tipo = :tipo,
                  valor = :valor, 
                  data_caixa = :data_caixa,
                  numero_doc = :num_doc ";
        if(isset($_POST['vencimento']) && !empty($_POST['vencimento'])){
            $query .= ', vencimento = :vencimento ';
        }

        if($_POST['status'] == '1'){
            $query .= ', status = 1, 
                       pago_em = :pago_em';
            if($_POST['multa'] != ''){
                $query .= ', multa = :multa';
            }
        }else{
            $query .= ',pago_em = null
                       , status = 0
                       , multa = 0 ';
        }

        $query .= " WHERE id = :id";


        $sql = $this->db->prepare($query);
        $sql->bindValue(':id', addslashes($id));
        $sql->bindValue(':id_usuario', addslashes($_SESSION['idUsuario']));
        $sql->bindValue(':id_ator', addslashes($_POST['ator']));
        $sql->bindValue(':id_categoria', addslashes($_POST['categoria']));
        $sql->bindValue(':descricao', addslashes($_POST['descricao']));
        $sql->bindValue(':id_forma_pagamento', addslashes($_POST['forma_pagamento']));
        if($_POST['tipo'] == '2'){
            $sql->bindValue(':tipo', '0');
        }else{
            $sql->bindValue(':tipo', addslashes($_POST['tipo']));
        }
        $sql->bindValue(':data_caixa', implode('-', array_reverse(explode('/', $_POST['data_caixa']))));
        $sql->bindValue(':vencimento', implode('-', array_reverse(explode('/', addslashes($_POST['vencimento'])))));
        $sql->bindValue(':num_doc', addslashes($_POST['num_doc']));
        $sql->bindValue(':valor', str_replace(',', '.', str_replace('.', '', addslashes($_POST['valor']))));
        if($_POST['status'] == '1'){
            $sql->bindValue(':pago_em', implode('-', array_reverse(explode('/', addslashes($_POST['pago_em'])))));
            if($_POST['multa'] != ''){
                $sql->bindValue(':multa', str_replace(',', '.', $_POST['multa']));
            }
        } 
        
        try{
            $sql->execute();
            if($sql->rowCount() > 0){
                return json_encode(
                    array(
                        'code' => '202',
                        'mensagem' => 'Editado com sucessso!'
                    )
                );
            }else{
                return json_encode(
                    array(
                        'code' => '300',
                        'mensagem' => 'Nada alterado'
                    )
                );
            }
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    'code' => '300',
                    'mensagem' => $e->getMessage()
                )
            );
        }
    }


    public function repetirLancamento($id, $mes, $ano){

    	$query = "INSERT INTO
				  financeiro_lancamento
				  SET
				  id_empresa = :id_empresa,
				  id_usuario = :id_usuario,
				  id_ator = :id_ator,
				  id_categoria = :id_categoria,
				  descricao = :descricao,
				  id_forma_pagamento = :id_forma_pagamento,
				  tipo = :tipo,
				  valor = :valor, 
				  id_lancamento = :id_lancamento,
				  repete = 0,
				  status = 0";
		if(isset($_POST['vencimento']) && !empty($_POST['vencimento'])){
			$query .= ', vencimento = :vencimento';
		}

		if(isset($_POST['total']) && $_POST['total'] != ''){
			$query .= ', valor_total = :valor_total';
		}else{
			$query .= ',valor_total = :valor';
		}


    	$sql = $this->db->prepare($query);
    	$sql->bindValue(':id_empresa', addslashes($_SESSION['idEmpresa']));
    	$sql->bindValue(':id_usuario', addslashes($_SESSION['idUsuario']));
    	$sql->bindValue(':id_ator', addslashes($_POST['ator']));
    	$sql->bindValue(':id_categoria', addslashes($_POST['categoria']));
    	$sql->bindValue(':descricao', addslashes($_POST['descricao']));
    	$sql->bindValue(':id_forma_pagamento', addslashes($_POST['forma_pagamento']));
		$sql->bindValue(':tipo', addslashes($_POST['tipo']));
    	if(isset($_POST['vencimento']) && !empty($_POST['vencimento'])){
    		$vencimento = explode('/', $_POST['vencimento']);
    		$sql->bindValue(':vencimento', $ano.'-'.$mes.'-'.$vencimento[0]);
    	}
    	$sql->bindValue(':valor', str_replace(',', '.', str_replace('.', '', addslashes($_POST['valor']))));
    	if(isset($_POST['total']) && $_POST['total'] != ''){
    		$sql->bindValue(':valor_total', str_replace(',', '.', str_replace('.', '', addslashes($_POST['total']))));
    	}

    	$sql->bindValue(":id_lancamento", $id);

    	try{
    		$sql->execute();
    		$id = $this->db->lastInsertId();
    		if($id > 0){
    			return true;
    		}else{
    			return false;
    		}
    		
    	}catch(PDOException $e){
    		print_r($e->getMessage());
    	}
    }

    public function filtrarLancamento(){
        $intervalo = explode(' - ', $_POST['intervalo']);
        $query = "SELECT
                  f.*,
                  a.nome
                  FROM
                  financeiro_lancamento f,
                  ator a
                  WHERE
                  f.id_empresa = :id_empresa
                  AND
                  a.id = f.id_ator
                  AND
                  f.vencimento >= :data_inicio
                  AND
                  f.vencimento <= :data_final ";
        if($_POST['status'] != '99'){
            if($_POST['status'] == '2'){
                $query .= ' AND f.vencimento < :hoje
                            AND f.status = 0';
            }else{
                $query .= ' AND f.status = :status';
            }
        }
        $sql = $this->db->prepare($query);
        
        $sql->bindValue(':data_inicio', implode('-', array_reverse(explode('/', addslashes($intervalo[0])))));
        $sql->bindValue(':data_final', implode('-', array_reverse(explode('/', addslashes($intervalo[1])))));
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        if($_POST['status'] != '99'){
            if($_POST['status'] == '2'){
                $sql->bindValue(':hoje', date('Y-m-d'));
            }else{
                $sql->bindValue(':status', $_POST['status']);
            }
        }
        try{
            $sql->execute();
            if($sql->rowCount() > 0){
                return json_encode(
                    array(
                        "code" => '200',
                        "lancamento" => $sql->fetchAll(),
                        
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => "Nenhum registro"
                    )
                );
            }
        }catch(PDOException $e){
            return json_encode(
                    array(
                        "code" => '300',
                        "mensagem" => $e->getMessage()
                    )
                );
        }
    }

    public function listarDetalheFiadoPorId($id){
        $sql = $this->db->prepare('SELECT 
                                   f.*,
                                   a.nome
                                   FROM
                                   financeiro_fiado f,
                                   ator a
                                   WHERE
                                   f.id_empresa = :id_empresa
                                   AND
                                   f.id = :id
                                   AND
                                   a.id = f.id_ator');
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        $sql->bindValue(':id', addslashes($id));
        
        try{
            $sql->execute();
            return $sql->fetch();
        }catch(PDOException $e){
            return $e->getMessage();
        }
    }

    public function listarPorDataCaixa($data){
          $sql = $this->db->prepare("SELECT
                                     f.*,
                                     a.nome
                                     FROM
                                     financeiro_lancamento f,
                                     ator a
                                     WHERE
                                     f.id_empresa = :id_empresa
                                     AND
                                     a.id = f.id_ator
                                     AND
                                     f.data_caixa = :data
                                     AND
                                     tipo = '1'");
            $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
            $sql->bindValue(':data', $data); 
            try{
                $sql->execute();
                return $sql->fetchAll();
            }catch(PDOException $e){
                return $e->getMessage();
            }
    }

}