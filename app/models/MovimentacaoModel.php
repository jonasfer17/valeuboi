<?php 

class MovimentacaoModel{

	function __construct($db){
  		try{
  			$this->db = $db;
  		}catch(PDOException $e){
  			exit("Não foi possivel conectar ao Banco de Dados");
  		}
    }

    public function inserir($id_item, $id_venda, $descricao, $quantidade, $ajuste, $venda, $custo){
    	$sql = $this->db->prepare("INSERT INTO
    							   movimentacao
    							   SET
    							   id_empresa = :id_empresa,
    							   id_usuario = :id_usuario,
    							   id_item = :id_item,
    							   descricao = :descricao,
    							   quantidade = :quantidade,
    							   ajuste = :ajuste,
    							   valor_venda = :valor_venda,
    							   valor_produto = :valor_produto,
    							   valor_comissao = '0'");

    	$sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
    	$sql->bindValue(':id_usuario', $_SESSION['idUsuario']);
    	$sql->bindValue(':id_item', addslashes(intval($id_item)));
    	$sql->bindValue(':descricao', addslashes($descricao));
    	$sql->bindValue(':quantidade', addslashes($quantidade));
    	$sql->bindValue(':ajuste', addslashes($ajuste));
    	$sql->bindValue(':valor_venda', addslashes(str_replace(',', '.', $venda)));
    	$sql->bindValue(':valor_produto', addslashes(str_replace(',', '.', $custo)));

    	try{
    		$sql->execute();
    		if($this->db->lastInsertId() > 0){
    			return array(
                        "code" => '200',
                        "id" => $this->db->lastInsertId()
                    );
    		}else{
    			return array(
                        "code" => '300',
                        "mensagem" => 'Erro nenhum dado adicionado'
                    );
    		}
    	}catch(PDOException $e){
    		return array(
                        "code" => '300',
                        "mensagem" => $e->getMessage()
                    );
    	}
    }
}
