<?php 

require_once 'MovimentacaoModel.php';

class ProdutoModel{

    private $movimentacao = null;

	function __construct($db){
		try{
			 $this->db = $db;
		}catch(PDOException $e){
			exit("Não foi possivel conectar ao Banco de Dados");
		}
        $this->movimentacao = new MovimentacaoModel($db);
    }

    public function listar(){
    	$query = "SELECT 
    			  * 
    			  FROM
    			  produto
    			  WHERE
    			  id_empresa = :id_empresa";


    	$sql = $this->db->prepare($query);
    	$sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
    	try{
            $sql->execute();
                return json_encode(
                    array(
                        "code" => '200',
                        "lista" => $sql->fetchAll()
                    )
                );
            
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    public function listarPorId($id){
        $query = "SELECT 
                  * 
                  FROM
                  produto
                  WHERE
                  id_empresa = :id_empresa
                  AND
                  id = :id";


        $sql = $this->db->prepare($query);
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        $sql->bindValue(':id', addslashes(intval($id)));
        try{
            $sql->execute();
                return json_encode(
                    array(
                        "code" => '200',
                        "produto" => $sql->fetch()
                    )
                );
            
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    public function pesquisarProduto($txt){
        $query = "SELECT 
                  id,
                  nome,
                  gtin,
                  preco_venda 
                  FROM
                  produto
                  WHERE
                  id_empresa = :id_empresa
                  AND
                  (gtin LIKE :txt
                  OR
                  codigo_interno LIKE :txt
                  OR
                  nome LIKE :txt)
                  LIMIT 2";
        $sql = $this->db->prepare($query);
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        $sql->bindValue(':txt', '%'.addslashes($txt).'%');
        try{
            $sql->execute();
            if($sql->rowCount() > 0){
                return json_encode(
                    array(
                        "code" => '200',
                        "produtos" => $sql->fetchAll()
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '204',
                        "mensagem" => 'Produto não encontrado'
                    )
                );
            }
                
            
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    public function pesquisarProdutoPorCodigo($txt){
        $query = "SELECT 
                  id,
                  nome,
                  gtin,
                  preco_venda 
                  FROM
                  produto
                  WHERE
                  id_empresa = :id_empresa
                  AND
                  gtin = :txt
                  LIMIT 1";
        $sql = $this->db->prepare($query);
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        $sql->bindValue(':txt', addslashes($txt));
        try{
            $sql->execute();
            if($sql->rowCount() > 0){
                return json_encode(
                    array(
                        "code" => '200',
                        "produto" => $sql->fetch()
                    )
                );
            }else{
                return json_encode(
                    array(
                        "code" => '204',
                        "mensagem" => 'Produto não encontrado'
                    )
                );
            }
                
            
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    public function cadastrar(){
        $sql = $this->db->prepare("INSERT INTO 
                                   produto
                                   SET
                                   id_empresa = :id_empresa,
                                   codigo_interno = :codigo,
                                   gtin = :gtin,
                                   nome = :nome,
                                   id_unidade_medida = :unidade_medida,
                                   id_grupo = :id_grupo,
                                   preco_compra = :preco_compra,
                                   preco_custo = :preco_custo,
                                   preco_sugerido = :preco_sugerido,
                                   margem = :margem,
                                   preco_venda = :preco_venda,
                                   margem_real = :margem_real");
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        $codigo = $this->getCodigoInterno();
        $sql->bindValue(':codigo', $codigo['cod_produto'] + 1);
        $sql->bindValue(':gtin', $_POST['gtin']);
        $sql->bindValue(':nome', addslashes($_POST['nome']));
        $sql->bindValue(':unidade_medida', addslashes($_POST['unidade_medida']));
        $sql->bindValue(':id_grupo', addslashes(intval($_POST['grupo'])));
        $sql->bindValue(':preco_compra', addslashes(str_replace(',', '.', $_POST['preco_compra'])));
        $sql->bindValue(':preco_custo', addslashes(str_replace(',', '.', $_POST['preco_custo'])));
        $sql->bindValue(':preco_sugerido', addslashes(str_replace(',', '.', $_POST['preco_sugerido'])));
        $sql->bindValue(':margem', addslashes(str_replace(',', '.', $_POST['margem'])));
        $sql->bindValue(':preco_venda', addslashes(str_replace(',', '.', $_POST['preco_venda'])));
        $sql->bindValue(':margem_real', addslashes(str_replace(',', '.', $_POST['margem_real'])));

        try{
            $sql->execute();
            $id = $this->db->lastInsertId();
            if($id > 0){
                $this->setCodigoInterno($id ,$codigo['cod_produto']+1);
                $this->movimentacao->inserir($id, '0', 'Cadastro de produtos', '0', '0', $_POST['preco_venda'], $_POST['preco_custo']);
                return json_encode(
                    array(
                        "code" => '200',
                        "mensagem" => 'Cadastrado com sucesso'
                    )
                );
            }else{
                return json_encode(
                    array(
                    "code" => '300',
                    "mensagem" => 'Erro'
                    )
                );
            }
            
        }catch(PDOException $e){
             return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

     public function editar($id){
        $sql = $this->db->prepare("UPDATE
                                   produto
                                   SET
                                   gtin = :gtin,
                                   nome = :nome,
                                   id_unidade_medida = :unidade_medida,
                                   id_grupo = :id_grupo,
                                   preco_compra = :preco_compra,
                                   preco_custo = :preco_custo,
                                   preco_sugerido = :preco_sugerido,
                                   margem = :margem,
                                   preco_venda = :preco_venda,
                                   margem_real = :margem_real
                                   WHERE
                                   id = :id
                                   AND
                                   id_empresa = :id_empresa");
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        $sql->bindValue(':id', addslashes(intval($id)));
        $sql->bindValue(':gtin', $_POST['gtin']);
        $sql->bindValue(':nome', addslashes($_POST['nome']));
        $sql->bindValue(':unidade_medida', addslashes($_POST['unidade_medida']));
        $sql->bindValue(':id_grupo', addslashes($_POST['grupo']));
        $sql->bindValue(':preco_compra', addslashes(str_replace(',', '.', $_POST['preco_compra'])));
        $sql->bindValue(':preco_custo', addslashes(str_replace(',', '.', $_POST['preco_custo'])));
        $sql->bindValue(':preco_sugerido', addslashes(str_replace(',', '.', $_POST['preco_sugerido'])));
        $sql->bindValue(':margem', addslashes(str_replace(',', '.', $_POST['margem'])));
        $sql->bindValue(':preco_venda', addslashes(str_replace(',', '.', $_POST['preco_venda'])));
        $sql->bindValue(':margem_real', addslashes(str_replace(',', '.', $_POST['margem_real'])));

        try{
            $sql->execute();
            if($sql->rowCount() > 0){
                return json_encode(
                    array(
                        "code" => '202',
                        "mensagem" => 'Editado com sucesso'
                    )
                );
            } else{
                return json_encode(
                    array(
                    "code" => '300',
                    "mensagem" => 'Nada alterado'
                    )
                );
            } 
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    
    public function getCodigoInterno(){
        $sql = $this->db->prepare('SELECT
                                   COALESCE(MAX(codigo_interno),0) as cod_produto
                                   FROM
                                   produto_codigo
                                   WHERE
                                   id_empresa = :id_empresa');
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        try{
            $sql->execute();
            if($sql->rowCount() > 0){
                return $sql->fetch();
            }
        }catch(PDOException $e){
            return false;
        }
    }

    public function setCodigoInterno($id_produto ,$codigo_interno){
        $sql = $this->db->prepare('INSERT INTO
                                   produto_codigo
                                   SET
                                   id_empresa = :id_empresa,
                                   id_produto = :id_produto,
                                   codigo_interno = :codigo_interno');
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        $sql->bindValue(':id_produto', $id_produto);
        $sql->bindValue(':codigo_interno', $codigo_interno);
        try{
            $sql->execute();
        }catch(PDOException $e){
            return false;
        }
    }

    public function getGtin($gtin){
        $sql = $this->db->prepare("SELECT
                                   gtin
                                   FROM
                                   produto
                                   WHERE
                                   id_empresa = :id_empresa
                                   AND
                                   gtin = :gtin");
        $sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
        $sql->bindValue(':gtin', $gtin);
        try{
            $sql->execute();
            if($sql->rowCount() > 0){
                return false;
            }else{
                return true;
            }
        }catch(PDOException $e){
            return false;
        }
    }

}
