<?php 

class ProdutoGrupoModel{

	function __construct($db){
		try{
			$this->db = $db;
		}catch(PDOException $e){
			exit("Não foi possivel conectar ao Banco de Dados");
		}
    }

    public function listar(){
    	$sql = $this->db->prepare("SELECT
    							   *
    							   FROM
    							   produto_grupo
    							   WHERE
    							   id_empresa = :id_empresa");

    	$sql->bindValue(":id_empresa", $_SESSION['idEmpresa']);
    	try{
            $sql->execute();
                return json_encode(
                    array(
                        "code" => '200',
                        "lista" => $sql->fetchAll()
                    )
                );
            
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    public function listarPorId($id){
    	$sql = $this->db->prepare("SELECT
    							   *
    							   FROM
    							   produto_grupo
    							   WHERE
    							   id_empresa = :id_empresa
    							   AND
    							   id = :id");

    	$sql->bindValue(":id_empresa", $_SESSION['idEmpresa']);
    	$sql->bindValue(':id', addslashes(intval($id)));
    	try{
            $sql->execute();
                return json_encode(
                    array(
                        "code" => '200',
                        "grupo" => $sql->fetch()
                    )
                );
            
            
        }catch(PDOException $e){
            return json_encode(
                array(
                    "code" => '300',
                    "mensagem" => $e->getMessage()
                )
            );
        }
    }

    public function cadastrar(){
    	$sql = $this->db->prepare("INSERT INTO
    							   produto_grupo
    							   SET
    							   id_empresa = :id_empresa,
    							   nome = :nome");

    	$sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
    	$sql->bindValue(':nome', addslashes($_POST['nome']));
    	try{
    		$sql->execute();
    		if($this->db->lastInsertId() > 0){
    			return json_encode(
    				array(
    					'code' => '200',
    					'mensagem' => 'Cadastrado com sucesso',
    					'pagina' => 'grupo'
    				)
    			);
    		}else{
    			return json_encode(
    				array(
    					'code' => '300',
    					'mensagem' => 'Erro ao cadastrar',
    					'pagina' => 'grupo'
    				)
    			);
    		}
    	}catch(PDOException $e){
    		return json_encode(
				array(
					'code' => '300',
					'mensagem' => $e->getMessage(),
					'pagina' => 'grupo'
				)
			);
    	}
    }

    public function editar($id){
    	$sql = $this->db->prepare("UPDATE
    							   produto_grupo
    							   SET
    							   nome = :nome
    							   WHERE
    							   id_empresa = :id_empresa
    							   AND
    							   id = :id");

    	$sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
    	$sql->bindValue(':nome', addslashes($_POST['nome']));
    	$sql->bindValue(':id', addslashes(intval($id)));
    	try{
    		$sql->execute();
    		if($sql->rowCount() > 0){
    			return json_encode(
    				array(
    					'code' => '202',
    					'mensagem' => 'Editado com sucesso',
    					'pagina' => 'grupo'
    				)
    			);
    		}else{
    			return json_encode(
    				array(
    					'code' => '300',
    					'mensagem' => 'Nada foi alterado',
    					'pagina' => 'grupo'
    				)
    			);
    		}
    	}catch(PDOException $e){
    		return json_encode(
				array(
					'code' => '300',
					'mensagem' => $e->getMessage(),
					'pagina' => 'grupo'
				)
			);
    	}
    }

    public function excluir($id){
    	$sql = $this->db->prepare("DELETE FROM
    							   produto_grupo
    							   WHERE
    							   id_empresa = :id_empresa
    							   AND
    							   id = :id");

    	$sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
    	$sql->bindValue(':id', addslashes(intval($id)));
    	try{
    		$sql->execute();
    		if($sql->rowCount() > 0){
    			return json_encode(
    				array(
    					'code' => '200',
    					'mensagem' => 'Excluido com sucesso',
    					'pagina' => 'grupo'
    				)
    			);
    		}else{
    			return json_encode(
    				array(
    					'code' => '300',
    					'mensagem' => 'Nada foi alterado',
    					'pagina' => 'grupo'
    				)
    			);
    		}
    	}catch(PDOException $e){
    		return json_encode(
				array(
					'code' => '300',
					'mensagem' => $e->getMessage(),
					'pagina' => 'grupo'
				)
			);
    	}
    }

}