<?php 

class CategoriaModel{

	function __construct($db){
		try{
			$this->db = $db;
		}catch(PDOException $e){
			exit("Não foi possivel conectar ao Banco de Dados");
		}
    }


    public function cadastrar(){
    	$sql = $this->db->prepare("INSERT INTO financeiro_categoria
    							   SET
    							   id_empresa = :id_empresa,
    							   nome = :nome");
    	$sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
    	$sql->bindValue(':nome', addslashes(trim(ucfirst($_POST['nome']))));
    	try{
    		$sql->execute();
    		if($this->db->lastInsertId() > 0){
    			return json_encode(
    				array(
    					'codigo' => '200',
    					'mensagem' => 'Cadastrado com sucesso'
    				)
    			);
    		}else{
    			return json_encode(
    				array(
    					'codigo' => '300',
    					'mensagem' => 'Erro ao cadastrar'
    				)
    			);
    		}
    	}catch(PDOException $e){
				return json_encode(
    				array(
    					'codigo' => '301',
    					'mensagem' => $e->getMessage()
    				)
    			);
    	}
    }

    public function listar(){
    	$sql = $this->db->prepare('SELECT 
    							   * 
    							   FROM
    							   financeiro_categoria
    							   WHERE
    							   id_empresa = :id_empresa');
    	$sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
    	$sql->execute();
    	return $sql->fetchAll();
    }

    public function listarPorId($id){
    	$sql = $this->db->prepare('SELECT 
    							   * 
    							   FROM
    							   financeiro_categoria
    							   WHERE
    							   id_empresa = :id_empresa
    							   AND
    							   id = :id');
    	$sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
    	$sql->bindValue(':id', addslashes($id));
    	
    	try{
    		$sql->execute();
    		return $sql->fetch();
    	}catch(PDOException $e){
    		return array();
    	}
    	
    }

    public function editar($id){
    	$sql = $this->db->prepare("UPDATE 
    							   financeiro_categoria
    							   SET
    							   nome = :nome
    							   WHERE
    							   id = :id
    							   AND
    							   id_empresa = :id_empresa");
    	$sql->bindValue(':id', addslashes($id));
    	$sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
    	$sql->bindValue(':nome', addslashes(trim(ucfirst($_POST['nome']))));
    	try{
    		$sql->execute();
    		if($sql->rowCount() > 0){
    			return json_encode(
    				array(
    					'codigo' => '202',
    					'mensagem' => 'Editado com sucesso'
    				)
    			);
    		}else{
    			return json_encode(
    				array(
    					'codigo' => '300',
    					'mensagem' => 'Erro ao editar'
    				)
    			);
    		}
    	}catch(PDOException $e){
				return json_encode(
    				array(
    					'codigo' => '301',
    					'mensagem' => $e->getMessage()
    				)
    			);
    	}
    }


    public function deletar($id){
    	$sql = $this->db->prepare("DELETE FROM
    							   financeiro_categoria
    							   WHERE
    							   id = :id
    							   AND
    							   id_empresa = :id_empresa");
    	$sql->bindValue(':id', addslashes($id));
    	$sql->bindValue(':id_empresa', $_SESSION['idEmpresa']);
    	try{
    		$sql->execute();
    		if($sql->rowCount() > 0){
    			return json_encode(
    				array(
    					'codigo' => '200',
    					'mensagem' => 'Excluido com sucesso'
    				)
    			);
    		}else{
    			return json_encode(
    				array(
    					'codigo' => '300',
    					'mensagem' => 'Erro ao excluir'
    				)
    			);
    		}
    	}catch(PDOException $e){
				return json_encode(
    				array(
    					'codigo' => '301',
    					'mensagem' => $e->getMessage()
    				)
    			);
    	}

    }
}