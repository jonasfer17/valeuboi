<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Projeto</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo CSS;?>bootstrap.min.css">
   
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo CSS;?>font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo CSS;?>ionicons.min.css">
    <!-- Sweet Alert -->
    <link rel="stylesheet" href="<?php echo CSS;?>sweetalert2.min.css">
    <!-- Sweet Alert -->
    <link rel="stylesheet" href="<?php echo CSS;?>select2.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo CSS;?>blue.css">
    <link rel="stylesheet" href="<?php echo CSS;?>dataTables.bootstrap.css">
    <!-- dateranger -->
    <link rel="stylesheet" href="<?php echo CSS;?>daterangepicker.css">
    <link rel="stylesheet" href="<?php echo CSS;?>bootstrap-datepicker.min.css">
    <!-- Charts -->
    <link rel="stylesheet" href="<?php echo CSS;?>morris.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo CSS;?>AdminLTE.css">

    <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo CSS;?>_all-skins.min.css">
    

    <!-- Pace style
    <link rel="stylesheet" href="<?php echo CSS;?>pace.css">
 -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="<?php echo JS;?>jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?php echo JS;?>bootstrap.min.js"></script>
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>
<body class="hold-transition skin-red sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo URL;?>" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>ERP</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">VALEU BOI</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
    
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="https://via.placeholder.com/160x160" class="user-image" alt="User Image">
                            <span class="hidden-xs"><?php echo $_SESSION['login'];?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="https://via.placeholder.com/160x160" class="img-circle" alt="User Image">
                                <p>
                                    <?php echo $_SESSION['nome'];?>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body">
                                <div class="row">
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Perfil</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Configurações</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="<?php echo URL;?>login/logout">Sair</a>
                                    </div>
                                </div>
                                <!-- /.row -->
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                </ul>
            </div>
        </nav>
    </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel text-center">
                <div class="image">
                    <img src="https://via.placeholder.com/150x150" class="img-circle" alt="User Image">
                </div>
            </div>
            <!-- search form -->
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">MENU</li>
                <li <?php if(isset($menu) && $menu == 'admin'){ echo  'class="active"'; }?> >
                    <a href="<?php echo URL;?>admin"><i class="fa fa-book"></i> <span>Home</span></a>
                </li>
                <li <?php if(isset($menu) && $menu == 'pdv'){ echo  'class="active"'; }?> >
                    <a href="<?php echo URL;?>pdv"><i class="fa fa-book"></i> <span>PDV</span></a>
                </li>
                <li <?php if(isset($menu) && $menu == 'pessoa'){ echo  'class="active"'; }?> >
                    <a href="<?php echo URL;?>pessoa" ><i class="fa fa-book"></i> <span>Clientes / Fornecedores</span></a>
                </li>
                <li class="treeview <?php if(isset($menu) && $menu == 'produto'){ echo 'menu-open'; }?> ">
                    <a href="#">
                        <i class="fa fa-users"></i> <span>Produtos</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu" <?php if(isset($menu) && $menu == 'produto'){ echo 'style="display: block;"'; }?> >
                        <li <?php if(isset($submenu) && $submenu == 'produto'){ echo  'class="active"'; }?>> 
                            <a href="<?php echo URL;?>produto"><i class="fa fa-circle-o"></i> Produto</a>
                        </li>
                        <li <?php if(isset($submenu) && $submenu == 'grupo'){ echo  'class="active"'; }?>> 
                            <a href="<?php echo URL;?>produto/grupo"><i class="fa fa-circle-o"></i> Grupo de Produto</a>
                        </li>
                        <li <?php if(isset($submenu) && $submenu == 'unidade_medida'){ echo  'class="active"'; }?>> 
                            <a href="<?php echo URL;?>produto/unidademedida"><i class="fa fa-circle-o"></i>Unidade de medidas</a>
                        </li>
                    </ul>
                </li>
                <li class="treeview <?php if(isset($menu) && $menu == 'financeiro'){ echo 'menu-open'; }?> ">
                    <a href="#">
                        <i class="fa fa-users"></i> <span>Financeiro</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu" <?php if(isset($menu) && $menu == 'financeiro'){ echo 'style="display: block;"'; }?> >
                        <li <?php if(isset($submenu) && $submenu == 'caixa'){ echo  'class="active"'; }?>> 
                            <a href="<?php echo URL;?>financeiro/caixa"><i class="fa fa-circle-o"></i> Caixa</a>
                        </li>
                        <li <?php if(isset($submenu) && $submenu == 'fiado'){ echo  'class="active"'; }?>> 
                            <a href="<?php echo URL;?>financeiro/fiado"><i class="fa fa-circle-o"></i> Fiado</a>
                        </li>
                        <li <?php if(isset($submenu) && $submenu == 'lancamento'){ echo  'class="active"'; }?>> 
                            <a href="<?php echo URL;?>financeiro/lancamento"><i class="fa fa-circle-o"></i> Lançamentos</a>
                        </li>
                    </ul>
                </li>
                <li class="treeview <?php if(isset($menu) && $menu == 'cadastros'){ echo 'menu-open'; }?> ">
                    <a href="#">
                        <i class="fa fa-users"></i> <span>Cadastros</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu" <?php if(isset($menu) && $menu == 'cadastros'){ echo 'style="display: block;"'; }?> >
                        <li <?php if(isset($submenu) && $submenu == 'forma_pagamento'){ echo 'class="active"'; }?>> 
                            <a href="<?php echo URL;?>cadastro/formapagamento"><i class="fa fa-circle-o"></i> Formas de pagamento</a>
                        </li>
                        <li <?php if(isset($submenu) && $submenu == 'categoria'){ echo  'class="active"'; }?>> 
                            <a href="<?php echo URL;?>cadastro/categoria"><i class="fa fa-circle-o"></i> Categorias</a>
                        </li>
                        <!--
                        <li <?php if(isset($submenu) && $submenu == 'subcategoria'){ echo 'class="active"'; }?>> 
                            <a href="<?php echo URL;?>cadastro/subcategoria"><i class="fa fa-circle-o"></i> Subcategorias</a>
                        </li>
                        -->
                    </ul>
                </li>
                <li class="treeview <?php if(isset($menu) && $menu == 'configuracao'){ echo 'menu-open'; }?> ">
                    <a href="#">
                        <i class="fa fa-cog"></i> <span>Configuração</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu" <?php if(isset($menu) && $menu == 'configuracao'){ echo 'style="display: block;"'; }?> >
                        <li <?php if(isset($submenu) && $submenu == 'empresa'){ echo 'class="active"'; }?>> 
                            <a href="<?php echo URL;?>configuracao/empresa"><i class="fa fa-circle-o"></i> Empresa</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php if(isset($titulo)){ echo $titulo; }?>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">