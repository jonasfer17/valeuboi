            </section>
        </div>
    </div>
    
    <div class="modal fade" id="modalOpcCaixa">
        <div class="modal-dialog">
            <div class="modal-content"style="margin-top: 20%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Operações do caixa</h4>
                </div>
                <form method="POST" id="form-novo-caixa">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                Caixa aberto em 
                            </div>
                        </div>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal">
                        Cancelar
                    </button>
                    <button type="submit" class="btn btn-primary btn-flat" id="btn-salvar">
                        Salvar e ir ao PDV
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>


</div>

<script src="<?php echo JS;?>pace.min.js"></script>
<script src="<?php echo JS;?>jquery.slimscroll.min.js"></script>
<script src="<?php echo JS;?>sweetalert2.min.js"></script>
<script src="<?php echo JS;?>fastclick.js"></script>
<script src="<?php echo JS;?>jquery.mask.js"></script>
<script src="<?php echo JS;?>jquery.validate.min.js"></script>
<script src="<?php echo JS;?>select2.full.min.js"></script>
<script src="<?php echo JS;?>adminlte.min.js"></script>
<script>
     var URL = "<?php echo URL;?>";
</script>
<script src="<?php echo JS;?>script.js"></script>
<script src="<?php echo JS;?>page/pdv.js"></script>
</body>
</html>