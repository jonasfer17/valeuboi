<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sys - PDV</title>
  
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" href="<?php echo CSS;?>bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo CSS;?>font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo CSS;?>ionicons.min.css">
    <link rel="stylesheet" href="<?php echo CSS;?>sweetalert2.min.css">
    <link rel="stylesheet" href="<?php echo CSS;?>select2.min.css">
    <link rel="stylesheet" href="<?php echo CSS;?>AdminLTE.css">
    <link rel="stylesheet" href="<?php echo CSS;?>_all-skins.min.css">

    <script src="<?php echo JS;?>jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?php echo JS;?>bootstrap.min.js"></script>

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-red layout-top-nav" >
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="<?php echo URL;?>admin/" class="navbar-brand">&nbsp;&nbsp;&nbsp;&nbsp;VALEU BOI</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            
          </ul>
          
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="<?php echo URL;?>">
                            <i class="fa fa-arrow-circle-left"></i><span class="hidden-xs">Voltar para o menu</span>
                        </a>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
          </ul>
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <section class="content">