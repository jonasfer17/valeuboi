
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Versão</b> 1.0.0
    </div>
    <strong><?php echo date('Y')?><a href="https://thepetz.com.br"> Jonas Fernandes</a>.
  </footer>

  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- jQuery 3 -->

<!-- PACE -->
<script src="<?php echo JS;?>pace.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo JS;?>jquery.slimscroll.min.js"></script>
<!-- Mask -->
<script src="<?php echo JS;?>jquery.mask.js"></script>
<!-- Validate -->
<script src="<?php echo JS;?>jquery.validate.min.js"></script>
<!-- Sweet Alert -->
<script src="<?php echo JS;?>sweetalert2.min.js"></script>

<script src="<?php echo JS;?>jquery.dataTables.min.js"></script>
<script src="<?php echo JS;?>dataTables.bootstrap.min.js"></script>
<!--

<script src="<?php echo JS;?>Chart.min.js"></script>

<script src="<?php echo JS;?>jquery.flot.js"></script>

<script src="<?php echo JS;?>jquery.flot.resize.js"></script>

<script src="<?php echo JS;?>jquery.flot.pie.js"></script>

<script src="<?php echo JS;?>raphael.min.js"></script>
<script src="<?php echo JS;?>morris.min.js"></script>
-->
<!-- Select 2 -->
<script src="<?php echo JS;?>select2.full.min.js"></script>
<!-- Daterange -->
<script src="<?php echo JS;?>moment.js"></script>
<script src="<?php echo JS;?>bootstrap-datepicker.js"></script>
<!--
<script src="<?php echo JS;?>bootstrap-datepicker.pt-BR.js"></script>
-->
<script src="<?php echo JS;?>daterangepicker.js"></script>
<!-- FastClick -->
<script src="<?php echo JS;?>fastclick.js"></script>
<!-- iCheck -->
<script src="<?php echo JS;?>icheck.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo JS;?>adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo JS;?>demo.js"></script>
<!-- page script -->
<script type="text/javascript">
  // To make Pace works on Ajax calls


    $(function () {


        $('.select2').select2();
        $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue'
        });
        $('#reservation').daterangepicker({
            locale: {
              format: 'DD/MM/YYYY'
            }
        });
        $("#reservation").data('daterangepicker').setStartDate("<?php echo date('01/m/Y');?>");
        $("#reservation").data('daterangepicker').setEndDate("<?php echo date('t/m/Y');?>");
    });
  var URL = "<?php echo URL;?>";
</script>
<script src="<?php echo JS;?>script.js"></script>
<?php if(isset($script)):?>
<script src="<?php echo JS.$script;?>"></script>
<?php endif;?>

</body>
</html>
