
    <div class="box">
        <div class="box-body" style="min-height: 450px;">
            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <form class="form" method="post" id="form-categoria" <?php if(isset($categoria['id'])){ ?> data-url="editarCategoria/<?php echo $categoria['id'];?>" <?php }else{ ?> data-url="cadastrarCategoria/" <?php } ?> >
                        <div class="row">
                            <div class="col-md-12" style="margin-top: 10%;">
                                <label>Categoria</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="nome" id="nome" value="<?php if(isset($categoria['nome'])){ echo $categoria['nome']; }?>" autofocus="true">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-flat pull-right">Salvar</button>
                                <a href="<?php echo URL;?>cadastro/categoria">
                                    <button type="button" class="btn btn-default btn-flat pull-right">Voltar</button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

            
        