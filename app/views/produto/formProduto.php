<?php if(isset($produto)){
    extract($produto);
}?>
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-tag"></i> Produtos</h3>
        </div>
        <form class="form" method="POST" id="form-produto" <?php if(isset($produto)){ echo 'data-url="editarProduto/'.$produto['id'].'"'; }else{ echo 'data-url="cadastrarProduto"'; }?> >
            <div class="box-body">
                <div class="row">
                   <div class="col-md-12">
                        <small style="color: gray;">Deixe em branco para gerar um código automático</small>
                    </div>
                    <div class="col-md-2">
                        <label>GTIN</label>
                        <div class="form-group">
                            <input type="text" name="gtin" class="form-control gtin" <?php if(isset($produto)){ echo 'value="'.$produto['gtin'].'"'; }  ?> >   
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <label>Nome</label>
                        <div class="form-group">
                            <input type="text" name="nome" class="form-control" <?php if(isset($produto)){ echo 'value="'.$produto['nome'].'"'; }  ?> >
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label>Unidade de Medida</label>
                        <div class="form-group">
                            <select class="form-control" name="unidade_medida">
                                <option value="0">Selecione</option>
                                <option value="KG"  <?php if(isset($produto)){ if($produto['unidade_medida'] == 'KG'){ echo 'selected'; } }  ?> >KG</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <label>Preço de compra</label>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    R$
                                </div>
                                <input type="text" name="preco_compra" class="form-control money"  <?php if(isset($produto)){ echo 'value="'.$produto['preco_compra'].'"'; }  ?> >
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label>Preço de custo</label>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    R$
                                </div>
                                <input type="text" name="preco_custo" id="preco_custo" class="form-control money margem"  <?php if(isset($produto)){ echo 'value="'.$produto['preco_custo'].'"'; }  ?> >
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label>Margem de lucro</label>
                        <div class="form-group">
                            <div class="input-group">
                                 <div class="input-group-addon">
                                    %
                                </div>
                                <input type="text" name="margem" id="margem_lucro" class="form-control porcentagem margem"  <?php if(isset($produto)){ echo 'value="'.$produto['margem'].'"'; }  ?> >
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label>Preço sugerido</label>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    R$
                                </div>
                                <input type="text" name="preco_sugerido" id="preco_sugerido"  <?php if(isset($produto)){ echo 'value="'.$produto['preco_sugerido'].'"'; }  ?>  class="form-control money" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label>Preço de venda</label>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    R$
                                </div>
                                <input type="text" name="preco_venda" id="preco_venda"  <?php if(isset($produto)){ echo 'value="'.$produto['preco_venda'].'"'; }  ?>  class="form-control money">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label>Margem real</label>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    %
                                </div>
                                <input type="text" name="margem_real" id="margem_real"  <?php if(isset($produto)){ echo 'value="'.$produto['margem_real'].'"'; }  ?>  class="form-control porcentagem" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" id="btn-salvar" class="btn btn-info btn-flat pull-right">Salvar</button>
                <a href="<?php echo URL;?>produto/">
                    <button type="button" class="btn btn-default btn-flat pull-right">Voltar</button>
                </a>
            </div>
        </form>
    </div>
              