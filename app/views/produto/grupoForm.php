<?php if(isset($grupo)){
    extract($grupo);
}?>
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-tag"></i> Grupos de Produtos</h3>
        </div>
        <form class="form" method="POST" id="form-sec" <?php if(isset($grupo)){ echo 'data-url="editarGrupo/'.$grupo['id'].'"'; }else{ echo 'data-url="cadastrarGrupo"'; }?> >
            <div class="box-body">
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <label>Nome</label>
                        <div class="form-group">
                            <input type="text" name="nome" class="form-control" <?php if(isset($grupo)){ echo 'value="'.$grupo['nome'].'"'; }  ?> >
                        </div>
                    </div>
                </div>
                <br>
            </div>
            <div class="box-footer">
                <button type="submit" id="btn-salvar" class="btn btn-primary btn-flat pull-right">Salvar</button>
                <a href="<?php echo URL;?>produto/grupo">
                    <button type="button" class="btn btn-default btn-flat pull-right">Voltar</button>
                </a>
            </div>
        </form>
    </div>
              