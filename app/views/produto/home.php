<?php extract($produto);?>
<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Produtos</h3>
               <div class="box-tools">
                    <br>
                    <a href="<?php echo URL;?>produto/formulario">
                        <button class="btn bg-navy">
                            <i class="fa fa-search"></i> &nbsp;&nbsp;Cadastrar
                        </button>
                    </a>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <br>
                <form method="post" id="form-filtro-lancamento">
                    <div class="row">
                        
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-default btn-flat"><i class="fa fa-search"></i> Pesquisar</button>
                        </div>
                    </div>
                </form>
                <br><br>
                <div class="table-responsive">
                        <table id="tabela" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="text-left">Código</th>
                                    <th class="text-center">Nome</th>
                                    <th class="text-center">Valor</th>
                                    <th class="text-right">Operação</th>
                                </tr>
                            </thead>
                            <tbody id="tabela-produto">
                            <?php
                                    foreach ($produto['lista'] as $p):
                                ?>
                                <tr>
                                    <td class="text-left"><?php echo $p['gtin']; ?></td>
                                    <td class="text-center"><?php echo $p['nome']; ?></td>
                                    <td class="text-center">R$ <?php echo number_format($p['preco_venda'], 2, ',', '.'); ?></td>
                                    <td class="text-right">
                                        <a href="<?php echo URL;?>produto/formulario/<?php echo $p['id']; ?>">
                                            <button type="button" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></button>
                                        </a>
                                        <button type="button" class="btn btn-danger btn-flat"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                                <?php 
                                        endforeach;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->



