<?php if(isset($unidade_medida)){
    extract($unidade_medida);
}?>
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-tag"></i> Unidades de Medidas</h3>
        </div>
        <form class="form" method="POST" id="form-sec" <?php if(isset($unidade_medida)){ echo 'data-url="editarUnidadeMedida/'.$unidade_medida['id'].'"'; }else{ echo 'data-url="cadastrarUnidadeMedida"'; }?> >
            <div class="box-body">
                <br>
                <div class="row">
                    <div class="col-md-6">
                        <label>Nome</label>
                        <div class="form-group">
                            <input type="text" name="nome" class="form-control" <?php if(isset($unidade_medida)){ echo 'value="'.$unidade_medida['nome'].'"'; }  ?> >
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label>Fator</label>
                        <div class="form-group">
                            <input type="text" name="fator" class="form-control porcentagem" <?php if(isset($unidade_medida)){ echo 'value="'.$unidade_medida['fator'].'"'; }  ?> >
                        </div>
                    </div>
                </div>
                <br>
            </div>
            <div class="box-footer">
                <button type="submit" id="btn-salvar" class="btn btn-primary btn-flat pull-right">Salvar</button>
                <a href="<?php echo URL;?>produto/unidademedida">
                    <button type="button" class="btn btn-default btn-flat pull-right">Voltar</button>
                </a>
            </div>
        </form>
    </div>
              