<?php extract($grupo);?>
<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Gupos de Produtos</h3>
               <div class="box-tools">
                    <br>
                    <a href="<?php echo URL;?>produto/formularioGrupo">
                        <button class="btn bg-navy">
                            <i class="fa fa-search"></i> &nbsp;&nbsp;Cadastrar
                        </button>
                    </a>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                
                <br><br>
                <div class="table-responsive">
                        <table id="tabela" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">Nome</th>
                                    <th class="text-right">Operação</th>
                                </tr>
                            </thead>
                            <tbody id="tabela-grupo">
                            <?php
                                    foreach ($grupo['lista'] as $g):
                                ?>
                                
                                <tr>
                                    <td class="text-center"><?php echo $g['nome']; ?></td>
                                    <td class="text-right">
                                        <a href="<?php echo URL;?>produto/formulariogrupo/<?php echo $g['id']; ?>">
                                            <button type="button" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></button>
                                        </a>
                                        <button type="button" class="btn btn-danger btn-flat btn-excluir" data-id="<?php echo $g['id'];?>" data-url="excluirGrupo/<?php echo $g['id'];?>"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                                <?php 
                                    endforeach;
                                ?>
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->



