<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Clientes / Fornecedores</h3>
                <div class="box-tools">
                    <br>
                    <br>
                    <button class="btn bg-navy" id="btn-add">
                        <i class="fa fa-search"></i> &nbsp;&nbsp;Cadastrar
                    </button>
                </div>
            </div>
            <br>
            <br>
            <br>
            <div class="col-md-9 col-sm-9 col-xs-6"></div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="form-group-sm">
                    <input type="text" id="input-filtro" class="form-control" placeholder="Pesquisar por nome">
                </div>
            </div>
            <br><br>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome</th>
                            <th class="text-right">Ação</th>
                        </tr>
                    </thead>
                    <tbody id="tabela-corpo">
                        <?php foreach($pessoa as $p):?>
                            <tr  data-url="cliente/info/1">
                                <td><?php echo $p['id']?></td>
                                <td><?php echo $p['nome']?></td>
                                <td class="text-right">
                                    <button class="btn btn-flat btn-default btn-editar" data-nome="<?php echo $p['nome']; ?>" data-id="<?php echo $p['id'];?>">
                                        <i class="fa fa-cog"></i>
                                    </button>
                                    <button class="btn btn-flat btn-danger  btn-deletar" data-id="<?php echo $p['id'];?>">
                                        <i class="fa fa-trash" ></i>
                                    </button>
                                </td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        <!-- /.box-body -->
        </div>
    <!-- /.box -->
    </div>
</div>

<div class="modal fade" id="modal-default">
    <form id="form-pessoa" method="POST">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Clientes / Fornecedores</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                         <label for="nome">Nome</label>
                        <div class="form-group">
                            <input type="text" class="form-control" id="nome" name="nome" placeholder="Informe o nome">
                        </div>
                    </div>
                  <!-- /.box-body -->

                  
                </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-flat btn-primary">Salvar</button>
                    <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Voltar</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </form>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

