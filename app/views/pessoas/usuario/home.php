        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                        <div class="box-tools">
                        <?php if(in_array('200', $permissao)): ?>
                            <div class="row">
                                <a href="<?php echo URL;?>usuario/formularioUsuario">
                                 <button type="button" class="btn btn-primary btn-flat margin pull-right" id="btn-cadastrar" data-title="CADASTRAR USUÁRIO" data-form="form-cad-usuario"><i class="fa fa-plus"></i> Cadastrar</button>
                                </a>
                            </div>
                        <?php endif; ?>
                        </div>
                        <br><br><br>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="input-group input-group-sm">
                                    <input type="text" id="filtro-tabela" class="form-control" placeholder="Pesquisar">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-block btn-sm btn-default btn-flat">Filtro</button>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive no-padding">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th >#</th>
                                    <th class="text-center">Nome</th>
                                    <th class="text-right">Operação</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr >
                                    <td>1</td>
                                    <td class="text-center">Nome</td>
                                    <td class="text-right">
                                    <?php if(in_array('201', $permissao)): ?>
                                        <button type="button" class="btn btn-default btn-flat btn-editar" data-id="1" data-title="EDITAR USUÁRIO" data-form="form-edit-usuario"><i class="fa fa-pencil"></i></button>
                                    <?php endif; ?>
                                    <?php if(in_array('202', $permissao)): ?>    
                                        <button type="button" class="btn btn-danger btn-flat"><i class="fa fa-trash"></i></button>
                                    <?php endif; ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>          
        </div>

