        <div class="row">
            <div class="col-md-12">
                <div class="box" style="min-height: 450px">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                        <div class="box-tools">
                            <div class="row">
                                <a href="<?php echo URL;?>usuario/formularioGrupo">
                                <button type="button" class="btn btn-primary btn-flat margin pull-right"><i class="fa fa-plus"></i> Cadastrar</button>
                                </a>
                            </div>
                        </div>
                        <br><br><br>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="input-group input-group-sm">
                                    <input type="text" id="filtro-tabela" class="form-control" placeholder="Pesquisar">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-block btn-sm btn-default btn-flat">Filtro</button>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive no-padding">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">Nome</th>
                                    <th class="text-right">Operação</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($grupo as $gp):?>
                                <tr id="<?php echo $gp['id']; ?>">
                                    <td class="text-center"><?php echo $gp['nome']?></td>
                                    <td class="text-right">
                                        <a href="<?php echo URL;?>usuario/formularioGrupo/<?php echo $gp['id'];?>">
                                            <button type="button" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></button>
                                        </a>
                                        <button type="button" class="btn btn-danger btn-flat btn-excluir-grupo" data-id="<?php echo $gp['id'];?>"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>          
        </div>

       