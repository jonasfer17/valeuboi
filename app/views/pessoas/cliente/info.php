<div class="row">
    <div class="col-md-3">
          <!-- Profile Image -->
        <div class="box box-primary">
            <div class="box-body box-profile">
                <img class="profile-user-img img-responsive img-circle" src="https://via.placeholder.com/128x128" alt="User profile picture">
                <h3 class="profile-username text-center">Nome</h3>
                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b>Vendas</b> <a class="pull-right">1,322</a>
                    </li>
                    <li class="list-group-item">
                        <b>Produtos adiquiridos</b> <a class="pull-right">543</a>
                    </li>
                </ul>
            </div>
        <!-- /.box-body -->
        </div>
        <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Sobre</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <strong><i class="fa fa-map-marker margin-r-5"></i> Endereço</strong>

                    <p class="text-muted">
                        Lorem ipsum dolor sit amet.
                    </p>
                    <hr>
                    <strong><i class="fa fa-phone margin-r-5"></i> Contato</strong>

                    <p class="text-muted">Lorem ipsum dolor sit amet. </p>

                    <hr>

                    <strong><i class="fa fa-file-text-o margin-r-5"></i> Observações</strong>

                    <p>Lorem ipsum dolor sit amet.</p>
                </div>
                <!-- /.box-body -->
            </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#historico" data-toggle="tab">Histórico</a></li>
            </ul>
            <div class="tab-content">
                <div class="active tab-pane" id="historico">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="box box-solid">
                                <div class="box-header with-border">
                                <h3 class="box-title">Produtos comprados recentemente</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <ul class="products-list product-list-in-box">
                                        <li class="item">
                                            <div class="product-img">
                                                <img src="https://via.placeholder.com/50x50" alt="Product Image">
                                            </div>
                                            <div class="product-info">
                                                <a href="javascript:void(0)" class="product-title">Samsung TV
                                                <span class="label label-warning pull-right">$1800</span></a>
                                                <span class="product-description">
                                                    Samsung 32" 1080p 60Hz LED Smart HDTV.
                                                </span>
                                            </div>
                                            </li>
                                            <!-- /.item -->
                                            <li class="item">
                                                <div class="product-img">
                                                    <img src="https://via.placeholder.com/50x50" alt="Product Image">
                                                </div>
                                                <div class="product-info">
                                                    <a href="javascript:void(0)" class="product-title">Bicycle
                                                    <span class="label label-info pull-right">$700</span></a>
                                                    <span class="product-description">
                                                        26" Mongoose Dolomite Men's 7-speed, Navy Blue.
                                                    </span>
                                                </div>
                                            </li>
                                            <!-- /.item -->
                                            <li class="item">
                                                <div class="product-img">
                                                    <img src="https://via.placeholder.com/50x50" alt="Product Image">
                                                </div>
                                                <div class="product-info">
                                                    <a href="javascript:void(0)" class="product-title">Xbox One <span class="label label-danger pull-right">$350</span></a>
                                                    <span class="product-description">
                                                        Xbox One Console Bundle with Halo Master Chief Collection.
                                                    </span>
                                                </div>
                                            </li>
                                        </li>
                                        <!-- /.item -->
                                    </ul>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.tab-pane -->
              
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- /.nav-tabs-custom -->
    </div>
<!-- /.col -->
</div>