
<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Caixa</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-offset-3 col-sm-2 col-xs-6">
                        <div class="description-block border-right">
                            <h5 class="description-header" id="totalDespesa">
                                R$ <?php 
                                    if(isset($lancamento['despesa'])){ 
                                        if($lancamento['despesa'] > 0){ 
                                            echo number_format($lancamento['despesa'], 2, ',',''); 
                                        }else{ 
                                            echo "0,00"; 
                                        }
                                    }else{
                                        echo "0,00";
                                    } ?></h5>
                            <span class="description-text">TOTAL DE DESPESAS</span>
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-6">
                        <div class="description-block border-right">
                            <h5 class="description-header" id="totalReceita">
                                R$ <?php 
                                    if(isset($lancamento['receita'])){ 
                                        if($lancamento['receita'] > 0){ 
                                            echo number_format($lancamento['receita'], 2, ',',''); 
                                        }else{ 
                                            echo "0,00"; 
                                        }
                                    }else{
                                        echo "0,00";
                                    } ?>
                            </h5>
                            <span class="description-text">TOTAL DE RECEITAS</span>
                        </div>
                    </div>
                    <a href="<?php echo URL;?>financeiro/formularioLancamento">
                        <button type="button" class="btn btn-primary btn-flat margin pull-right"><i class="fa fa-plus"></i> Adicionar</button>
                    </a>
                </div>
                <br>
                <form method="post" id="form-filtro-lancamento">
                    <div class="row">
                        <div class="col-md-3">
                            <select class="form-control" name="tipo">
                                <option value="99">Todas as contas</option>
                                <option value="2">Receitas</option>
                                <option value="1">Despesas</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select class="form-control" name="categoria">
                                <option value="99">Todas as categorias</option>
                                <?php foreach($categoria as $cat):?>
                                    <option value="<?php echo $cat['id']; ?>"><?php echo $cat['nome'];?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="reservation" name="intervalo">
                                </div>
                              </div>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-default btn-flat"><i class="fa fa-search"></i> Pesquisar</button>
                        </div>
                    </div>
                </form>
                <br><br>
                <div class="table-responsive">
                        <table id="tabela" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">Tipo</th>
                                    <th class="text-center">Descrição</th>
                                    <th class="text-center">Cliente/Fornecedor</th>
                                    <th class="text-center">Forma de pagamento</th>
                                    <th class="text-center">Valor</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Vencimento</th>
                                    <th class="text-right">Operação</th>
                                </tr>
                            </thead>
                            <tbody id="tabela-lancamento">
                            <?php
                                    foreach ($lancamento['lista'] as $f):
                                ?>
                                    <tr>
                                        <td class="text-center"><?php if($f['tipo'] == 1){ echo 'Despesa'; }else{ echo 'Receita'; }?></td>
                                        <td class="text-center"><?php echo $f['descricao']; ?></td>
                                        <td class="text-center"><?php echo $f['nome']; ?></td>
                                        <td class="text-center"><?php echo $f['forma_pagamento']?></td>
                                        <td class="text-center"><?php echo number_format($f['total'],2,',','.'); ?></td>
                                        <td class="text-center">
                                        <?php
                                            if($f['status'] == '1'){
                                        ?>
                                                <span class="label label-success">Pago</span>
                                        <?php
                                            }else{
                                                if($f['vencimento'] < date('Y-m-d')){
                                        ?>
                                                <span class="label label-danger">Atrasado</span>
                                        <?php
                                                }else{
                                        ?>
                                                <span class="label label-warning">Aberto</span>
                                        <?php           
                                                }
                                            }
                                        ?>
                                        </td>
                                        <td class="text-center"><?php echo implode('/', array_reverse(explode('-', $f['vencimento']))); ?></td>
                                        <td class="text-right">
                                            <a href="<?php echo URL;?>financeiro/formularioLancamento/<?php echo $f['id']; ?>">
                                                <button class="btn btn-flat btn-default">
                                                    <i class="fa fa-cog"></i>
                                                </button>
                                            </a>
                                            <!--
                                            <button class="btn btn-flat btn-danger">
                                                <i class="fa fa-trash" ></i>
                                            </button>
                                        -->
                                        </td>
                                    </tr>

                                <?php 
                                        endforeach;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->



