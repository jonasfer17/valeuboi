
<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Caixa</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-sm-2 col-xs-6">
                    <div class="description-block border-right">
                        <h5 class="description-header" id="totalVenda">R$ <?php if(isset($caixa['detalhes'])  && $caixa['detalhes']['venda'] > 0){ echo number_format($caixa['detalhes']['venda'], 2, ',',''); }else{ echo "0,00"; }?></h5>
                        <span class="description-text">TOTAL DE VENDA</span>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <div class="description-block border-right">
                        <h5 class="description-header" id="totalCaixa">R$ <?php if(isset($caixa['detalhes'])  && $caixa['detalhes']['caixa'] > 0){ echo number_format($caixa['detalhes']['caixa'], 2, ',',''); }else{ echo "0,00"; }?></h5>
                        <span class="description-text">TOTAL DE CAIXA</span>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <div class="description-block border-right">
                        <h5 class="description-header" id="totalFiado">R$ <?php if(isset($caixa['detalhes'])  && $caixa['detalhes']['fiado'] > 0){ echo number_format($caixa['detalhes']['fiado'], 2, ',',''); }else{ echo "0,00"; }?></h5>
                        <span class="description-text">TOTAL DE FIADO</span>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <div class="description-block border-right">
                        <h5 class="description-header" id="totalSaida">R$ <?php if(isset($caixa['detalhes'])  && $caixa['detalhes']['saida'] > 0){ echo number_format($caixa['detalhes']['saida'], 2, ',',''); }else{ echo "0,00"; }?></h5>
                        <span class="description-text">TOTAL DE SAIDAS</span>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <div class="description-block border-right">
                        <h5 class="description-header" id="mediaCliente">R$ <?php if(isset($caixa['detalhes'])  && $caixa['detalhes']['media'] > 0){ echo number_format($caixa['detalhes']['media'], 2, ',',''); }else{ echo "0,00"; }?></h5>
                        <span class="description-text">MÉDIA POR CLIENTE</span>
                    </div>
                </div>
                <div class="box-tools">
                    <div class="row">
                        <button type="button" class="btn btn-primary btn-flat margin pull-right" id="btn-add"><i class="fa fa-plus"></i> Adicionar</button>
                    </div>
                </div>
                <br><br>
                <form method="post" id="form-filtro-caixa">
                    <div class="row">
                        <div class="col-md-3">
                            
                        </div>
                        <div class="col-md-3">
                            
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" name="intervalo" id="reservation">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-default btn-flat"><i class="fa fa-search"></i> Pesquisar</button>
                        </div>
                    </div>
                </form>
                <br><br>
                <div class="table-responsive">
                <table id="tabela" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Data</th>
                            <th>Valor de venda</th>
                            <th>Valor no caixa</th>
                            <th>Valor fiado</th>
                            <th>Valor saida</th>
                            <th>Ticket médio</th>
                            <th class="text-right">Ação</th>
                        </tr>
                    </thead>
                    <tbody id="tabela-caixa">
                    <?php
                    if(isset($caixa['caixa'])  && count($caixa['caixa']) > 0){
                        foreach($caixa['caixa'] as $p):?>
                                <tr>
                                    <td><?php echo implode('/', array_reverse(explode('-', $p['data'])));?></td>
                                    <td><?php echo number_format($p['valor_total'], 2, ',',''); ?></td>
                                    <td><?php echo number_format($p['valor_caixa'], 2, ',',''); ?></td>
                                    <td><?php echo number_format($p['valor_fiado'], 2, ',',''); ?></td>
                                    <td><?php echo number_format($p['valor_saida'], 2, ',',''); ?></td>
                                    <td><?php echo number_format($p['media_cliente'], 2, ',',''); ?></td>
                                    <td class="text-right">
                                        <button class="btn btn-flat btn-primary" onClick="btnInfo(this);"  data-id="<?php echo $p['id'];?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                        <?php if(in_array('id', $permissao)):?>
                                        <button class="btn btn-flat btn-default" onClick="btnEdit(this);" data-id="<?php echo $p['id'];?>">
                                            <i class="fa fa-cog"></i>
                                        </button>
                                        <?php endif;?>
                                        <?php if(in_array('id', $permissao)):?>
                                        <button class="btn btn-flat btn-danger" onClick="btnDelete(this);" data-id="<?php echo $p['id'];?>">
                                            <i class="fa fa-trash" ></i>
                                        </button>
                                        <?php endif;?>
                                    </td>
                                </tr>
                            <?php endforeach;
                        }?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

<div class="modal fade" id="modal-default">
    <form id="form-caixa" method="POST">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Fluxo de Caixa</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <label>Data</label>
                            <div class="form-group">
                                <input type="text" class="form-control date" id="data" name="data" value="<?php echo date('d/m/Y')?>">
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <label>Total de clientes</label>
                            <div class="form-group">
                                <input type="text" class="form-control" id="total_cliente" name="total_cliente">
                            </div>
                        </div>
                         <div class="col-md-3 col-sm-3 col-xs-6">
                            <label>Ticket Médio</label>
                            <div class="form-group">
                                <input type="text" class="form-control" id="ticket" name="ticket" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <label for="nome">Total de vendas</label>
                            <div class="form-group">
                                <input type="text" class="form-control" id="valor_total" data-mask="##0,00" data-mask-reverse="true" name="total_venda">
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <label for="nome">Total no caixa</label>
                            <div class="form-group">
                                <input type="text" class="form-control" readonly id="valor_caixa" data-mask="##0,00" data-mask-reverse="true" name="total_caixa" >
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <label for="nome">Diferença</label>
                            <div class="form-group">
                                <input type="text" class="form-control" id="diferenca" data-mask="##0,00" data-mask-reverse="true" name="diferenca" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="row" class="forma_pagamento"> 
                    <?php 
                        foreach($forma_pagamento as $fp): ?>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <label>Total vendido em <?php echo $fp['nome']?></label>
                                <div class="form-group">
                                    <input type="text" class="form-control forma" data-mask="##0,00" id="forma<?php echo $fp['id'];?>" data-mask-reverse="true" name="forma_pagamento[<?php echo $fp['id'];?>]">
                                </div>
                            </div>
                        <?php 
                        endforeach; 
                    ?>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <label>Total vendido no Fiado</label>
                            <div class="form-group">
                                <input type="text" class="form-control forma" data-mask="##0,00" id="fiado"  data-mask-reverse="true" name="fiado">
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <label>Total de saidas</label>
                            <div class="form-group">
                                <input type="text" class="form-control saida" data-mask="##0,00" id="saida"  data-mask-reverse="true" name="saida">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        
                    </div>
                    <div class="row" class="forma_pagamento">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label for="nome">Observação</label>
                            <div class="form-group">
                                <textarea name="obs" id="obs" class="form-control" rows="3"></textarea>
                            </div>
                        </div> 
                    </div>
                    
                </div>
                

                <div class="modal-footer">
                    <button type="submit" class="btn btn-flat btn-primary">Salvar</button>
                    <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Voltar</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </form>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<div class="modal fade" id="modal-info">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="box box-default box-solid collapsed-box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Entradas</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                            <div class="box-body" style="display: none;">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Forma de pagamento</th>
                                            <th>Valor Total</th>
                                            <th>Valor Liquido</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tabela-entrada">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-default box-solid collapsed-box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Saidas</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                            <div class="box-body" style="display: none;">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Descrição</th>
                                            <th>Valor</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tabela-saida">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-default box-solid collapsed-box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Fiado</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                            <div class="box-body" style="display: none;">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Descrição</th>
                                            <th>Valor</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tabela-fiado">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Voltar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


