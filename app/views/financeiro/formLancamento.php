    <?php 
    if(isset($lancamento)){
            extract($lancamento);
    }

    ?>
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-tag"></i> Lançamentos</h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small style="color: gray">Evitar lançamentos com dias maiores que 28 que repitam cobranças</small>
        </div>
        <form class="form-horizontal" method="POST" id="form-lancamento" <?php if(isset($lancamento)){ echo 'data-url="editarLancamento/'.$lancamento['id'].'"'; }else{ echo 'data-url="novoLancamento"'; }?> >
            <div class="box-body">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="#tab_1" data-toggle="tab">Geral</a></li>
                      <li><a href="#tab_2" data-toggle="tab">Extra</a></li>
                    </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Data do caixa</label>
                                        <div class="col-sm-2">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="data_caixa" class="form-control date" <?php if(isset($lancamento) && !empty($lancamento['data_caixa'])){ echo 'value="'.implode('/', array_reverse(explode('-', $lancamento['data_caixa']))).'"'; } ?> >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Categorias</label>
                                        <div class="col-sm-4">
                                            <select class="form-control select2" id="categoria" name="categoria" style="width: 100%;">
                                                <option value="0" selected>Selecione</option>
                                                <?php foreach($categoria as $c):?>
                                                    <option value="<?php echo $c['id']; ?>" <?php if(isset($lancamento) && $lancamento['id_categoria'] == $c['id']){ echo "selected"; }?> ><?php echo $c['nome']; ?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Descrição</label>
                                        <div class="col-sm-6">
                                            <input type="text" name="descricao" class="form-control" <?php if(isset($lancamento) && !empty($lancamento['descricao'])){ echo 'value="'.$lancamento['descricao'].'"'; }?> >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Fornecedor / Cliente</label>
                                        <div class="col-sm-6">
                                            <select class="form-control select2" name="ator" style="width: 100%;">
                                                <option value="0">Nenhum selecionado</option>
                                                <?php foreach($pessoa as $c): ?>
                                                    <option value="<?php echo $c['id'];?>" <?php if(isset($lancamento) && $lancamento['id_ator'] == $c['id']){ echo "selected"; }?> ><?php echo $c['nome']; ?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Número do documento</label>
                                        <div class="col-sm-6">
                                            <input type="text" name="num_doc" class="form-control" <?php if(isset($lancamento) && !empty($lancamento['numero_doc'])){ echo 'value="'.$lancamento['numero_doc'].'"'; }?>  data-mask="00000000000000000000000000000000000000000000">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Forma de pagamento</label>
                                        <div class="col-sm-4">
                                            <select class="form-control" name="forma_pagamento" style="width: 100%;">
                                                <option value="0">Selecione</option>
                                                <?php foreach($forma_pagamento as $f):?>
                                                    <option value="<?php echo $f['id']; ?>" <?php if(isset($lancamento) && $lancamento['id_forma_pagamento'] == $f['id']){ echo "selected"; }?> ><?php echo $f['nome']; ?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Tipo</label>
                                        <div class="col-sm-4">
                                            <select class="form-control" name="tipo" style="width: 100%;">
                                                <option value="2" <?php if(isset($lancamento) && $lancamento['tipo'] == '0'){ echo "selected"; }?> >Receita</option>
                                                <option value="1" <?php if(isset($lancamento) && $lancamento['tipo'] == '1'){ echo "selected"; }?> >Despesa</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Vencimento</label>
                                        <div class="col-sm-2">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="vencimento" class="form-control date" <?php if(isset($lancamento) && !empty($lancamento['vencimento'])){ echo 'value="'.implode('/', array_reverse(explode('-', $lancamento['vencimento']))).'"'; } ?> >
                                            </div>
                                        </div>
                                    </div>
                                </div>        
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Valor</label>
                                        <div class="col-sm-2">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    R$
                                                </div>
                                                <input type="text" name="valor" class="form-control money" <?php if(isset($lancamento) && !empty($lancamento['valor'])){ echo 'value="'.$lancamento['valor'].'"'; }?> >
                                            </div>
                                        </div>
                                    </div>
                                </div>   
                                
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Pago?</label>
                                        <div class="col-sm-2">
                                            <select class="form-control" id="status" name="status">
                                                <option value="0" <?php if(isset($lancamento) && $lancamento['status'] == '0'){ echo "selected"; }?> >Não</option>
                                                <option value="1" <?php if(isset($lancamento) && $lancamento['status'] == '1'){ echo "selected"; }?> >Sim</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row pago_em">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" >Pago em</label>
                                        <div class="col-sm-2">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="pago_em" class="form-control date" <?php if(isset($lancamento) && !empty($lancamento['pago_em'])){ echo 'value="'.implode('/', array_reverse(explode('-', $lancamento['pago_em']))).'"'; } ?>>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row pago_em" <?php if(isset($lancamento) && $lancamento['status'] == '1'){ echo 'style="display: bolck;"'; } ?> >
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" >Multa/Juros</label>
                                        <div class="col-sm-2">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="multa" class="form-control money" <?php if(isset($lancamento) && !empty($lancamento['multa'])){ echo 'value="'.$lancamento['multa'].'"'; }?>>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php if(!isset($lancamento)):?>
                                <hr>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Se repete?</label>
                                        <div class="col-sm-2">
                                            <select class="form-control" id="repete" name="repete">
                                                <option value="0">Não se repete</option>
                                                <option value="1">A cada mês</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row repete">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Vezes</label>
                                        <div class="col-sm-1">
                                            <input type="text" name="intervalo" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            </div>
                            <div class="tab-pane" id="tab_2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Observações</label>
                                            <textarea class="form-control" rows="4" placeholder=""></textarea>
                                        </div>          
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                <div class="box-footer">
                    <button type="submit" class="btn btn-info btn-flat pull-right">Salvar</button>
                    <a href="<?php echo URL;?>financeiro/lancamento/">
                        <button type="button" class="btn btn-default btn-flat pull-right">Voltar</button>
                    </a>
                </div>
                <!-- /.box-footer -->
            </div>
        </form>
    </div>
              