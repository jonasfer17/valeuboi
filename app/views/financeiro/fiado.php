<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Fiado</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-offset-1 col-sm-2 col-xs-6">
                    <div class="description-block border-right">
                        <h5 class="description-header" id="total_aberto">R$ <?php if($total_aberto > 0){ echo number_format($total_aberto, 2, ',','.'); }else{ echo "0,00"; }?></h5>
                        <span class="description-text">TOTAL DE ABERTOS</span>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <div class="description-block border-right">
                        <h5 class="description-header" id="total_vencido">R$ <?php if($total_vencido > 0){ echo number_format($total_vencido, 2, ',','.'); }else{ echo "0,00"; }?></h5>
                        <span class="description-text">TOTAL VENCIDOS</span>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <div class="description-block border-right">
                        <h5 class="description-header" id="total_pago">R$ <?php if($total_pago > 0){ echo number_format($total_pago, 2, ',','.'); }else{ echo "0,00"; }?></h5>
                        <span class="description-text">TOTAL PAGO</span>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <div class="description-block border-right">
                        <h5 class="description-header" id="total_fiado">R$ <?php if($total_fiado > 0){ echo number_format($total_fiado, 2, ',','.'); }else{ echo "0,00"; }?></h5>
                        <span class="description-text">TOTAL DE FIADO</span>
                    </div>
                </div>
                <div class="box-tools">
                    <div class="row">
                        <button type="button" class="btn btn-primary btn-flat margin pull-right" id="btn-add"><i class="fa fa-plus"></i> Adicionar</button>
                    </div>
                </div>
                <br><br>
                <form method="post" id="form-filtro-fiado">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-3">
                            <select class="form-control" name="status">
                                <option value="99">Selecione o status</option>
                                <option value="0">Aberto</option>
                                <option value="1">Pagos</option>
                                <option value="2">Vencidos</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" name="intervalo" id="reservation">
                                </div>
                              </div>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-default btn-flat"><i class="fa fa-search"></i> Pesquisar</button>
                        </div>
                    </div>
                </form>
                <br><br>
                <div class="table-responsive">
                <table id="tabela" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Cliente</th>
                            <th>Valor</th>
                            <th>Data do caixa</th>
                            <th>Vencimento</th>
                            <th>Status</th>
                            <th class="text-right">Ações</th>
                        </tr>
                    </thead>
                    <tbody id="tabela-fiado">
                    <?php foreach($fiado as $p):?>
                                <tr>
                                    <td><?php echo $p['nome']?></td>
                                    <td><?php echo number_format($p['valor'], 2, ',', '');?></td>
                                    <td><?php echo implode('/', array_reverse(explode('-', $p['data_caixa'])));?></td>
                                    <td><?php echo implode('/', array_reverse(explode('-', $p['vencimento']))); ?></td>
                                    <td><?php if($p['status'] == 0 && $p['vencimento'] < date('Y-m-d')){ ?> <span class="label label-danger">Atrasado</span> <?php }else if($p['status'] == 0){ ?> <span class="label label-warning">Aberto</span> <?php }else{ ?> <span class="label label-success">Pago</span>  <?php } ?></td>
                                    <td class="text-right">
                                        <?php if(in_array('id', $permissao)): ?>
                                        <button class="btn btn-flat btn-primary btn-info"  data-id="<?php echo $p['id'];?>">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    <?php endif; ?>
                                        <button class="btn btn-flat btn-default" onclick="btnEdit(this);" data-id="<?php echo $p['id'];?>">
                                            <i class="fa fa-cog"></i>
                                        </button>
                                        <button class="btn btn-flat btn-danger" onclick="btnDelete(this);" data-id="<?php echo $p['id'];?>">
                                            <i class="fa fa-trash" ></i>
                                        </button>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Cliente</th>
                            <th>Valor</th>
                            <th>Data do caixa</th>
                            <th>Vencimento</th>
                            <th>Status</th>
                            <th class="text-right">Ações</th>
                        </tr>
                    </tfoot>
                </table>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

<div class="modal fade" id="modal-default">
    <form id="form-fiado" class="form-modal" method="POST">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Fiado</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <label>Data do caixa</label>
                            <div class="form-group">
                                <input type="text" class="form-control date" id="data" name="data" value="<?php echo date('d/m/Y')?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Cliente</label>
                            <div class="form-group">
                                <select class="form-control select2" id="cliente" name="cliente" style="width: 100%;">
                                    <option value="0" disabled="disabled" selected="true">Selecione o Cliente</option>
                                    <?php foreach($clientes as $c):?>
                                        <option value="<?php echo $c['id'];?>"><?php echo $c['nome'];?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <label>Valor</label>
                            <div class="form-group">
                                <input type="text" class="form-control" id="valor" data-mask="##0,00" data-mask-reverse="true" name="valor">
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <label>Vencimento</label>
                            <div class="form-group">
                                <input type="text" class="form-control date" id="vencimento" name="vencimento">
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <label>Status</label>
                            <div class="form-group">
                                <select class="form-control" name="status" id="status">
                                    <option value="0">Aberto</option>
                                    <option value="1">Pago</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="pag">
                         <div class="col-md-4 col-sm-4 col-xs-12">
                            <label>Data de pagamento</label>
                            <div class="form-group">
                                <input type="text" class="form-control date" id="pagamento" name="pagamento">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="obs">Observação</label>
                            <div class="form-group">
                                <textarea class="form-control" id="obs" name="obs" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-flat btn-primary">Salvar</button>
                    <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Voltar</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </form>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<div class="modal fade" id="modal-info">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Informações do Caixa</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-7">
                        <div class="box-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Forma de pagamento</th>
                                        <th>Valor Total</th>
                                        <th>Valor Liquido</th>
                                    </tr>
                                </thead>
                                <tbody id="tabela-corpo">
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <br>
                        <div id="usuario"></div>
                        <b></b>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Voltar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<script>
    $(function(){
        
        
    })
</script>