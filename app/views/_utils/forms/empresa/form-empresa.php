<div class="box">
    <div class="box-body">
        <form class="form" method="post" id="form-empresa" <?php if(isset($ator['id'])){ ?> data-url="editarEmpresa/<?php echo $ator['id'];?>" <?php }else{ ?> data-url="cadastrarEmpresa/" <?php } ?>>
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#geral" data-toggle="tab" aria-expanded="true">Geral</a></li>
                    <li class=""><a href="#endereco" data-toggle="tab" aria-expanded="false">Endereço</a></li>
                    <li class=""><a href="#tributario" data-toggle="tab" aria-expanded="false">Tributário</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="geral" style="min-height: 300px;">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Nome <span class="required">*</span></label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="nome"value="<?php if(isset($ator['nome'])){ echo $ator['nome']; }?>" autofocus="true">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Sigla <span class="required">*</span></label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="sigla" value="<?php if(isset($ator['sigla'])){ echo $ator['sigla']; }?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Tipo <span class="required">*</span></label>
                                <div class="form-group">
                                    <select class="form-control" name="tipo" id="tipo-pessoa">
                                        <option value="1">Fisica</option>
                                        <option value="2">Juridica</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 fisica">
                                <label>CPF <span class="required">*</span></label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="cpf" value="<?php if(isset($ator['cpf'])){ echo $ator['cpf']; }?>">
                                </div>
                            </div>
                            <div class="col-md-6 juridica">
                                <label>Razão social <span class="required">*</span></label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="razao_social" value="<?php if(isset($ator['razao_social'])){ echo $ator['razao_social']; }?>">
                                </div>
                            </div>
                            <div class="col-md-3 juridica">
                                <label>CNPJ <span class="required">*</span></label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="cnpj" value="<?php if(isset($ator['cnpj'])){ echo $ator['cnpj']; }?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Email <span class="required">*</span></label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="email" value="<?php if(isset($ator['email'])){ echo $ator['email']; }?>" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Responsável <span class="required">*</span></label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="responsavel" value="<?php if(isset($ator['responsavel'])){ echo $ator['responsavel']; }?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Telefone 1 <span class="required">*</span></label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="telefone_1" value="<?php if(isset($ator['telefone'])){ echo $ator['telefone']; }?>" >
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>Telefone 2</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="telefone_2" value="<?php if(isset($ator['telefone'])){ echo $ator['telefone']; }?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>Telefone 3</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="telefone_3" value="<?php if(isset($ator['telefone'])){ echo $ator['telefone']; }?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="endereco" style="min-height:300px;">
                        <div class="row">
                            <div class="col-md-2">
                                <label>CEP</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="cep" value="<?php if(isset($ator['cep'])){ echo $ator['cep']; }?>" >
                                </div>
                            </div>
                            <div class="col-md-8">
                                <label>Rua</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="rua" value="<?php if(isset($ator['rua'])){ echo $ator['rua']; }?>">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label>N°</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="numero" value="<?php if(isset($ator['numero'])){ echo $ator['numero']; }?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Complemento</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="complemento" value="<?php if(isset($ator['complemento'])){ echo $ator['complemento']; }?>" >
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>Bairro</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="bairro" value="<?php if(isset($ator['bairro'])){ echo $ator['bairro']; }?>">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>Cidade</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="cidade" value="<?php if(isset($ator['cidade'])){ echo $ator['cidade']; }?>">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>Estado</label>
                                <div class="form-group">
                                    <select class="form-control" name="estado">
                                        <option value="0">Selecione</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="tab-pane" id="tributario" style="min-height:300px;">
                        <div class="row">
                            <div class="col-md-2">
                                <label>Insc. Estadual</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="inscricao_estadual" value="<?php if(isset($ator['ie'])){ echo $ator['ie']; }?>">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>Regime tributário</label>
                                <div class="form-group">
                                    <select class="form-control" name="regime">
                                        <option value="0">Selecione</option>
                                        <option value="1">Simples Nacional</option>
                                        <option value="0">Normal</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>Contribuinte ICMS</label>
                                <div class="form-group">
                                    <select class="form-control" name="icms">
                                        <option value="0">Não</option>
                                        <option value="1">Sim</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>PIS/Confins</label>
                                <div class="form-group">
                                    <select class="form-control" name="pis">
                                        <option value="0">Não</option>
                                        <option value="1">Sim</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary btn-flat pull-right">Salvar</button>
                    <a href="<?php echo URL;?>empresa/">
                        <button type="button" class="btn btn-default btn-flat pull-right">Voltar</button>
                    </a>
                </div>
            </form>
        </div>