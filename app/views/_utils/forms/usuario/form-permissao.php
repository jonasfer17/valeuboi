
    <div class="box">
        <div class="box-body" style="min-height: 450px;">
            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <form class="form" method="post" id="form-permissao" <?php if(isset($permissoes['id'])){ ?> data-url="editarPermissao/<?php echo $permissoes['id'];?>" <?php }else{ ?> data-url="cadastrarPermissao/" <?php } ?> >
                        <div class="row">
                            <div class="col-md-12">
                                <label>Categoria</label>
                                <div class="form-group">
                                    <select class="form-control" name="categoria">
                                        <option value="0">Selecione</option>
                                    <?php foreach($categoria as $cat):?>
                                        <option value="<?php echo $cat['id'];?>"  <?php if(isset($permissoes['id_categoria'])){ if($permissoes['id_categoria'] == $cat['id'] ){ echo "selected"; } } ?> ><?php echo $cat['nome'];?></option>   
                                    <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label>Nome</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="nome" id="nome" value="<?php if(isset($permissoes['nome'])){ echo $permissoes['nome']; }?>" autofocus="true">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-flat pull-right">Salvar</button>
                                <a href="<?php echo URL;?>usuario/permissao">
                                    <button type="button" class="btn btn-default btn-flat pull-right">Voltar</button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

            
        