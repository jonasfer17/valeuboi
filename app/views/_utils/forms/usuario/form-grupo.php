<div class="box">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <form class="form" method="post" id="form-grupo" <?php if(isset($grupo['id'])){ ?> data-url="editarGrupo/<?php echo $grupo['id'];?>" <?php }else{ ?> data-url="cadastrarGrupo/" <?php } ?>>
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8">
                            <div class="form-group">
                                <label>Nome</label>
                                <input type="text" class="form-control" name="nome" value="<?php if(isset($grupo['nome'])){ echo $grupo['nome']; }?>" autofocus="true">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Permissões</h3><br>
                                    <small style="color: #ababab;">Selecione ao menos 1 permissão</small>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-6">
                                                <?php
                                                    $i = 0; 
                                                    foreach ($permissoes as $per): 
                                                    if($i < 15){?>
                                                        <div class="checkbox icheck">
                                                        <label for="c_<?php echo $per['id']; ?>" style="font-size: 12px;">
                                                            <input type="checkbox" name="permissao[]" id="c_<?php echo $per['id']; ?>" value="<?php echo $per['id']; ?>" <?php if(isset($parametros)){ if(in_array($per['id'], $parametros)){ echo 'checked'; } }?> > <?php echo $per['nome'];?>
                                                        </label>
                                                    </div>
                                                <?php
                                                    $i++;  
                                                    }else{ ?>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-6">
                                            <?php
                                                $i = 0;
                                                }
                                            endforeach; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-flat pull-right">Salvar</button>
                            <a href="<?php echo URL;?>usuario/grupo">
                                <button type="button" class="btn btn-default btn-flat pull-right">Voltar</button>
                            </a>
                        
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>


                       