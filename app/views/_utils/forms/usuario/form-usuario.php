<div class="box">
    <div class="box-body" style="min-height: 450px;">
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal form" method="post">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Empresa</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="empresa" id="select-empresa">
                                <option value="0">Selecione</option>
                                <?php foreach($empresa as $emp): ?>
                                <option value="<?php echo $emp['id'];?>"> <?php echo $emp['nome']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Funcionário</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="funcionario" id="select-funcionario">
                                <option value="0">Selecione</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Login</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="login">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Senha</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="senha">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Grupo</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="grupo">
                            <option value="0">Selecione</option>
                                <?php foreach($grupo as $gp): ?>
                                <option value="<?php echo $gp['id'];?>"> <?php echo $gp['nome']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>        
                </div>
            </div>
        </div>
    </div>
</div>
    