<?php extract($caixa); ?>
<br>
<br>
<br>
<br>
<div class="text-center">
    <img src="<?php echo IMG;?>adm/pdv/cash-register.png">
    <h3>&nbsp;Fechamento de caixa</h3>
    <p>
        Caixa aberto em <?= implode('/', array_reverse(explode('-', $caixa['data_abertura']))) ?> - <?= $caixa['hora_abertura'] ?> . Fechado em <?= implode('/', array_reverse(explode('-', $caixa['data_fechamento']))) ?> - <?= $caixa['hora_fechamento'] ?>
    </p>
    <button type="button" class="btn btn-success btn-flat margin" style="width: 150px; height: 90px;">
        <b>R$ <?= number_format($caixa['fundo_caixa'],2, ',','') ?></b>
        <br>
        Saldo na abertura
    </button>
    <button type="button" class="btn btn-danger btn-flat margin" style="width: 150px; height: 90px;">
        <b>R$ <?= number_format($caixa['total_vendas'],2, ',','') ?></b>
        <br>
        Saldo em vendas
    </button>
    <button type="button" class="btn btn-success btn-flat margin" style="width: 150px; height: 90px;">
        <b>R$</b>
        <br>
        Total Saidas
    </button>
    <button type="button" class="btn btn-danger btn-flat margin" style="width: 150px; height: 90px;">
        <b>R$</b>
        <br>
        Total Fiado
    </button>
<br>
<br>
    <p>Saldo em caixa <b>R$ <?= number_format($caixa['total_geral'],2, ',','') ?></b></p>
<br>
    <button type="button" class="btn bg-navy btn-flat margin" id="btn-fechar-caixa"><i class="fa fa-print">
        </i>&nbsp;&nbsp;&nbsp;Imprimir
    </button>
<br>
    <a href="<?php echo URL;?>">
        <button type="button" class="btn btn-link btn-flat margin"><i class="fa fa-mail-reply">
            </i>&nbsp;&nbsp;&nbsp;Voltar
        </button>
     </a>
</div>


