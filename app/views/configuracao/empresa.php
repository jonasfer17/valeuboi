<div class="row">
    <div class="col-md-12">
        <div class="box box-default">
            <div class="box-header with-border">
                <i class="fa fa-warning"></i>
                <h3 class="box-title">Dados da Empresa</h3>
            </div>
            <div class="box-body">
            <form id="form" action="#" class="wizard-big">
                    <h1>Dados</h1>
                    <fieldset>
                    <h2>Dados Básicos</h2>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Nome *</label>
                                    <input id="nome" name="nome" type="text" value="Teste" class="form-control required">
                                </div>
                                <div class="form-group">
                                    <label>Fantasia *</label>
                                    <input id="fantasia" name="fantasia" type="text" value="Teste" class="form-control required">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>CNPJ *</label>
                                    <input id="cnpj" name="cnpj" type="text" value="13426323000193" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Inscrição Estadual</label>
                                    <input id="ie" name="ie" type="text" class="form-control number">
                                </div>
                            </div>
                            
                        </div>
                    </fieldset>
                    <h1>Endereço</h1>
                    <fieldset>
                        <h2>Endereço</h2>
                        <div class="row">
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>CEP *</label>
                                    <input id="cep" name="cep" type="text" data-mask="00.000-00" class="form-control required">
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <h1>Contato</h1>
                    <fieldset>
                        <div class="text-center" style="margin-top: 120px">
                            <h2>You did it Man :-)</h2>
                        </div>
                    </fieldset>

                    <h1>Finish</h1>
                    <fieldset>
                        <h2>Terms and Conditions</h2>
                        <input id="acceptTerms" name="acceptTerms" type="checkbox" class="required"> <label for="acceptTerms">I agree with the Terms and Conditions.</label>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>