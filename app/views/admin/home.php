<div class="row">
    <div class="col-md-3" style="display: none;">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-line-chart"></i>
                <h3 class="box-title">Recebimentos mês(%)</h3>
            </div>
            <div class="box-body text-center">
                <div id="recebimento-chart" style="height: 150px;"></div>
            </div>
            <div class="box-footer no-padding">
                <ul class="nav nav-pills nav-stacked">
                    <li>
                        <a href="#">Realizado <span class="pull-right text-blue"> R$ 200</span></a>
                    </li>
                    <li>
                        <a href="#">Diferença <span class="pull-right text-red"> R$ 800,00</span></a>
                    </li>
                    <li>
                        <a href="#">Previsto <span class="pull-right"> R$ 1000,00</span></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-line-chart"></i>
                <h3 class="box-title">Despesas mês(%)</h3>
            </div>
            <div class="box-body text-center">
                <div id="despesa-chart" style="height: 150px;"></div>
            </div>
            <div class="box-footer no-padding">
                <ul class="nav nav-pills nav-stacked">
                    <li>
                        <a href="#">Realizado <span class="pull-right text-blue"> R$ 1212,00</span></a>
                    </li>
                    <li>
                        <a href="#">Diferença <span class="pull-right text-red"> R$ -12,00</span></a>
                    </li>
                    <li>
                        <a href="#">Previsto <span class="pull-right"> R$ 1200,00</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-4"></div>
    <div class="col-md-5" style="display: none;">
        <!-- Calendar -->
        <div class="box box-solid">
            <div class="box-header">
                <i class="fa fa-calendar"></i>
                <h3 class="box-title">Calendário</h3>
            </div>
            
            <div class="box-body no-padding">
              <div id="calendar" style="width: 100%" value="02-16-2012"></div>
            </div>
            
            <div class="box-footer text-black">
                <div class="row">
                    <br>
                    <div class="col-sm-12">
                        <div class="clearfix text-center">
                            <span>Lançamentos do dia</span>
                        </div>
                    </div>
                    <br>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-text-width"></i>
                <h3 class="box-title">Contas</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text" style="font-size: 12px;">A Receber Hoje</span>
                                <span class="info-box-number">R$ <?php  echo $recebimentos_hoje['valor'] > 0 ? number_format($recebimentos_hoje['valor'], 2, ',','.') : '0,00'; ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-red"><i class="fa fa-flag-o"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text" style="font-size: 11px;">Recebimentos Atrasados</span>
                                <span class="info-box-number">R$ <?php  echo $recebimentos_atrasados['valor'] > 0 ? number_format($recebimentos_atrasados['valor'], 2, ',','.') : '0,00'; ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-green"><i class="fa fa-envelope-o"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text" style="font-size: 12px;">A pagar Hoje</span>
                                <span class="info-box-number">R$ <?php  echo $contas_pagar['valor'] > 0 ? number_format($contas_pagar['valor'], 2, ',','.') : '0,00'; ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-red"><i class="fa fa-flag-o"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text" style="font-size: 12px;">Pagamentos Atrasados</span>
                                <span class="info-box-number">R$ <?php  echo $contas_atrasadas['valor'] > 0 ? number_format($contas_atrasadas['valor'], 2, ',','.') : '0,00'; ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-6">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Caixas</h3>
                <div class="box-tools">
                </div>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Usuário</th>
                            <th>Data</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($caixas as $c): ?>
                            <tr>
                                <td><?= $c['login']; ?></td>
                                <td><?= implode('/', array_reverse(explode('-', $c['data_abertura']))); ?></td>
                                <td><?= $c['status'] == '0' ? '<span class="label label-success">Aberto</span>' : '<span class="label label-danger">Fechado</span>'; ?></td>
                                <td><span class="glyphicon glyphicon-search" aria-hidden="true" onclick="getInfoCaixa(this)" data-id="<?= $c['id']; ?>"></span></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-info-caixa">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top: 20%;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <ul>
                            <li>Info caixa
                                <ul>
                                    <li><b>Usuário: </b> <span id="sp_usuario"></span> </li>
                                    <li><b>Data abertura: </b> <span id="sp_dt_abertura"></span> </li>
                                    <li><b>Hora abertura: </b> <span id="sp_hr_abertura"></span> </li>
                                    <li><b>Data fechamento: </b> <span id="sp_dt_fechamento"></span> </li>
                                    <li><b>Hora fechamento: </b> <span id="sp_hr_fechamento"></span> </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul>
                            <li>Valores
                                <ul>
                                    <li><b>Fundo de caixa: </b> <span id="sp_fundo_caixa"></span> </li>
                                    <li><b>Desconto: </b> <span id="sp_desconto"></span> </li>
                                    <li><b>Vendas: </b> <span id="sp_venda"></span> </li>
                                    <li><b>Total: </b> <span id="sp_total"></span> </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Voltar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>