<?php 
    if($venda['code'] != '200'){
         echo '<script>$(function(){ $("#modal-forma-pagamento").modal("show"); });</script>';
    }
    
?>

<div class="row">
    <div class="col-md-9">
        <div class="form-group">
            <label>Cliente</label>
            <select class="form-control select2" style="width: 100%;">
                <option value="1">Selecione o cliente</option>
            </select>
        </div>
    </div>
    <div class="col-md-3 ">
         <label><br></label>
        <button class="btn btn-block btn-primary" data-toggle="modal" data-target="#modal-forma-pagamento">
            Adicionar forma de pagamento
        </button>
    </div>
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-body" id="box-pagamento" style="height: 280px;">
                <?php if($venda['code'] == '200'){
                        foreach($venda['venda'] as $v):?>
                        <div class="row pagamento">
                            <div class="col-md-4">
                                <h3><?php echo $v['nome']?></h3>
                            </div>
                            <div class="col-md-4">
                                <h3><input type="text" name="valor" class="form-control input-finalizar money" id="fp<?php echo $v['id']?>" value="<?php echo $v['valor'];?>"></h3>
                            </div>
                            <div class="col-md-3 text-right" style="margin-top: 5px;">
                               <h3><i class="fa fa-trash"></i></h3>
                            </div>
                        </div>
                        <hr>
                    <?php 
                        endforeach; 
                    }?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-3 col-xs-12">
        <div class="info-box text-center">
            <span class="info-box-text"><h3 id="valor-total">R$ <?php echo $info['info']['valor_total'];?></h3></span>  
            <span class="info-box-text"><h5>Valor total</h5></span>
        </div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-12">
        <div class="info-box text-center">
            <span class="info-box-text"><h3 id="valor-desconto">R$ <?php echo $info['info']['descontos'];?></h3></span>  
            <span class="info-box-text"><h5>Desconto</h5></span>
        </div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-12">
        <div class="info-box text-center">
            <span class="info-box-text"><h3 id="valor-recebido">R$ <?php echo $valor_recebido['valor'];?></h3></span>  
            <span class="info-box-text"><h5>Valor recebido</h5></span>
        </div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-12">
        <div class="info-box text-center">
            <span class="info-box-text"><h3 id="valor-troco">R$ <?php
            if($valor_recebido['valor'] > ($info['info']['valor_total'] - $info['info']['descontos']) ) {
                $troco = ($valor_recebido['valor'] - ($info['info']['valor_total'] - $info['info']['descontos']));
                echo number_format($troco, 2,',','');
            }else{
                echo "0,00";
            }
            ?>
            </h3></span>  
            <span class="info-box-text"><h5>Troco</h5></span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-sm-4"></div>
    <div class="col-md-3 col-sm-4 col-xs-6">
        <a href="<?php echo URL;?>pdv">
            <button class="btn btn-block btn-link">
                <h4>Voltar (Ctrl+F11)</h4>
            </button>
        </a>
    </div>
    <div class="col-md-3 col-sm-4 col-xs-6">
        <a href="<?= URL ?>pdv/encerrarVenda">
            <button class="btn btn-block btn-primary" id="btn-finalizar" <?php if($valor_recebido['valor'] < ($info['info']['valor_total'] - $info['info']['descontos'])){ echo 'disabled="true"'; }?>>
                <h5>Finalizar venda (Ctrl+Enter)</h5>
            </button>
        </a>
    </div>
</div>


<div class="modal fade" id="modal-forma-pagamento" data-backdrop="false">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top: 100px;" >
            <div class="modal-header">
                <h4 class="modal-title">Forma de pagamento</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <?php foreach ($forma_pagamento as $fp) { ?>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <button class="btn info-box btn-primary" onclick="addFormaPagamento(this);" data-id="<?php echo $fp['id'];?>">
                                <h4><?php echo $fp['nome'];?></h4>
                            </button>
                        </div>
                    <?php } ?>
                    
                </div>
            </div>
            <div class="modal-footer">
                <a href="<?php echo URL;?>pdv">
                    <button type="button" class="btn btn-primary">Voltar</button>
                </a>
            </div>
        </div>
    </div>
</div>


<script>
    var url_venda = 'encerrarVenda';
<?php   if($valor_recebido['valor'] >= ($info['info']['valor_total'] - $info['info']['descontos'])){?>
            var trava = 1;
<?php   }else{?>
            var trava = 0;
<?php   }?>
</script>