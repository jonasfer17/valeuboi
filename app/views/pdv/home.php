<div class="row">
	<form role="form" id="form-pdv-produto" method="POST">
		<div class="col-md-12">
			<div class="box box-danger">
	          	<div class="box-body">
	          		<div class="col-md-2">
		                <div class="form-group">
		                  <label>Quantidade</label>
		                  <input type="text" class="form-control" id="quantidade" name="quantidade" autofocus="true">
		                </div>
	            	</div>
	                <div class="col-md-4">
	                	<div class="form-group">
	                  		<label>Produto</label>
	                  		<input type="text" class="form-control" id="produto" name="produto" placeholder="Digite o código ou nome do produto">
	                	</div>
	            	</div>
	            	<div class="col-md-2">
		                <div class="form-group">
		                  <label>Valor unitário</label>
		                  <input type="text" class="form-control" id="unitario" name="unitario">
		                </div>
	            	</div>
	            	<div class="col-md-2">
		                <div class="form-group">
		                  <label>Valor total</label>
		                  <input type="text" class="form-control" id="total" name="total">
		                </div>
	            	</div>
	            	<div class="col-md-2">
						<div class="form-group">
							<label><br></label>
							<button type="button" class="btn btn-sm btn-block btn-primary btn-block-flat" onclick="insereProdutoEnter();" style="height: 100%;">Adicionar Produto (Enter)</button>
						</div>
					</div>
	        	</div>
	         </div>
	     </div>
    </form>
</div>
<div class="row">
    <div class="col-md-9">
        <div class="box box-danger">
            <div class="box-body table-responsive no-padding"  id="tabela" style="height: 300px;">
              	<table class="table table-condesed">
              		<thead>
              			<tr>
                            <th class="text-center">Produto</th>
                            <th class="text-center">Qtde.</th>
                            <th class="text-center">Valor unitário</th>
                            <th class="text-center">Valor total</th>
                            <th class="text-right"></th>
                        </tr>
              		</thead>
                	<tbody id="tabela-produto">
                        <?php 
                        if(isset($detalhe)){
                            foreach($detalhe as $v):?>
                                <tr id="<?php echo $v['id']?>">
                                    <td class="text-center"><?php echo $v['nome'];?></td>
                                    <td class="text-center"><?php echo floatval($v['quantidade']);?></td>
                                    <td class="text-center"><?php echo $v['valor_unitario'];?></td>
                                    <td class="text-center"><?php echo $v['valor_total']; ?></td>
                                    <td class="text-right">
                                        <button type="button" class="btn btn-sm btn-default btn-flat">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button type="button" class="btn btn-sm btn-default btn-flat" onclick="excluirProduto(this)" data-id="<?php echo $v['id'];?>">
                                            <i class="fa fa-trash" style="color:red;"></i>
                                        </button>
                                    </td>
                                </tr>
                            <?php endforeach;
                        }
                        ?>
                	</tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-3">
    	<div class="box box-success"  style="height: 300px;">
            <div class="box-header with-border">
              <h3 class="box-title">Informações</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              	<strong><i class="fa fa-book margin-r-5"></i> Venda N° </strong><br>
              	Data: <?php echo date('d/m/Y');?>
              	<hr>
              	<strong><i class="fa fa-file-text-o margin-r-5"></i> Opções</strong>
                <br><br>
                <button class="btn btn-primary">Opções do caixa (Ctrl+M)</button>
            </div>
            <!-- /.box-body -->
          </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
    </div>
    <div class="col-md-2 col-sm-3 col-xs-12">
        <div class="info-box text-center">
            <span class="info-box-text"><h3 id="total-itens"> <?php if(isset($cabecalho['total_produtos'])) { echo $cabecalho['total_produtos']; }else{ echo '0,00'; } ?></h3></span>  
            <span class="info-box-text"><h5>Itens</h5></span>
        </div>
    </div>
    <div class="col-md-2 col-sm-3 col-xs-12">
        <div class="info-box text-center">
            <span class="info-box-text"><h3 id="valor-desconto">R$ <?php if(isset($cabecalho['descontos'])) { echo number_format($cabecalho['descontos'], 2,',',''); }else{ echo '0,00'; } ?></h3></span>  
            <span class="info-box-text"><h5>Descontos</h5></span>
        </div>
    </div>
    <div class="col-md-2 col-sm-3 col-xs-12">
        <div class="info-box text-center">
            <span class="info-box-text"><h3 id="valor-total">R$ <?php if(isset($cabecalho['valor_total'])) { echo number_format($cabecalho['valor_total'] - $cabecalho['descontos'],2,',',''); }else{ echo '0,00'; } ?></h3></span>  
            <span class="info-box-text"><h5>Total</h5></span>
        </div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-12">
        <a href="<?php echo URL;?>pdv/setFormaPagamento">
            <button class="btn info-box btn-primary">
                <h5>Finalizar venda (Ctrl+Enter)</h5>
            </button>
        </a>
    </div>
</div>

<script>
    var url_venda = "setFormaPagamento/";
    var trava = 1;
</script>