<br>
<br>
<br>
<br>
<div class="text-center">
    <img src="<?php echo IMG;?>adm/pdv/cash-register.png">
    <h3><i class="fa fa-warning text-yellow"></i> &nbsp;Seu caixa não esta aberto!</h3>
    <p>
        É necessário abrir o caixa para
        realizar vendas no PDV
    </p>
    <button type="button" class="btn bg-navy btn-flat margin" data-toggle="modal" data-target="#modal-caixa"><i class="fa fa-unlock">
        </i>&nbsp;&nbsp;&nbsp;Abrir caixa
    </button>
    <a href="<?php echo URL;?>admin">
        <button type="button" class="btn bg-navy btn-flat margin"><i class="fa fa-mail-reply">
            </i>&nbsp;&nbsp;&nbsp;Voltar
        </button>
     </a>
</div>


<div class="modal fade" id="modal-caixa">
    <div class="modal-dialog">
        <div class="modal-content"style="margin-top: 20%;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Abrir Caixa</h4>
            </div>
            <form method="POST" id="form-novo-caixa">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Caixa</label>
                            <div class="form-group">
                                <input type="text" name="usuario" class="form-control" readonly value="<?php echo $_SESSION['nome'];?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Valor em caixa</label>
                            <div class="form-group">
                                <input type="text" name="valor" class="form-control money">
                            </div>
                        </div>
                    </div>
                </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal">
                    Cancelar
                </button>
                <button type="submit" class="btn btn-primary btn-flat" id="btn-salvar">
                    Salvar e ir ao PDV
                </button>
            </div>
            </form>
        </div>
    </div>
</div>
        