<?php

class Controller
{
    
    public $db = null;

    
    function __construct()
    {
        $this->getConexao();
    }

    /**
     * Inicia a conexão com o banco de dados - Caminho - app/config/config.php
     */
    private function getConexao()
    {
        // configura as opções (opcionais) da conexão PDO. neste caso, definimos o modo de busca para
        // "objetos", o que significa que todos os resultados serão objetos, como este: $ result-> user_name!
        // Por exemplo, o modo fetch FETCH_ASSOC retornaria resultados como este: $ result ["user_name]!
        $options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);

        // gerar uma conexão de banco de dados, usando o conector PDO
        $this->db = new PDO(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME.";charset=utf8", DB_USER, DB_PASS, $options);
    }

    public function loadView($view, $dados = array()){
        extract($dados);
        require_once VIEWS. $view .".php";
    }


    public function loadTemplate($view, $dados = array()){
        extract($dados);
        require_once VIEWS.'_templates/header.php';
        require_once VIEWS. $view .".php";
        require_once VIEWS.'_templates/footer.php';
    }

    public function loadTemplatePDV($view, $dados = array()){
        extract($dados);
        require_once VIEWS.'_templates/pdv_header.php';
        require_once VIEWS. $view .".php";
        require_once VIEWS.'_templates/pdv_footer.php';
    }

    public function loadPDF($view, $dados = array()){
        extract($dados);
        require_once VIEWS.'_pdf/cupom_header.php';
        require_once VIEWS. $view .".php";
        require_once VIEWS.'_pdf/cupom_footer.php';
    }

    public function loadForm($form){
        require_once FORMS. $form .".php";
    }

    public function loadModel($model_name)
    {
        require_once 'app/models/' . $model_name . '.php';
        return new $model_name($this->db);
    }

    public function loadDados($titulo, $menu, $submenu = '', $menuterc = '', $menuquar = ''){
        $dados = array();
        $dados['titulo'] = $titulo;
        $dados['menu'] = $menu;
        $dados['submenu'] = $submenu;
        $dados['menuterc'] = $menuterc;
        $dados['menuquar'] = $menuquar;
        return $dados;
    }

    public function permissoes(){
        return explode(',', $_SESSION['permissoes']);
    }



    public function estaLogado(){
        if(isset($_SESSION['idUsuario']) && !empty($_SESSION['idUsuario'])){
            return true;
        }else{
            return false;
        }
    }

}
